def key_positions(m, key):
    k = max(key(a) for a in m)
    c = [0 for i in range(k+1)]
    print(c)
    for a in m:
        c[key(a)] += 1
    summ = 0
    for i in range(k+1):
        c[i], summ = summ, summ + c[i]
    return c
        

def counting_sort(m, key):
    b = [None for i in range(len(m))]
    p = key_positions(m, key)
    for a in m:
        b[p[key(a)]] = a
        p[key(a)] = p[key(a)] + 1
    return b
print(counting_sort([2,-2,1], lambda x : x*x))
