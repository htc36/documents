def adjacency_list(graph_string):
    header, *edges = [s.split() for s in graph_string.splitlines()]
    directed = header[0] == 'D'
    weighted = len(header) == 3 and header[2] == 'W'
    num_vertices = int(header[1])
    adj_list = [[] for _ in range(num_vertices)]
    for edge in edges:
        edge_data = map(int, edge)
        if weighted:
            source, target, weight = edge_data
        else:
            source, target = edge_data
            weight = None
        adj_list[source].append((target, weight))
        if not directed:
            adj_list[target].append((source, weight))
    return adj_list



def bfs_loop(adj_list, q, state):
    while len(q) != 0:
        uuu = q.pop(0)
        for iii in adj_list[uuu]:
            numm = iii[0]
            if state[numm] == "U":
                state[numm] = "D"
                q.append(numm)
            state[uuu] = "P"
    
def comp(adj):
    n = len(adj) # n will be size of the adj
    state = ["U"] * n # initialize
    parent = [None] * n
    q = []
    components = []

    for iii in range(n):
        if state[iii] == "U":
            prev = state[:]
            state[iii] = "D"
            q.append(iii)
            bfs_loop(adj, q, state)
            new = []
            for jjj in range(len(state)):
                if prev[jjj] != state[jjj]:
                    new.append(jjj)
            components.append(new)
    return components
def arrangement(adj):
    adjl = adjacency_list(adj)
    return comp(adjl)
direct_friendship_info = """\
U 7
1 2
1 5
1 6
2 3
2 5
3 4
4 5
"""

print(sorted(sorted(tables) for tables in arrangement(direct_friendship_info)))
