def adjacency_list(graph_str):
    
    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])   
    
    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()
            
    
            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))
               
            overall[int(new[0])].append(tup)
        
    else:
        
        for iii in range(0, num_of_vert):
            
            for jjj in raw[1 : -1]:
                new = jjj.split()
                
                if iii == int(new[0]):
                    
                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)
                
                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))                
                    overall[int(new[1])].append(tup)
                    
        
    return overall

def dfs_tree(adj, s):
    n = len(adj)
    state = ["Un"] * n
    parent = [None] * n 
    state[s] = "Discovered"
    dfs_loop(adj, s, state, parent)
    return parent

def dfs_loop(adj, u, state, parent):
    for v in adj[u]:
        if state[v[0]] == "Un":
            state[v[0]] = "Discovered"
            parent[v[0]] = u
            dfs_loop(adj, v[0], state, parent)
        state[u] = "Processed"

# an undirected graph

adj_list = [
    [(1, None), (2, None)],
    [(0, None), (2, None)],
    [(0, None), (1, None)]
]

print(dfs_tree(adj_list, 0))
print(dfs_tree(adj_list, 1))
print(dfs_tree(adj_list, 2))


