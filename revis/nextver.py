def powerset(s):
    output = []
    s = list(s)
    helper('', len(s), output)
    overall = []
    for iii in output:
        new = []
        for jjj in range(len(iii)):
            if iii[jjj] == '1':
                new.append(s[jjj])
        overall.append(new)
    return overall

def helper(candidate, inputer, output):
    if len(candidate) == inputer:
        output.append((list(candidate)))
    else:
        for child_candidate in [candidate + '0', candidate + '1']:
            helper(child_candidate, inputer, output)
s = {1, 2, 3, 4, 5, 6, 7}
subsets = powerset(s)
print(len(subsets) == 2**7)
