class Node:
    """Represents an internal node in a Huffman tree. It has a frequency count
       and left and right subtrees, assumed to be the '0' and '1' children
       respectively.
    """
    def __init__(self, count, left, right):
        self.count = count
        self.left = left
        self.right = right

    def __str__(self, level=0):
        return ((2 * level) * ' ' + f"Node({self.count},\n" +
            self.left.__str__(level + 1) + ',\n' +
            self.right.__str__(level + 1) + ')')

    def is_leaf(self):
        return False

class Leaf:
    """A leaf node in a Huffman encoding tree. Contains a character and its
       frequency count.
    """
    def __init__(self, count, char):
        self.count = count
        self.char = char

    def __str__(self, level=0):
        return (level * 2) * ' ' + f"Leaf({self.count}, '{self.char}')"

    def is_leaf(self):
        return True



def huffman_decode(s, tree):
    copy = tree
    string = ''
    for iii in s:
        if iii == '0':
            copy = copy.left
        else:
            copy = copy.right
        if copy.is_leaf():
            string += copy.char
            copy = tree

    return string

def huffman_encode(s, huffman_tree):
    dictt = {}
    helper(dictt, huffman_tree, "")
    string = '' 
    for iii in s:
        string += (dictt[iii])

    return string

def helper(dicct, tree, path):
    if tree.is_leaf():
        dicct[tree.char] = path
    else:
        helper(dicct, tree.right, path + "1")
        helper(dicct, tree.left, path + "0")
        

# The example from the lecture notes
tree = Node(42,
  Node(17,
    Leaf(8, 'b'),
    Leaf(9, 'a')),
  Node(25,
    Node(10,
      Node(5,
        Leaf(2, 'f'),
        Leaf(3, 'd')),
      Leaf(5, 'e')),
    Leaf(15, 'c')))
print(huffman_encode('adcb', tree)) 
