def adjacency_list(graph_string):
    header, *edges = [s.split() for s in graph_string.splitlines()]
    directed = header[0] == 'D'
    weighted = len(header) == 3 and header[2] == 'W'
    num_vertices = int(header[1])
    adj_list = [[] for _ in range(num_vertices)]
    for edge in edges:
        edge_data = map(int, edge)
        if weighted:
            source, target, weight = edge_data
        else:
            source, target = edge_data
            weight = None
        adj_list[source].append((target, weight))
        if not directed:
            adj_list[target].append((source, weight))
    return adj_list

def adjacency_matrix(graph_str):
    adj_list = adjacency_list(graph_str)
    n = len(adj_list)
    adj_matrix = [[float('inf')] * n for _ in range(n)]
    for i in range(len(adj_matrix)):
        adj_matrix[i][i] = 0
    for iii in range(len(adj_list)):
        for v, w in adj_list[iii]:
            adj_matrix[iii][v] = w
    return adj_matrix



def floyd(adjacency_matrix):
    import copy
    new = copy.deepcopy(adjacency_matrix)
    for k in range(len(adj_matrix)):
        for j in range(len(adj_matrix)):
            for i in range(len(adj_matrix)):
                new[j][i] = min(new[j][i], new[j][k] + new[k][i])
    return new
                   



graph_str = """\
D 3 W
0 1 1
1 2 2
2 0 4
"""

adj_matrix = adjacency_matrix(graph_str)
dist_matrix = floyd(adj_matrix)

print("Adjacency matrix:", adj_matrix)
print("Distance matrix:", dist_matrix)
