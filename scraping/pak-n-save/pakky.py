import requests
from bs4 import BeautifulSoup
import json
import math


# Pushes request to Pak n Save API that retrieves the links to all the pages I want to scrape from on the website
def getUrlLinks(s):
    s.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}
    print(s.cookies.get_dict())
    test = s.post('https://www.paknsaveonline.co.nz/CommonApi/Store/ChangeStore?storeId=65defcf2-bc15-490e-a84f-1f13b769cd22').json()
    print(s.cookies.get_dict()["STORE_ID"])
    data = s.get('https://www.paknsaveonline.co.nz/CommonApi/Navigation/MegaMenu?v=2401&storeId=65defcf2-bc15-490e-a84f-1f13b769cd22').json()
    data2 = s.get('https://www.paknsaveonline.co.nz/CommonApi/Store/GetStoreList').json()["stores"]
    for iii in data2:
        if iii['id'] == '21ecaaed-0749-4492-985e-4bb7ba43d59c':
            print(iii['name'])
    print(data)
    overallSections = (data['NavigationList'][0]["Children"])
    links = []
    for section in overallSections:
        for innerSection in (section["Children"]):
            if (len(innerSection["Children"]) != 0):
                for bottomSection in innerSection['Children']:
                    links.append(bottomSection["URL"])
            else:
                links.append(innerSection["URL"])
    print(links)

    for link in links:
        departmentList = link.split("/")[2 : 4]
    soup = BeautifulSoup(s.get('https://www.paknsaveonline.co.nz/category/fresh-foods-and-bakery', cookies=s.cookies.get_dict()).content)
    print(s.cookies.get_dict()["STORE_ID"])
    # print(soup)

    return links

def runSections(s, links):
    base = 'https://www.paknsaveonline.co.nz'
    for link in links:
        soup = BeautifulSoup(s.get(base + link + "?page=1").content)
        pageButtons = (soup.findAll("a", {"class": "btn btn--tertiary btn--large fs-pagination__btn"}))
        maxPage = soup.find("div", {"class": "fs-product-filter__item u-color-half-dark-grey u-hide-down-l"}).text.split(" ")[-2]
        maxPage = soup.find("p", {"class": "u-color-half-dark-grey"}).text.split(" ")[-2]
        maxPage = soup.find("p", {"class": "fs-pagination__info"}).text.split(" ")[-2]

        maxPage = math.ceil((int(maxPage) / 20))
        departmentList = link.split("/")[2 : 4]
        for page in range(1, maxPage + 1):
            url = base + link + "?pg=" + str(page)
            print(url)
            scrapeKeywords(s, url)



def scrapeKeywords(s, url):
    soup = BeautifulSoup(s.get(url).content)
    items = soup.findAll("div", {"class": "fs-product-card"})
    resultsList = []
    for iii in items:
        productDetailsDict = json.loads(iii.find("div", {"class": "js-product-card-footer fs-product-card__footer-container"})['data-options'])
        productOutput = [iii.find("p", {"class": "u-color-half-dark-grey u-p3"}).text]
        productOutput += [(productDetailsDict['productId']), productDetailsDict['productName']]

        priceSpec = productDetailsDict['ProductDetails']

        productOutput.append(priceSpec['PriceMode'])
        if priceSpec['HasMultiBuyDeal']:
            productOutput += [priceSpec['MultiBuyQuantity'], priceSpec['MultiBuyPrice'] ]
        else:
            productOutput += [None, priceSpec['PricePerItem'] ]
        resultsList.append(productOutput)
    print(resultsList)


def run():
    s = requests.Session()
    # scrapeKeywords(s, 'https://www.paknsaveonline.co.nz/category/fresh-foods-and-bakery?pg=2')
    # return
    links = getUrlLinks(s)
    return
    runSections(s, links)


run()
