create table products2 (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
name varchar(80),
brand varchar(40),
unit varchar(20),
origPrice Decimal(5,2),
salePrice Decimal(5,2),
pkgType varchar(20),
volSize varchar(20),
saleType varchar(25),
minAmount int,
type varchar(25)
);


