from selenium import webdriver
from bs4 import BeautifulSoup
#import pandas as pd
import urllib.request
import time
import re
import random
def urlToSoup(url):
    try:
        page = urllib.request.urlopen(url)
    except:
        print("An error occured.")
    return page

def generateDictonary(url, dictt):
    page = urlToSoup(url)
    soup = BeautifulSoup(page)
    products = soup.find(id = "product-list")
    for a in products.findAll('div', {"class" : {"gridProductStamp gridStamp"}}):
        if a.find('img', {"src" : {"/Content/PromotionTags/badge-onecard.png"}}) != None: 
            pricing = priceOfOneSpecial(a)
        elif a.find('img', {"src" : {"/Content/PromotionTags/badge-special.png"}}) != None: 
	    #there is a bonus play image in the breakfast foods
            pricing = priceOfSpecial(a)
        else:
            pricing = priceOfMulti(a)
        productName = a.find('h3', {"class" : {"gridProductStamp-name"}}).text.strip()
        dictt[productName] = pricing
    nextPage = soup.find('li', {"class" : {"next"}}).find('a', href=True)
    if nextPage != None:
        time.sleep(random.random() * 10)
        nextPageNum = nextPage['href'][-1]
        url = url[: -1]
        url += nextPageNum
        print(url)
        printer(dictt)
        generateDictonary(url, dictt)
    else:
        printer(dictt)
        
def printer(dictt):
    #for key,val in sorted(dictt.items(), key = lambda x : (x[1][0]/x[1][1]), reverse=True):
    for key,val in dictt.items():
        print("{}\n${:.2f} down to ${:.2f}\n${:.2f} off, {:.2f}% markdown"
                .format(key,val[0], val[1], val[0]-val[1], val[0]/val[1]))
        if val[2] != 1.0:
            print("Multibuy {} for ${}".format(val[2], val[1] * val[2]))
        print()

def priceOfOneSpecial(a):
    old = a.find('div', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:]
    new = a.find('span', {"class" : {"gridProductStamp-price club-text-colour din-medium"}}).text.strip().split()[-2][1:]
    unit = a.find('span', {"class" : {"gridProductStamp-price club-text-colour din-medium"}}).text.strip().split()[-1]
    pricePerKg = -1
    weight = -1
    if unit == "kg":
        pricePerKg = new
        weight = 1.0

    return [float(old), float(new), 1.0, pricePerKg, weight]    
def priceOfSpecial(a):
    old = a.find('span', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:]
    new = a.find('span', {"class" : {"gridProductStamp-price savings-text din-medium"}}).text.strip().split()[-2][1:]
    unit = a.find('span', {"class" : {"gridProductStamp-price savings-text din-medium"}}).text.strip().split()[-1]
    pricePerKg = -1
    weight = -1
    if unit == "kg":
        pricePerKg = new
        weight = 1.0
    return [float(old), float(new), 1.0, pricePerKg, weight]

def priceOfMulti(a):
    multiAmount = a.find('div', {"class" : {"multi-buy-award-quantity"}}).text.strip().split()[0]
    multiPrice = a.find('div', {"class" : {"multi-buy-award-value"}}).text.strip().split()[0]
    new = float(multiPrice) / float(multiAmount)
    old = a.find('div', {"class" : {"gridProductStamp-price din-medium"}}).text.strip().split("\xa0")[0][1:]
    return [float(old), new, float(multiAmount), -1, -1]

    


def main():
    url = "https://shop.countdown.co.nz/shop/specials/bakery?page=1"
    dictt = {}
    generateDictonary(url, dictt)
def main2():
    url = 'https://shop.countdown.co.nz/shop/specials/meat?page=1'
    file1 = open("scrub.txt","w")#write mode 
    file1.write(str(BeautifulSoup(urllib.request.urlopen(url))) )
    file1.close() 

main2()

