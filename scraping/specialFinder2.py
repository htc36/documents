from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
#import pandas as pd
import urllib.request
import time
import re
import random
import sys
import csv
import mysql.connector
from mysql.connector import Error

def urlTo120(url,driver):
#    inputBox = driver.find_element_by_xpath('//*[@id="PostcodeSuburb"]')
#    inputBox.clear()
#    inputBox.send_keys('riccart')
#    time.sleep(random.uniform(3,8))
#    driver.find_element_by_xpath('//*[@id="ui-id-2"]/li[1]').click()
#    time.sleep(random.uniform(2,5))
#    driver.find_element_by_xpath('//*[@id="content-panel"]/div/form/div[2]/input').click()
#    time.sleep(random.uniform(2,4))
    url = "https://shop.countdown.co.nz/shop/specials/meat?page=1"
    driver.get(url)
    amount_selector = '//*[@id="pageSize"]'
    button120 = '//*[@id="pageSize"]/option[3]'
    driver.find_element_by_xpath(amount_selector).click()
    driver.find_element_by_xpath(button120).click()
    time.sleep(1)

    return driver

def generateDictonary(url, tuplistt, driver, typee, connection):
   # time.sleep(random.uniform(3,7))
    scroll_down(driver)
    soup = BeautifulSoup(driver.page_source)
    products = soup.find(id = "product-list")
    for a in products.findAll('div', {"class" : {"gridProductStamp gridStamp"}}):
        if a.find('img', {"src" : {"/Content/PromotionTags/badge-onecard.png"}}) != None: 
            pricing = priceOfOneSpecial(a)
        elif a.find('img', {"src" : {"/Content/PromotionTags/badge-special.png"}}) != None: 
	    #there is a bonus play image in the breakfast foods
            pricing = priceOfSpecial(a)
        elif (a.find('div', {"class" : {"multi-buy-award-quantity"}})) != None:
            pricing = priceOfMulti(a)
        elif a.find('img', {"src" : {"/Content/PromotionTags/F2009_Trolley_Dash_Bonus.png"}}) != None: 
            pricing = priceOfSpecial(a)
        else: 
            pricing = [0, 1, 0, 0]
        productName = a.find('h3', {"class" : {"gridProductStamp-name"}}).text.strip()
        if pricing[3] == -1.0:
            pricing[3] = findPPKG(productName)

        tup = (productName, pricing[0], pricing[1], pricing[2], pricing[3], typee)
        print("the amount of items in current cache ready to be added:")
        tuplistt.append(tup)
        print(len(tuplistt))
    nextPage = None
    try:
        nextPage = soup.find('li', {"class" : {"next"}}).find('a', href=True)
        print(nextPage)
    except:
        print("can't find next page so has reached the end")
    if nextPage != None:
        print(url)
        nextPageNum = nextPage['href'][-1]
        url = url[: -1]
        url += nextPageNum
        driver.get(url)
        time.sleep(random.uniform(1,2))
        generateDictonary(url, tuplistt, driver, typee, connection)
    else:
        print("no more pages in food type, adding to database.... items to add:")

        #listt = sorted(listt, key = lambda x : (x[1]/x[2]), reverse=True)
        #with open("sheIsAllOverRedRover2.csv", "a", newline="") as f:
        #    writer = csv.writer(f)
        #    writer.writerows(listt)
        print(tuplistt)
        addToDatabase(tuplistt, connection)
        #printer(dictt)

def addToDatabase(listt, connection):
        cursor = connection.cursor()
        query = "INSERT INTO itemsTest (name, oldPrice, newPrice, minAmount, amount, type) VALUES (%s, %s, %s, %s, %s, %s)"
        cursor.executemany(query, listt)
        connection.commit()
        cursor.close()
        
def printer(dictt):
    #for key,val in sorted(dictt.items(), key = lambda x : (x[1][0]/x[1][1]), reverse=True):
    for key,val in dictt.items():
        print("{}\n${:.2f} down to ${:.2f}\n${:.2f} off, {:.2f}% markdown"
                .format(key,val[0], val[1], val[0]-val[1], val[0]/val[1]))
        if val[2] != 1.0:
            print("Multibuy {} for ${}".format(val[2], val[1] * val[2]))
        print()

def priceOfOneSpecial(a):
    old = a.find('div', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:]
    new = a.find('span', {"class" : {"gridProductStamp-price club-text-colour"}}).text.strip().split()[-2][1:]
    unit = a.find('span', {"class" : {"gridProductStamp-price club-text-colour"}}).text.strip().split()[-1]
    pricePerKg = -1.0
    if unit == "kg":
        pricePerKg = 1.0
    return [float(old), float(new), 1.0, pricePerKg]    

def priceOfSpecial(a):
    old = a.find('span', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:]
    new = a.find('span', {"class" : {"gridProductStamp-price savings-text"}}).text.strip().split()[-2][1:]
    unit = a.find('span', {"class" : {"gridProductStamp-price savings-text"}}).text.strip().split()[-1]
    pricePerKg = -1.0
    if unit == "kg":
        pricePerKg = 1.0
    return [float(old), float(new), 1.0, pricePerKg]    

def priceOfMulti(a):

    multiAmount = a.find('div', {"class" : {"multi-buy-award-quantity"}}).text.strip().split()[0]
    multiPrice = a.find('div', {"class" : {"multi-buy-award-value"}}).text.strip().split()[0]
    new = float(multiPrice) / float(multiAmount)
    try:
        old = a.find('div', {"class" : {"gridProductStamp-price"}}).text.strip().split("\xa0")[0][1:]

    except:
        print(multiPrice)
        try:
            old = a.find('span', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:]
            test = float(old)
        except:
            print(a.find('div', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:])
            print(a.find('div', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split())
            old = a.find('div', {"class" : {"gridProductStamp-subPrice"}}).text.strip().split()[-1][1:]

    return [float(old), new, float(multiAmount), -1.0]
def findPPKG(name):
    unit = name.split(" ")[-1]
    try:
        if "kg" in unit:
            ans = float(unit[:-2])
        elif "g" == unit[-1]:
            ans = float(unit[:-1]) / 1000
        elif re.match(r'^[0-9]+g$',name.split(" ")[-2]):
            ans = float(name.split(" ")[-2][: -1]) / 1000.0  
        #elif unit[-2:] == "pk":
        #    ans = float(unit[:-2])
        elif re.match('^[0-9]+l', unit):
            ans = float(unit[:-1])
        elif re.match('^[0-9]+ml', unit):
            ans = float(unit[:-2]) / 1000.0
        else:
            ans = -1.0
    except:
        ans= -111
    return ans

def scroll_down(driver):
    """A method for scrolling the page."""
    # Get scroll height.
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        # Scroll down to the bottom.
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wait to load the page.
        time.sleep(2)
        # Calculate new scroll height and compare with last scroll height.
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            time.sleep(2)
            break
        last_height = new_height


def databaseConnect():
    try:
        connection = mysql.connector.connect(host='localhost', database='tester', user='root', password='Dingding29er')
        if connection.is_connected():
            db_info = connection.get_server_info()
            print("Connected to Mysql", db_info)
            return connection
        else:
            print("not connected")
    except Error as e:
        print("Error", e)
def databaseConnectServer():

    try:
        connection = mysql.connector.connect(host='45.76.124.20', database='specials', user='root', password='Cc5c8cac59')
        if connection.is_connected():
            db_info = connection.get_server_info()
            print("Connected to Mysql", db_info)
            return connection
        else:
            print("not connected")
    except Error as e:
        print("Error", e)

def main():
    options = Options()
    options.headless = True
    driver = webdriver.Chrome("/usr/local/bin/chromedriver2")
    url = "https://shop.countdown.co.nz/shop/showaddresses"
    driver.get(url)
    driver = urlTo120(url,driver)
    connection = databaseConnectServer()
    #dictt = {}
    locations = ["christmas", "bakery", "deli-chilled-foods", "meat", "seafood", "baby-care", "baking-cooking", "biscuits-crackers", "breakfast-foods", "canned-prepared-foods", "chocolate-sweets-snacks", "cleaning-homecare", "drinks-hot-cold", "frozen-foods", "health-wellness", "home-kitchenware", "meal-ingredients", "office-entertainment", "personal-care", "pet-care"]
    
    for iii in locations:
        url = "https://shop.countdown.co.nz/shop/specials/" + iii + "?page=1"
        driver.get(url)
        generateDictonary(url, [], driver, iii, connection)
main()


