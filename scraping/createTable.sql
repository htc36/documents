create table itemsTest (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
name varchar(100),
oldPrice Decimal(5,2),
newPrice Decimal(5,2),
minAmount int,
amount Decimal(6,3),
type varchar(50));


