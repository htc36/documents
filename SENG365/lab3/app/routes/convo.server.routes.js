const convo = require('../controllers/convo.server.controller');
const message = require('../controllers/message.server.controller');


module.exports = function(app) {
    app.route('/api/convo')
        .get(convo.list)
        .post(convo.create);

    app.route('/api/convo/:convoId')
        .get(convo.read)
        .put(convo.update)
        .delete(convo.delete);

    app.route('/api/conversations/:convo_id/messages')
        .get(message.list)
        .post(message.create);

    /*  app.route('/api/conversations/:id/messages/:id')
          .get(message.read);
  */
};
