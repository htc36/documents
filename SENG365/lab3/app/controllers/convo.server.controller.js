const Convo = require('../models/convo.server.model');

exports.list = async function(req, res) {
    try {
        const result = await Convo.getAll();
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR getting convos ${err}`);
    }
};

exports.create = async function(req, res) {
    try {
        let convo_data = {
            'convo_name': req.body.convo_name
        };

        let convo = convo_data['convo_name'].toString();
        let values = [
            [convo]
        ];

        const result = await Convo.insert(values);
        res.status(201)
            .send(`Inserted ${req.body.convo_name} at id ${result}`);
    } catch (err) {
        res.status(500)
            .send(`ERROR posting user ${err}`);
    }
};

exports.read = async function(req, res) {
    try {
        const id = req.params.convoId;
        const result = await Convo.getOne(id);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR fetching convo ${err}`);
    }
};

exports.update = async function(req, res) {
    try {
        const id = req.params.convoId;
        const name = req.body.convo_name;
        const result = await Convo.alter(id, name);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR fetching user ${err}`);
    }
};

exports.delete = async function(req, res) {
    try {
        const id = req.params.convoId;
        const result = await Convo.remove(id);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR fetching user ${err}`);
    }
};

exports.convoById = async function(req, res) {
    return null;
};
