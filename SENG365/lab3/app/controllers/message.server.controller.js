const Message = require('../models/message.server.model');

exports.list = async function(req, res) {
    try {

        console.log("booty");
        const id = req.params.convo_id;
        const result = await Message.getAll(id);
        res.status(200)
            .send(result);
    } catch (err) {
        res.status(500)
            .send(`ERROR getting message ${err}`);
    }
};

exports.create = async function(req, res) {
    try {
        let message_data = {
            'message': req.body.some_message
        };

        let message = message_data['message'].toString();
        let values = [
            [message]
        ];

        const result = await Message.insert(values);
        res.status(201)
            .send(`Inserted ${req.body.message} at id ${result}`);
    } catch (err) {
        res.status(500)
            .send(`ERROR posting user ${err}`);
    }
};
