const db = require('../../config/db');

exports.getAll = async function() {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_users';
    const [rows, fields] = await connection.query(q);
    return rows;
};

exports.getOne = async function(userId) {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_users WHERE user_id = ?';
    const [rows, _] = await connection.query(q, userId);
    return rows;
};

exports.insert = async function(username) {
    let values = [username];
    const connection = await db.getPool().getConnection();
    const q = 'INSERT INTO lab2_users (username) VALUES (?)';
    const [result, _] = await connection.query(q, values);
    console.log(`Inserted user with id ${result.insertId}`);
    return result.insertId;
};

exports.alter = async function(userId, username) {
    const connection = await db.getPool().getConnection();
    const q = 'Update lab2_users set username = ? WHERE user_id = ?';
    const values = [ username, userId ];
    await connection.query(q, values);
   //return rows;
};

exports.remove = async function(userId) {
    const connection = await db.getPool().getConnection();
    const q = 'delete from lab2_users WHERE user_id = ?';
    const values = [ userId ];
    await connection.query(q, values);
};
