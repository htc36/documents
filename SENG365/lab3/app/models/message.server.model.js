const db = require('../../config/db');

exports.getAll = async function(convo_id) {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_messages where convo_id = ?';
    const [rows, _] = await connection.query(q, [convo_id]);
    console.log(rows);
    return rows;
};


exports.getOne = async function(convoId) {
    const connection = await db.getPool().getConnection();
    const q = 'SELECT * FROM lab2_conversations WHERE convo_id = ?';
    const [rows, _] = await connection.query(q, convoId);
    return rows;
};

exports.insert = async function(convo_name) {
    let values = [convo_name];
    const connection = await db.getPool().getConnection();
    const q = 'INSERT INTO lab2_conversations (convo_name) VALUES (?)';
    const [result, _] = await connection.query(q, values);
    console.log(`Inserted convo with id ${result.insertId}`);
    return result.insertId;
};

exports.alter = async function(convoId, convo_name) {
    const connection = await db.getPool().getConnection();
    const q = 'Update lab2_conversations set convo_name = ? WHERE convo_id = ?';
    const values = [ convo_name, convoId ];
    await connection.query(q, values);
    //return rows;
};

exports.remove = async function(convoId) {
    const connection = await db.getPool().getConnection();
    const q = 'delete from lab2_conversations WHERE convo_id = ?';
    const values = [ convoId ];
    await connection.query(q, values);
};
