const conversations = require('../controllers/conversation.server.controller');
const messages = require('../controllers/messages.server.controller');

module.exports = function(app) {
    app.route('/api/conversations')
        .get(conversations.list)
        .post(conversations.create);

    app.route('/api/conversations/:convo_id')
        .get(conversations.read)
        .put(conversations.update)
        .delete(conversations.delete);

    app.route('/api/conversations/:convo_id/messages')
        .get(messages.list)
        .post(messages.create);

    app.route('/api/conversations/:convo_id/messages/:msg_id')
        .get(messages.read);
}
