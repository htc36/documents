const express = require('express');
const app = express();

app.get('/', function(req, res) {
    res.send('HTTP request: GET /');
});

app.post('/', function(req, res) {
    res.send('HTTP request: POST /');
});

app.put('/', function(req, res) {
    res.send('HTTP request: PUT /');
});

app.delete('/', function(req, res) {
    res.send('HTTP request: DELETE /');
});


app.listen(3000, function() {
    console.log("example app listening on port 3000");
});

app.use(function(req, res, next) {
    res.status(404).send("404 not found");
});


