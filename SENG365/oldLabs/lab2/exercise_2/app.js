const mysql = require('mysql2/promise');
require('dotenv').config({path: "../.env"});
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

const pool = mysql.createPool({
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATA_BASE
});

async function getUsers(req, res) {
    console.log('Request to get all users from the db');

    try {
        const connection = await pool.getConnection();
        console.log('Successfully connected to the db');

        const [rows, fields] = await connection.query('select * from lab2_users');
        res.status(200)
            .send(rows);
    } catch (err) {
        res.status(500)
            .send(`Error getting users: ${err}`);
    }
}

async function postUser(req, res) {
    console.log('Request to add new user to db');

    try {
        const connection = await pool.getConnection();
        const sql = 'insert into lab2_users (username) values (?)';
        const values = [req.body.username];

        await connection.query(sql, [values]);
        res.status(201)
            .send(`User successfully added to the db`);
    } catch(err) {
        res.status(500)
            .send(`Error posting user: ${err}`)
    }
}

app.get('/users', getUsers);
app.post('/users', postUser);

async function getConversations(req, res) {
    console.log('Request to get all conversations');
    try {
        const connection = await pool.getConnection();
        const sql = 'select * from lab2_conversations';
        const values = [req.body.username];

        const [rows, fields] = await connection.query(sql, [values]);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error getting all conversations: ${err}`)
    }
}


app.get('/conversations', getConversations);

async function getSingleConversation(req, res) {
    console.log('Request to get a single conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'select * from lab2_conversations where convo_id = ?';
        const values = [req.params.id];

        const [rows, fields] = await connection.query(sql, [values]);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error getting a single conversation: ${err}`)
    }
}

app.get('/conversations/:id', getSingleConversation);

async function postConversation(req, res) {
    console.log('Request to post a single conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'insert into lab2_conversations (convo_name) values (?)';
        const values = [req.body.convo_name];

        const [rows, fields] = await connection.query(sql, [values]);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error posting a single conversation: ${err}`)
    }
}

app.post('/conversations', postConversation);

async function modifySingleConversation(req, res) {
    console.log('Request to modify a single conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'update lab2_conversations set convo_name = ? where convo_id = ?';
        const values = [req.body.convo_name, req.params.id];

        const [rows, fields] = await connection.query(sql, values);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error modifying a single conversation: ${err}`)
    }
}

app.put('/conversations/:id', modifySingleConversation);

async function deleteSingleConversation(req, res) {
    console.log('Request to delete a single conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'delete from lab2_conversations where convo_id = ?';
        const values = [req.params.id];

        const [rows, fields] = await connection.query(sql, values);
        res.status(201)
            .send('Conversation deleted');
    } catch(err) {
        res.status(500)
            .send(`Error deleting a single conversation: ${err}`)
    }
}

app.delete('/conversations/:id', deleteSingleConversation);

async function getMessagesOfConversation(req, res) {
    console.log('Request to get all messages inside a conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'select * from lab2_conversations where convo_id = ?';
        const values = [req.params.id];

        const [rows, fields] = await connection.query(sql, values);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error retrieving messages from a conversation: ${err}`)
    }
}
app.get('/conversations/:id/messages', getMessagesOfConversation);

async function getSingleMessageOfConversation(req, res) {
    console.log('Request to retrieve a message inside a conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'select * from lab2_messages where convo_id = ? and message_id = ?';
        const values = [req.params.id, req.params.m_id];

        const [rows, fields] = await connection.query(sql, values);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error retrieving a message from a conversation: ${err}`)
    }
}

app.get('/conversations/:id/messages/:m_id', getSingleMessageOfConversation);

async function postNewMessageToConversation(req, res) {
    console.log('Request to post a message inside a conversation');
    try {
        const connection = await pool.getConnection();
        const sql = 'insert into lab2_messages (convo_id, message, user_id) values (?,?,?)';
        const values = [req.params.id, req.body.message, req.body.user_id];

        const [rows, fields] = await connection.query(sql, values);
        res.status(201)
            .send(rows);
    } catch(err) {
        res.status(500)
            .send(`Error posting a message to a conversation: ${err}`)
    }
}

app.post('/conversations/:id/messages', postNewMessageToConversation);


const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Listen on port: ${port}`);
});
