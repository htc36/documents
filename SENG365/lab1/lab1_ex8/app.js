const express = require('express');
const app = express();
const data = require('./users.json');
const users = data.users;
const bodyParser = require('body-parser');
app.use(bodyParser.json());
console.log(users);

app.get('/users', (req, res) => {
    res.status(200).send(users);
});
app.get('/users/:id', (req, res) => {
    const id = req.params.id;
    let response = `No user with id ${id}`
    for (const user of users) {
        if (id == user.id) {
            response = user;
            break;
        }
    }
    res.status(200)
        .send(response);
});

app.post('/users', (req, res) => {
    const newUser = req.body;
    users.push(newUser);
    res.status(201)
        .send(users);
}); 
app.put('/users/:id', (req, res) => {
    const id = req.params.id;
    const updatedUser = req.body;
    for (const user of users) {
        if (id == user.id) {
            const index = users.indexOf(user);
            users[index] = updatedUser;
            break;
        }
    }
    res.status(200)
        .send(updatedUser);
});
app.delete('/users/:id', (req, res) => {
    const id = req.params.id;
    for (const user of users) {
        if (id == user.id) {
            const index = users.indexOf(user);
            users.splice(index, 1);
        }
    }
    res.send(users);
});

app.get('/followers/:id', (req, res) => {
    const id = req.params.id;
    let response = `No user with id ${id}`
    for (const user of users) {
        if (id == user.id) {
            const index = users.indexOf(user)
            response = users[index].followers;
            break;
        }
    }
    res.status(200)
        .send(response);
});
app.put('/followers/:id', (req, res) => {
    const id = req.params.id;
    for (const user of users) {
        if (id == user.id) {
            const index = users.indexOf(user);
            users[index].followers = req.body;
            break;
        }
    }
    res.status(200)
        .send(users);
});
app.delete('/followers/:id', (req, res) => {
    const id = req.params.id;
    for (const user of users) {
        if (id == user.id) {
            const index = users.indexOf(user);
            users[index].followers = []
        }
    }
    res.send(users);
});


app.listen(3000, function() {
    console.log("example app listening on port 3000");
});

app.use(function(req, res, next) {
    res.status(404).send("404 not found");
});


