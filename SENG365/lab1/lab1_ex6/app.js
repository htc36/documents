const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('HTTP request: GET /');
});
app.post('/', (req, res) => {
    res.send('HTTP request: post /');
});
app.put('/', (req, res) => {
    res.send('HTTP request: put /');
});
app.delete('/', (req, res) => {
    res.send('HTTP request: delete /');
});

app.listen(3000, function() {
    console.log("example app listening on port 3000");
});

app.use(function(req, res, next) {
    res.status(404).send("404 not found");
});


