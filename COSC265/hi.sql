create table VEHICLE 
(plates varchar(6) not null primary key,
year char(4),
eng_no varchar(9) not null unique,
ch_no varchar(7) not null unique,
type char(1) constraint check_type check (type in ('p', 'r', 't','l', 'm')),
make varchar(10),
model varchar(10),
foreign key (make, model) references tanja.VEHICLE_TYPE);


create table OWNS
(plates varchar(6) not null references VEHICLE,
ownerid varchar(8) not null,
purchase_date varchar(11),
drr varchar(10),
daresold varchar(12),
primary key (plates,ownerid));

create table color 
(
plates varchar(6) not null references VEHICLE,
color char(10)
);

create table REGISTRATION 
(plates varchar(6) not null references VEHICLE,
emp varchar(8),
reg_org varchar(4),
reg_date varchar(12),
country varchar(12),
drr varchar(6),
amount float(125),
primary key (plates, emp, reg_org, reg_date));


insert into owner
values('BA789256',58743344,'Anna','B','Simmons',
 '21 Aorangi Rd Christchurch','21-jan58','f',null,'3381275');


insert into VEHICLE  values('QJD123','2003','1686617','655239','p','ford','telstar');




