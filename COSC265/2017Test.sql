/*
--Q1a
Create table JobSkill
(
    j_name varchar(20),
    s_code char REFERENCES Skill(S_code),
    rank smallint,
    Primary key(J_name, S_code)
);
*/
--Q1b
--INSERT INTO JobSkill VALUES ('SWDeveloper', 'C', 2);
--INSERT INTO JobSkill VALUES ('SWDeveloper', 'D', 3);
--INSERT INTO JobSkill VALUES ('SWDeveloper', 'T', 1);
--INSERT INTO JobSkill VALUES ('Lifeguard', 'F', 2);
--INSERT INTO JobSkill VALUES ('Lifeguard', 'S', 1);
/*
--Q1 C
select count(*)
from jobskill
*/
/*
--Q1 d
update creature
set c_name = 'Smeagol'
where c_name = 'Gollum'
*/
--Q2 a
/*
select distinct C_name
from Creature
    natural join achievement A
    natural join Skill S
    where s_weight <= 0.5
    order by C_name;
*/
--Q2 b
/*
select *
from creature
where c_id in
    (select c_id
    from achievement
    where s_code in 
        (select s_code
        from skill
        where s_weight <= 0.5));

*/
/*

--q2 c
select c_type, count(*), avg(score)--, c_id, s_code, s_weight
from creature
    natural join achievement A
group by c_type--, c_id;
order by count(*) desc;
*/
--q2 d
/*
select distinct a1.s_code, a2.s_code
from achievement a1
    
    join achievement a2 on a1.c_id = a2.c_id --dkuplicated list 
where a1.s_code < a2.s_code and a1.score =2 and a2.score = 2;
*/
/*
--q2 e
select creature.c_id, count(a.s_code)
from creature
    left outer join achievement A on (creature.c_id = A.c_id)
group by creature.c_id
order by c_id 
*/

--q3 a
/*
create or replace view  ACH_VIEW
as  select A1.C_ID, C1.C_NAME, C1.C_TYPE, A1.S_CODE, A1.SCORE, S1.S_DESC
from achievement A1
    join CREATURE C1 on A1.C_ID = C1.C_ID
    join SKILL S1 on A1.S_CODE = S1.S_CODE;
*/
/*
--q3 b
select *
from ACH_VIEW
where C_ID <= 4;
*/
--q4 c
--create or replace trigger insert_creature
--instead of insert on ach_view
--Begin
--    insert into creature ( C_id, C_name, C_type )
--        values (:new.c_id, :new.C_name, :new.C_type);
--    insert into achievement (C_id, S_code, Score)
--        values (:new.c_id, :new.S_code, :new.Score);
--END;




----------------------------2017 again----------------------------------

--create table JOBSKILL
--(
--job_name varchar(20), 
--s_code char references skill(s_code),
--rank_value int ,
--constraint jobskill_pk primary key (job_name, s_code)
--)
--
--INSERT INTO JobSkill VALUES ('SWDeveloper', 'C', 2);
--INSERT INTO JobSkill VALUES ('SWDeveloper', 'D', 3);
--INSERT INTO JobSkill VALUES ('SWDeveloper', 'T', 1);
--INSERT INTO JobSkill VALUES ('Lifeguard', 'F', 2);
--INSERT INTO JobSkill VALUES ('Lifeguard', 'S', 1);

select count(*)
from jobskill
    

    
    


