/* Inserts for the Vehicle_Type table */
insert into vehicle_type values('daihatsu','charade','p',4,1.0,1000);
insert into vehicle_type values('mazda',121,'p',5,1.2,1000);
insert into vehicle_type values('ford','telstar','p',5,2.0,2500);
insert into vehicle_type values('VW','golf','d',5,1.5,1500);
insert into vehicle_type values('mazda',323,'p',5,2.3,2000);
insert into vehicle_type values('daihatsu','4wd','d',6,3.0,3000);
insert into vehicle_type values('honda','jazz','p',5,1.5,1500);
insert into vehicle_type values('honda','civic','p',5,1.8,1800);
insert into vehicle_type values('honda','accord','p',5,2,2000);

/* Inserts for the Owner table */
insert into owner values('BA789256',58743344,'Anna','B','Simmons','21 Aorangi Rd Christchurch','21-jan-58','f',null,'3381275');
insert into owner values('GG847264',48673675,'Peter','H','George','10a Lyndon St Christchurch','11-nov-65','m',null,'3213213');
insert into owner values('HA385767',38573657,'Steve','N','Smith','263 Memorial Av Christchurch','28-feb-72','m',null,'3113111');
insert into owner values('FF849583',38576767,'Jane','A','Austin','33 Bateman St Christchurch','21-jan-58','f',null,'3111141');
insert into owner values('HD837423',20229382,'George','K','Peterson','Wairekei St Christchurch','09-sep-92','m',null,'3923925');
insert into owner values('HD293847',09876273,'Mary','K','Lin','14 Main St Toytown','16-jun-40','f',null,'2534235');
insert into owner values('UJ203954',10293854,'Gwenda','F','Bosley','88 Alpha Av Acity','31-jan-55','f',null,'3123456');
insert into owner values('AU293857',20220222,'Richard','I','Wise','30 Shortland St TVCity','02-dec-56','m',null,'3334445');
insert into owner values('HA928375',20323888,'King','A','Alan','5 High St Christchurch','09-mar-48','m',null,'3839301');
insert into owner values('HD543235',33033033,'King','H','Jason','5 High St Christchurch','17-jul-65','m',null,'3839301');
insert into owner values('IA192837',30192847,'Minnie','N','Mouse','2194 Sunset Bl Wellington','01-jan-89','f','Disney studios','9992837');
insert into owner values('DBA256',38799344,'Jennie','B','Martin','21 Creyke Christchurch','21-jan-98','f',null,'3381275');
insert into owner values('JAL264',98622675,'Peter','H','Holland','10a Elisabeth St Christchurch','11-nov-99','m',null,'3213213');
insert into owner values('GRW858',38573284,'Steven','N','Roberts','263 Memorial Av Christchurch','28-feb-96','m',null,'3113111');

/* Inserts for the Employee table */
insert into employee values('Anna','B','Simmons',58743344,'f','21-jan-58','101a',1352,'22-feb-2010');
insert into employee values('Timothy','S','Sanders',38473342,'m','21-may-1970','101b',1352,'17-mar-2009');
insert into employee values('Michael','F','Forman',21321322,'m','09-nov-47',100,1352,'12-may-2018');
insert into employee values('Angela','S','Tay',91382743,'f','10-oct-53',13,1303,'15-jun-2009');
insert into employee values('John','F','Right',91837243,'m','01-oct-56',12,1303,'01-oct-2015');

/* Inserts for the REG_ORG table */
insert into REG_ORG values(1352,'Riccarton Rd',231,'Christchurch',58743344);
insert into REG_ORG values(1303,'Main North Rd',19,'Christchurch',91382743);

/* Change the EMPLOYEE table to add the foreign key */
alter table EMPLOYEE
add foreign key (reg_org) references REG_ORG;
