#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
    char* name;
    int age;
    double height;
} Person;

Person* newPerson(char* name, int age, double height) 
{
    Person* new = malloc(sizeof(Person));
    new->name = malloc(strlen(name) + 1);
    strncpy(new->name, name, strlen(name));
    new->name[strlen(name)] = '\0';
    new->age = age;
    new->height = height;
    return new;
}

void freePerson(Person* person){
    free(person->name);
    free(person);
}
int main(void) {
    Person* employee = newPerson("Billy", 30, 1.68);
    printf("%s is age %d and is %.2f m tall\n", employee->name, employee->age, employee->height);
    freePerson(employee);
}



