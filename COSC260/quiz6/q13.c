#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char* skipping(const char* s)
{
    int index =0;
    int slots = (strlen(s)/2) + 1;
    if (strlen(s) % 2 != 0) {
        slots++;
    }
    char* skipper = malloc(slots);
    int counter = 0;
    while (s[counter] != '\0') {
        if (counter % 2 == 0) {
            skipper[index] = s[counter];
            index++;
        }
        counter++;
    }
    skipper[index] = '\0';
    return skipper;
}
int main(void)
{
    char* s = skipping("A0B1C2D");
    printf("%s\n", s);
    free(s);
}
