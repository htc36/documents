#include <stdio.h>
#include <stdlib.h>

int* ramp(int n)
{
    int* data = malloc(n * sizeof(int));
    for (int index = 0; index < n; index++) {
        data[index] = index + 1;
    }
    return data;
}

int main(void)
{
    int* data = ramp(5);
    for (int i = 0; i < 5; i++) {
        printf("%d ", data[i]);
    }
    free(data);
}
