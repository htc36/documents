#include <stdlib.h>
#include <stdio.h>

//Returns a pointer to the nth occurace of a integer c
//in the sting s

char* strchrn(char* s, int c, int n)
{
    char* pointer = s;
    int occurance = 0;

    if (n <= 0) {
        return NULL;
    }
    for (int i =0; pointer[i] != '\0'; i++) {
        if (*s == c) {
            occurance++;
            if (occurance == n) {
                return s;
            }
        }
        s++;
    }
    return NULL;
}


int main(void)
{
    char* line = "This is a string";
    for (int n = 1; n <= 5; n++) {
        char* ptr = strchrn(line, 'i', n);
        if (ptr != NULL) {
            int index = ptr - line;
            printf("Occurrence %d of 'i' found at index = %d\n", n, index);
        } else {
            printf("Occurrence %d of 'i' not found\n", n);
        }
    }
}
