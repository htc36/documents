/* Tempreture file proceessing
 * that prints out all tempretures
 * that satisfys a minimum tempreture
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void printTempRecordings(char* date, double temp);

//Process the data in the FILE* to find the stations name and prints it
void stationData(FILE* file, double minNum)
{
    int count = 0;
    char line[500];
    while (fgets(line, sizeof line, file)[0] != '\n') {
        if (count == 2) {
            char* commaPos = strchr(line, ',');
            int index = commaPos - line;
            line[index] = '\0';
            printf("Dates when %.1lf C was reached at %s\n\n", minNum, line);
            puts("   Date       MaxTemp");
        }
        count++;
    }
}
//Process all the temp recordings, until there is a new line
//Finds all the records that have a suffiecant tempreture
void dailyTempRecordings(FILE* file, double tempMin)
{
    char line[500];
    char* inputLine = fgets(line, sizeof line, file);
    int count = 0;
    while (inputLine[0] != '\n') {
        if (count > 1) {
            char* date = strchr(inputLine, ',');
            char* commaPos = strchr(++date,',');
            *commaPos = '\0';
            char* temp = ++commaPos;
            commaPos = strchr(commaPos, ',');
            *commaPos = '\0';
            if (atof(temp) >= tempMin) {
                printTempRecordings(date, atof(temp));
            }
            inputLine = fgets(line, sizeof line, file);
        } else {
            inputLine = fgets(line, sizeof line, file);
        }
        count ++;
    }
}
//Takes a pointer to a line and prints each attribute in the line
void printTempRecordings(char* date, double temp)
{
    char day[8];
    char month[8];
    char year[8];
    date[8] = '\0';
    date += 6;
    strncpy(day, date, 8);
    *date = '\0';
    date -= 2;
    strncpy(month, date,8);
    *date = '\0';
    date -= 4;
    strncpy(year, date, 8);
    printf("%02d/%02d/%d%9.1lf C\n", atoi(day), atoi(month), atoi(year), temp);
}

//Gets the arguments given by the user and checks them
//if all is the data can be processed
int main(int argc, char* argv[])
{
    if (argc != 3) {
        fprintf(stderr, "Usage: processtemps filename threshold\n");
        exit(0);
    }
    FILE* inputFile = fopen(argv[1], "r");
    if (inputFile == NULL) {
        fprintf(stderr, "File '%s' not found\n", argv[1]);
        exit(0);
    }
    stationData(inputFile, atof(argv[2]));
    dailyTempRecordings(inputFile, atof(argv[2]));


}



