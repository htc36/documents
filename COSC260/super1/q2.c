#include <stdlib.h>
#include <stdio.h>

void smoothData(int n, double data[])
{
    double saver = 0;
    double saver2 = 0;

    for (int iii = 0; iii < n; iii++) {
        if (iii == 0) {
            saver = (3*data[iii] + data[iii +1]) / 4;

        } else if (iii == n-1) {
            saver = (data[iii-1] + (3 * data[iii])) / 4;
            data[iii -1] = saver2;
            data[iii] = saver;
        } else {
            saver2 = (data[iii -1] + (2 *data[iii]) + data[iii + 1])/4;
            data[iii -1] = saver;
            saver = saver2;
            
        }
    }
}
int main(void)
{
    double data[] = {1.0, 2.0, 4.0, 4.0};
    smoothData(4, data);
    printf("Smoothed data: %.3lf", data[0]);
    for (int i = 1; i < 4; i++) {
        printf(", %.3lf", data[i]);
    }
    puts("");
}


