#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#define CRITICAL_HIGH 9.81
#define CRITICAL_LOW 5.0

int readDoubles(int n, double data[])

{
    int iii = 0;
    double value = 0;
    while (scanf("%lf", &value) != EOF && iii < n) {
        data[iii] = value;
        iii++;
    }
    return iii;

}

void smoothData(int n, double data[])
{
    double saver = 0;
    double saver2 = 0;

    for (int iii = 0; iii < n; iii++) {
        if (iii == 0) {
            saver = (3*data[iii] + data[iii +1]) / 4;

        } else if (iii == n-1) {
            saver = (data[iii-1] + (3 * data[iii])) / 4;
            data[iii -1] = saver2;
            data[iii] = saver;
        } else {
            saver2 = (data[iii -1] + (2 *data[iii]) + data[iii + 1])/4;
            data[iii -1] = saver;
            saver = saver2;

        }
    }
}

int main(void)
{
    double timer = 0;
    double highest_value_time = 0;
    double maxx = 0;
    bool low = false;
    int value = 0;
    double data[100000] = {0.0};
    value =  readDoubles(100000, data);
    printf("%d", value);
    smoothData(value, data);
    for (int iii = 0; iii < value; iii++) {
        if (data[iii] < CRITICAL_LOW) {
            low = true;
        }
        if (low == true && data[iii] > CRITICAL_HIGH) {
            printf("Acceleration of 9.81 m/sec^2 exceeded at t = %.2lf secs.\n", timer);
            low = false;
        }
        if (data[iii] > maxx) {
            highest_value_time = timer;
            maxx = data[iii];
        }

        timer += 0.01;
    }
    printf("\n");
    printf("Maximum acceleration: %.2lf m/sec^2 at t = %.2lf secs.", maxx, highest_value_time);

}


