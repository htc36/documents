#include <stdlib.h>
#include <stdio.h>


int readDoubles(int n, double data[])

{
    int iii = 0;
    double value = 0;
    while (scanf("%lf", &value) != EOF && iii < n) {
        data[iii] = value;
        iii++;
    }
    return iii;

}

int main(void)
{
    double data[5] = {0.0};
    int numRead = 0;

    numRead = readDoubles(4, data);
    printf("Read %d values:\n", numRead);
    for (int i = 0; i < numRead; i++) {
        printf("%0.3lf\n", data[i]);
    }
}


