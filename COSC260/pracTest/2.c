#include <stdio.h>
#include <stdlib.h>


typedef struct {
    int x;
    int y;
    int z;
} Vector3d;

Vector3d vector(int x, int y, int z)
{
    Vector3d answer;
    answer.x = x;
    answer.y = y;
    answer.z = z;
    return answer;
}

Vector3d vectorCrossProduct(Vector3d a, Vector3d b)
{
    Vector3d crossAnswer;
    crossAnswer.x = (a.x * b.z) - (a.z * b.y);
    crossAnswer.y = (a.z * b.x) - (a.x * b.z);
    crossAnswer.z = (a.x * b.y) - (a.y * b.x);
    return crossAnswer;
}



int main(void)
{
    Vector3d v1 = vector(1, 2, 3);
    Vector3d v2 = vector(-4, -6, 2);
    Vector3d v3 = vectorCrossProduct(v1, v2);
    printf("(%d, %d, %d)\n", v3.x, v3.y, v3.z);
}

