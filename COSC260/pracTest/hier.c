#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>


char* strchrn(char* s, int c, int n)
{
    int count = 0;
    while (*s != '\0') {
        if (*s == c) {
            count++;
            if (count == n) {
                return s;
            }
        }
        s++;
    }
    return NULL;
}



// Simple test of the student queue
int main(void)
{
    char* line = "This is a string";
    for (int n = 1; n <= 5; n++) {
        char* ptr = strchrn(line, 'i', n);
        if (ptr != NULL) {
            int index = ptr - line;
            printf("Occurrence %d of 'i' found at index = %d\n", n, index);
        } else {
            printf("Occurrence %d of 'i' not found\n", n);
        }
    }
}

