#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

void swap3(int* x, int* y, int* z)
{
    int saver = 0;
    int a = *x;
    int b = *y;
    int c = *z;
    printf("%d <= %d <= %d\n", c, b, a);
    while (c >  b ||  b  >  a) {

        if (a < b) {
            saver = a;
            a = b;
            b = saver;
            printf("%d <= %d <= %d\n", c, b, a);
        }
        if (c > b) {
            saver = b;
            c = b;
            b = saver;
        }
        puts("hi");
    }
    *x = a;
    *y = b;
    *z = c;
}
int main(void)
{
    int a = 10;
    int b = 0;
    int c = 7;
    swap3(&a, &b, &c);
    printf("%d <= %d <= %d\n", c, b, a);

}
