#include <stdio.h>

int tokenCopy(char* dest, const char* src, int destSize)
{
    int counter = 0;

    while (*src != '\0' && counter < destSize -1 && *src != ' ') {
        counter++;
        *dest = *src;
        dest++;
        src++;
    }
    *dest = '\0';
    return counter;
}

int main(void)

{
    char buff[5];
    int n = tokenCopy(buff, "This is a string", 5);
    printf("%d '%s'\n", n, buff);

}
