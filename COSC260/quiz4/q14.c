#include <stdio.h>

int myIndex(int data[], int* element)
{
    int* first = &data[0];
    int counter = 0;
    while (first != element) {
        element--;
        counter++;
    }
    return counter;



}


int main(void)
{
    int data[30];
    int* p = &data[11];
    printf("Index is %d\n", myIndex(data, p));

    char* s = "Ummm: ";
    while (*s) {
        putchar(*s++);
    }
    printf("s[0] = %c", s[0]);
}
