#include <stdio.h>
void printViaPtr(const int* intPtr)
{
    printf("%d\n", *intPtr);
}


void print2Ints(int num, int num2) {
    int* numPtr = NULL;
    int* numPtr2 = NULL;
    numPtr = &num;
    numPtr2 = &num2;
    printViaPtr(numPtr);
    printViaPtr(numPtr2);
}


int main(void)
            
{
    print2Ints(11, -93);
}

