#include <stdio.h>


char* mystrrchr(char* s, int c)
{
    int c1 = 0;
    int c2 = 0;
    while (*s != '\0') {
        s++;
        c1++;
    }
    for (c2 = 0; c2 <= c1; c2++) {

        if(*s == c) {
            return s;
        }
        s--;

    }
    return NULL;
}


int main(void)
{
    char* s = "ENCN305";
    char* foundAt = mystrrchr(s, '5');
    if (foundAt == NULL) {
        puts("Not found");
    } else {
        printf("%zu\n", foundAt - s);
    }
}
