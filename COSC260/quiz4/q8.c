// A test program for the "findTwoLargest" function

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void findTwoLargest(const int data[], int n, int* largest, int* secondLargest)
{
    *largest = -2147483648;
    *secondLargest = -2147483648;

    for (int i = 0; i < n; i++) {
        if (data[i] > *largest) {
            *secondLargest = *largest;
            *largest = data[i];

        } else if ( data[i] > *secondLargest) {
            *secondLargest = data[i];
        } else {
            continue;
        }

    }
}

// Lastly, the main test routine.
int main()
{

    int data[] = {-2147483646, -2147483648, -2147483647};
    int result1 = 0, result2 = 0;
    findTwoLargest(data, 3, &result1, &result2);
    printf("%d %d\n", result1, result2);

}

