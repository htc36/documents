#include <stdio.h>

size_t mystrlene(const char s[])
{
    int i = 0;
    while(s[i] != '\0') {
        i++;
    }
    return i;
}
size_t mystrlen(const char* s)
{
    int count = 0;

    while (*s != '\0') {
        s++;
        count++;
    }
    return count;
}

int main(void)
{
    printf("%zd\n", mystrlen("ENCE260"));
}
