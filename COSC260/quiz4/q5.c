#include <stdio.h>

void swap(int* pi, int* pj)
{
    int saver = 0;
    saver = *pi;
    *pi = *pj;
    *pj = saver;
}

int main(void)
{
    int i = 10, j = 20;
    swap(&i, &j);
    printf("%d %d\n", i, j);
}

