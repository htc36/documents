#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    float screeHeight = 0;
    float rushHeight = 0;
    float slideBack = 0;
    int counter = 0;
    scanf("%f %f %f", &screeHeight, &rushHeight, &slideBack);

    while (screeHeight > 0) {
        screeHeight -= rushHeight;
        if (screeHeight > 0) {
            screeHeight += slideBack;
        }
        counter += 1;
    }
    printf("%d", counter);
}


