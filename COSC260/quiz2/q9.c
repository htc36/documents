#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(void) 
{
    bool done = false;
    int i = 0;
    for (i = 0; !done; i++) {
        if (i == 10) {
            done = true;
        }
    }
    printf("%d",i);

    int num = 0;
    do {
        scanf("%d", &num);
        printf("%d\n", num);
    } while (num != 42);
}
