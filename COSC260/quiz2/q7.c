#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int number = 0;
    while (number != 42) {
        scanf("%d", &number);
        printf("%d\n", number);
    }
}
