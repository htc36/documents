#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    char someChar = 0;          // A character
    int c = 0;                  // An int with a char in its low byte

    someChar = '*';
    printf("someChar = %c\n", someChar); // Print a single char

    printf("Enter a line of text, terminated by 'Enter'\n");

    // Read and print characters until newline or End-Of-File
    c = getchar();              // Get char (cast to int) or EOF
    while (c != '\n' && c != EOF) {
        printf("Character '%c', decimal %d, octal %o, hexadecimal %x\n", c, c, c, c);
        c = getchar();
    }

    return EXIT_SUCCESS;
}
