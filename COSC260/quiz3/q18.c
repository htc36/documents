#include<stdlib.h>
#include<stdio.h>
#include<ctype.h>

int isWonRow(char player, char game[3][3], int rowNum)
{
    if ((game[rowNum][0] == 'X' && game[rowNum][1] == 'X' && game[rowNum][2] == 'X')&& player =='X') {
        return 1;
    }
    if ((game[rowNum][0] == 'O' && game[rowNum][1] == 'O' && game[rowNum][2] == 'O') && player == 'O') {
        return 1;
    }
    return 0;
}

int main(void)
{
    char game[3][3] = {{'X', 'O', ' '},{' ', ' ', ' '}, {'X', 'X', 'O'}};
    printf("%d\n", isWonRow('X', game, 2));
}



