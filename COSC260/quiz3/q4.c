/* Program to introduce the 'char' data type, plus the use of getchar and
 * putchar to read and write chars from/to standard input.
 * Reads characters from stdin, printing each one out in both decimal and octal,
 * until '\n' or end-of-file is reached.
 * Written for ENCE260 by RJL.
 * June 2016
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int main(void)
{
    int c = 0;                  // An int with a char in its low byte
    int ddd = 0;
    int iii = 0;

    c = getchar();              // Get char (cast to int) or EOF

    while (c != EOF) {
        if (c == '\012') {
            printf("'\\n'\n");

<<<<<<< HEAD
    // Read and print characters until newline or End-Of-File
    c = getchar();              // Get char (cast to int) or EOF
    while (c != '\n' && c != EOF) {
        printf("Character '%c', decimal %d, octal %o, hexadecimal %x\n", c, c, c, c);
=======
        } else if  (isdigit(c)) {
            printf("'%c': Digit %c\n", c, c);

        } else if (isalpha(c)) {
            for (ddd = 65; ddd <= 90; ddd++) {
                if (c - ddd == 0) {
                    iii = ddd - 64;
                    printf("'%c': Letter %d\n", c, iii);
                }
                if (c - (ddd + 32) == 0) {
                    iii = ddd - 64;
                    printf("'%c': Letter %d\n", c, iii);
                }

            }


        } else {
            printf("'%c': Non-alphanumeric\n", c);
        }
>>>>>>> 233eb7b5a5d850354c1e0aac808627aa7071425a
        c = getchar();
    }

    return EXIT_SUCCESS;
}
