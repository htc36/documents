#include <stdio.h>
#include <stdlib.h>

int check_winner(char player, char oppisition) {
    if (player == 'P') {
        if (oppisition == 'R') {
            return 1;
        }

        if (oppisition == 'S') {
            return 0;
        }

        if (oppisition == 'P') {
            return 2;
        }
    }

    if (player == 'S') {
        if (oppisition == 'R') {
            return 0;
        }

        if (oppisition == 'S') {
            return 2;
        }

        if (oppisition == 'P') {
            return 1;
        }
    }

    if (player == 'R') {
        if (oppisition == 'R') {
            return 2;
        }

        if (oppisition == 'S') {
            return 1;
        }

        if (oppisition == 'P') {
            return 0;
        }
    }
    return 3;
}

int main(void) {
    printf("%d", check_winner('R', 'S'));
}
