public class StarmanSays {
	
	/**
	 * Methods are placed above the main method
	 * in this section of the program
	 */
	
	public String sayHello() {
		return "Hello";
	}
	 
        public String sayDontPanic() {
                return "Don't Panic!"
        }
        
        public String sayJavaCool() {
                return "Java is cool"
        }
        
        public String sayGoodbyeWorld() {
                return "Goodbye, World!"
                
        
	public static void main(String[] args) {
		StarmanSays starman = new StarmanSays();   // new instance of StarmanSays
		System.out.println(starman.sayHello()); 
	}
	
}
