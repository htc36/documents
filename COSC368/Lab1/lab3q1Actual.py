import random
import time
from _csv import writer
from tkinter import *

class Board(object):
    def __init__(self, dist, width):
        self.master = Tk()
        self.count = 0
        self.repetitions = 4
        self.height = 700
        self.width = 700
        self.center = self.width / 2
        self.randomDistances = random.sample(dist, len(dist))
        self.randomWidths = random.sample(width, len(width))
        self.started = False
        self.green = 1
        self.distances =[]
        self.widths =[]

    def setUp(self):
        self.c = Canvas(self.master, width=self.width, height=self.height)
        self.c.pack()
        for iii in self.randomDistances:
            self.distances += [iii] * self.repetitions
        for iii in self.randomWidths:
            self.widths += [iii] * self.repetitions
        self.createLines()


    def createLines(self):
        totalSpan = self.distances[self.count] + self.widths[self.count]
        margin = (self.width - totalSpan) / 2
        leftBottom = margin+self.widths[self.count]
        rightTop = margin+self.distances[self.count]
        rightBottom = margin+self.widths[self.count]+self.distances[self.count]
        if self.started == False:
            self.time = time.time()
            self.c.create_rectangle(margin, 0, leftBottom,self.height, tag="left", fill="green", outline="green")
            self.c.create_rectangle(rightTop, 0, rightBottom,self.height, tag="right", fill="blue", outline="blue")
            self.c.tag_bind("left", "<ButtonPress-1>", self.leftClick)
            self.c.tag_bind("right", "<ButtonPress-1>",  self.rightClick)
        else:
            self.c.coords("left", margin, 0,leftBottom, self.height)
            self.c.coords("right", rightTop, 0, rightBottom, self.height)

    def leftClick(self, dummy):
        self.started = True
        if self.count >= len(self.distances) -1:
            print("thanks")
            self.master.destroy()
            return
        if self.green == 1:
            self.logTime()
            self.count +=1
            self.green = 0
            self.c.itemconfigure("left", fill="blue", outline='blue')
            self.c.itemconfigure("right", fill="green", outline='green')
            self.createLines()
            return
        else:
            return
    def rightClick(self, dummy):
        self.started = True
        self.logTime()
        if self.count >= len(self.distances) -1:
            print("thanks")
            self.master.destroy()
            return
        if self.green == 0:
            self.c.itemconfigure("right", fill="blue", outline='blue')
            self.c.itemconfigure("left", fill="green",outline='green')
            self.green = 1
            self.count +=1
            self.createLines()
            return
        else:
            return

    def logTime(self):
        log = ["Harry", self.distances[self.count], self.widths[self.count],  self.count % self.repetitions, time.time() - self.time]
        self.time = time.time()
        with open("result.txt", 'a+', newline='') as write_obj:
            csv_writer = writer(write_obj)
            csv_writer.writerow(log)


dist = [64, 128, 256, 512]
width = [4, 8, 16, 32]
board = Board(dist,width)
board.setUp()
board.master.mainloop()