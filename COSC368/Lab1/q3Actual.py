import time
from tkinter import *
from tkinter.ttk import *
import random
import csv
from csv import writer

def append_list_as_row(file_name, list_of_elem):
    with open(file_name, 'a+', newline='') as write_obj:
        csv_writer = writer(write_obj)
        csv_writer.writerow(list_of_elem)


target = []
index = 0
# typee="dynamic"
typee="dynamic"
start=None
counts = {}
results = []

keys = list("qwertyuiopasdfghjklzxcvbnm")
random.shuffle(keys)
chosenKeys = keys[0:5]
for iii in range(6):
    random.shuffle(chosenKeys)
    target += ''.join(str(e) for e in chosenKeys)


window = Tk()
inputAreaFrame = Frame(window)
inputAreaFrame.pack(side=TOP, fill=X, padx=10, pady=10)

wantedResult = StringVar()
display = Label(inputAreaFrame, textvariable=wantedResult)
display.pack(side=LEFT)
wantedResult.set("Press any key to start")



frame = Frame(window)
frame.pack(side="right", padx=10, pady=10)

keyBoardFrame = Frame(frame, borderwidth=4, relief=RIDGE)

def createRows():
    keys = list("qwertyuiopasdfghjklzxcvbnm")
    random.shuffle(keys)
    return [keys[0:10], keys[10:19], keys[19:26]]



def createBoard():
    global keyBoardFrame, keys
    for child in keyBoardFrame.winfo_children():
        child.destroy()
    keys = createRows()
    for level in keys:
        keyLine = Frame(keyBoardFrame)
        for char in level:
            eachKey = Frame(keyLine, height=32, width=32)
            eachKey.pack_propagate(0)  # don't shrink
            b = Button(eachKey, text=char, width = 5, command=lambda x=char: addChar(x))
            eachKey.pack(side=LEFT)
            b.pack(side=LEFT)

        keyLine.pack(side=TOP, padx=2, pady=0)
    keyBoardFrame.pack()

createBoard()

def addChar(charToAdd):
    global index, target, wantedResult, dynamic, keys, start, results
    if (start == None):
        wantedResult.set(target[index])
        start = time.time()
        createBoard()

    if charToAdd == target[index]:
        counts[charToAdd] = counts.get(charToAdd,0) + 1
        result = (["Harry", typee, charToAdd, counts.get(charToAdd), (time.time() - start) * 1000])
        append_list_as_row("experiment_" + typee + "_log.txt", result)
        start = time.time()
        if index == len(target) -1:
            # If at end of block
            wantedResult.set("Thanks for you participation!")
            return
        index += 1
        wantedResult.set(target[index])
        if typee == 'dynamic':
            createBoard()
def main():
    global index, target, wantedResult
    for iii in range(0,2):
        index = 0
        wantedResult.set(target[index])
        createBoard()
        index = 0


window.mainloop()

