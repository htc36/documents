import math
import csv

results = []
dict = {}
timeDictionary = {}
with open('experiment_fitts_log.txt') as inputfile:
    for line in inputfile:
        listLine = (line.strip().split('\t'))
        keyTuple = (int(listLine[1]), int(listLine[2]), int(listLine[3]))
        timeTuple = (int(listLine[1]), int(listLine[2]))
        dict[keyTuple] = float(listLine[4])
        timeDictionary[timeTuple] = timeDictionary.get(timeTuple,[]) + [float(listLine[4])]

print(timeDictionary)
finalDict = {}
for key, value in dict.items():
    id = math.log2((key[0] / key[1]) + 1)
    tuple = (key[0], key[1])
    meanTime = (sum(timeDictionary.get(tuple)) / 8) / 1000
    finalDict[round(id,2)] = finalDict.get(round(id,2), []) + [meanTime]

results = []
for key, value in finalDict.items():
    summ = sum(value) / len(value)
    results.append((key, round(summ, 3)))


print(results)

file = open("summary.csv", "w")
file.write("ID, mean time\n")
for row in results:
    file.write("{},{}\n".format(str(row[0]), row[1]))

file.close()
