from tkinter import *
from tkinter.ttk import *
window = Tk()
side_labels = ["bottom1", "bottom2", "top1", "top2", "left1", "right1"]
for theside in side_labels:
    button = Button(window, text=theside)
    button.pack(side=theside[0:-1])
window.mainloop()


from tkinter import *
from tkinter.ttk import *
window = Tk()
for label_num in range(6):
    button = Button(window, text="Button"+str(label_num))
    button.grid(row=label_num // 3, column=label_num % 3)
window.mainloop()

from tkinter import *
from tkinter.ttk import *
window = Tk()
for label_num in range(6):
    button = Button(window, text="Button" + str(label_num))
    button.grid(row=label_num // 2, column=label_num % 3)
    if label_num==1:
        button.grid(columnspan=2, sticky="ew")
    elif label_num==3:
        button.grid(rowspan=2, sticky="ns")
window.columnconfigure(1, weight=1)
window.rowconfigure(1, weight=1)
window.rowconfigure(2, weight=1)
window.mainloop()


from tkinter import *
from tkinter.ttk import *
window = Tk()
frame_left = Frame(window, borderwidth=4, relief=RIDGE)
frame_left.pack(side="left", fill="y", padx=5, pady=5)
frame_right = Frame(window)
frame_right.pack(side="right")

button1 = Button(frame_left, text="Button 1")
button1.pack(side="top")
button2 = Button(frame_left, text="Button 2")
button2.pack(side="bottom")

for label_num in range(4):
    button = Button(frame_right, text="Button" + str(label_num + 3))
    button.grid(row=label_num // 2, column=label_num % 2)
window.mainloop()


from tkinter import *
from tkinter.ttk import *
window = Tk()

T = Text(window, height=10, width=24, wrap = 'none')

scroll_bar = Scrollbar(window, command = T.yview)
scroll_bar.pack(side=RIGHT, fill = Y)

horizontalScroll = Scrollbar(window, orient=HORIZONTAL, command= T.xview)
horizontalScroll.pack(side=BOTTOM, fill = X)

T.config(xscrollcommand = horizontalScroll.set, yscrollcommand = scroll_bar.set)

words = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, \ntotam rem aperiam, eaque ipsa quae ab illo inventore veritatis et\n quasi architecto beatae vitae dicta sunt explicabo.\n Nemo enim ipsam voluptatem quia voluptas \nsit aspernatur aut odit aut fugit, sed quia consequuntur magni \n dolores eos qui ratione voluptatem sequi nesciunt.\n Neque porro quisquam est, qui dolorem ipsum quia dolor \nsit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore \nmagnam aliquam quaerat voluptatem. Ut enim ad \nminima veniam, quis nostrum exercitationem ullam \ncorporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

T.insert(END, words)
T.pack()
window.mainloop()
