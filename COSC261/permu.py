def help(alpha, length, candidate, output):
    if len(candidate) == length:
        return  (candidate)
        
    else:
        for iii in alpha:
            for child in [candidate + [iii]]:
                return help(alpha, length, child, output)

def all_strings(alpha, length):
    output = []
    help(alpha, length, '', output)
    return output
