def print_pair(x_coord, y_coord):
    """Displays two numbers in coordinates"""
    intro = "[x,y] = "
    print("{0}[{1:.1f},{2:.1f}]".format(intro, x_coord, y_coord))
    

print_pair(1.72356, -22.1111)
print_pair(1.0, 2.0)

