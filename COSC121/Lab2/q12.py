def get_and_print_rectangle():
    """ Input a rectangle's width and height then print its area """
    width = input("Rectangle width? ")
    height = input("Rectangle height? ")
    width = float(width)
    height = float(height)
    area = width * height
    print ("The area of the rectangle is: " + str(area))
    
get_and_print_rectangle()
    
    