"""Calculating invigilated component of the course"""

def invigilated_percent(test_percent, exam_percent): 
    """caluclating overall percent"""
    return ((test_percent * 0.15) + (exam_percent * 0.6))/(0.15 + 0.6)

def main():
    """Getting user input then processing data"""
    test_percent = float(input("Test percent? "))
    exam_percent = float(input("Exam percent? "))
    mark = invigilated_percent(test_percent, exam_percent)
    print("{0} = {1:.1f}".format("Invigilated mark", mark))
    if mark >= 45:
        print("Invigilated pass :-)")
    else:
        print("Invigilated fail :-(")
    
main()