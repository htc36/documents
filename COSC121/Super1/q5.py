def number_of_passes(grade_list):
    """passgrades calculation"""
    passgrades = ['A+', 'A', 'A-', 'B+', 'B', 'B-', 'C+', 'C', 'C-', 'R']
    total = 0
    
    for letter in grade_list:
        for letter2 in passgrades:  
            if letter == letter2:
                total += 1              
    return total
            
result = number_of_passes(['A+', 'A', 'B', 'B', 'O'])
print(result) 

"""|| "A" || "A-" || "B+" || "B" || "B-" || "C+" || "C" || "C-" || "R"):"""