"""Calculating invigilated component of the course"""

def invigilated_percent(test_percent, exam_percent): 
    """caluclating overall percent"""
    return ((test_percent * 0.15) + (exam_percent * 0.6))/(0.15 + 0.6)

mark = invigilated_percent(50, 70)
print('Mark =', mark)
mark = invigilated_percent(50, 60)
print('Invig. avg. =', mark)
    