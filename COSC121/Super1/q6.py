def number_of_passes(grade_list):
    """passgrades calculation"""
    passgrades = ['A+', 'A', 'A-', 'B+', 'B', 'B-', 'C+', 'C', 'C-', 'R']
    total = 0
    
    for letter in grade_list:
        for letter2 in passgrades:  
            if letter == letter2:
                total += 1              
    return total

def pass_rate(grade_list):
    """calculating pass rate"""
    check = len(grade_list)
    if check != 0:
        result = number_of_passes(grade_list)
        grade = float(result/(len(grade_list)))
        return float(grade)
                
            
result = pass_rate([])
print(result is None)