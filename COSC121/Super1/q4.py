def is_clear_pass(invigilated_mark, exam_mark, overall_mark):
    """Checking for clear pass"""
    return (overall_mark >= 50) and (exam_mark >= 60 or invigilated_mark >= 45)
    
    
ans = is_clear_pass(49, 70, 49)
print(ans)
ans = is_clear_pass(45, 45, 49)
print(ans)