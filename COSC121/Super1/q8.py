def display_stats(marks):
    """displaying marks"""
    passing = 0
    failing = 0
    aver = 0
    for iii in marks:
        aver += iii
        if iii >= 45:
            passing += 1
        else:
            failing += 1
            
    aver = aver/(len(marks))
    percentpass = (passing/(len(marks))) * 100
    percentfail = (failing/(len(marks))) * 100
            
    print('Number passing = {0} ({1:.2f}%)'.format(passing, (percentpass)))
    print('Number failing = {0} ({1:.2f}%)'.format(failing, (percentfail)))
    print('Average mark = {:.2f}'.format(aver))
    print('Number of marks = {}'.format(len(marks)))
    
    
    
    
marks = [40, 43, 50, 20]
display_stats(marks)

"""print('G.P.A. = {:.2f}'.format(average))"""