"""Calculating invigilated component of the course"""

def invigilated_percent(test_percent, exam_percent): 
    """caluclating overall percent"""
    return ((test_percent * 0.15) + (exam_percent * 0.6))/(0.15 + 0.6)

def process_marks():
    """Processing everything"""
    name = input("Given name(s)? ").title()
    family_name = input("Family name? ").upper()
    test_percent = float(input("Test percent? "))
    exam_percent = float(input("Exam percent? "))
    mark = invigilated_percent(test_percent, exam_percent)
    print("{0}, {1}: {2:.2f}".format(family_name, name, mark))

process_marks()


