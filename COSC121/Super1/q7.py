def grade_point_value(grade):
    """ Takes a grade string and returns the points for the grade as an int 
    Returns None if the grade isn't in point_values.
    NOTE: You don't need to understand how this function works you just need to know
    what it does and how to call it
    """
    point_values = {'A+':9, 'A':8, 'A-':7, 
                    'B+':6, 'B':5, 'B-':4,
                    'C+':3, 'C':2, 'C-':1, 'R':1,
                    'D':0, 'E':-1, 'X':-3}
    return point_values.get(grade)

def grade_point_average(grade_list):
    """Finding gpa sum then finding average"""
    total = 0
    
    for iii in grade_list:
        total += grade_point_value(iii)
        
    if len(grade_list) != 0:
        result = total/(len(grade_list))
        return result
    

grades = []
average = grade_point_average(grades)
print(average is None)

