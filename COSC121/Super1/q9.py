def passing_students(students):
    """sorting tuples"""
    liste = []
    for name, num in students:
        if num >= 45:
            liste.append((name, num))
    liste.sort()
    return liste 


students = [('ARROW Bender', 26.25),
            ('MARIO Luigi', 46.5),
            ('ZAPPA Franky', 50.0)
            ]
passing = passing_students(students)
for record in passing:
    print(record)