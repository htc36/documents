def to_celsius(fahrenheit):
    """Return the given fahrenheit temperature in degrees celsius"""
    degrees_celsius = (5 / 9) * (fahrenheit - 32)
    return degrees_celsius

degrees = to_celsius(42)
print(degrees)