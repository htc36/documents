def same_ends(string_1, string_2):
    """Indexing words"""
    return (string_1[0] == string_2[0]) and (string_1[-1] == string_2[-1] and string_1 != string_2)

print(same_ends("flubber", "fester"))
print(same_ends("flubber", "flubber"))
print(same_ends("flubber", "feast"))
print(same_ends("flubber", "rubber"))