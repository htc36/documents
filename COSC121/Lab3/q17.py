def x_at_either_end(string):
    """Testing for x in words"""
    return string.endswith('x') or string.startswith('x')

print(x_at_either_end('Ping Pong'))
print(x_at_either_end('pax'))
print(x_at_either_end('xposure'))