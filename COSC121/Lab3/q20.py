def thing(i, j):
    """Does something silly"""
    
    return i > 10 and (j > 5 or i < 20)


print(thing(11, 0))
print(thing(21, 6))