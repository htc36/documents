def my_abs(value):
    """Absoulute Value Calculator"""
    if value < 0:
        return(-1*value)
        
    else:
        return(value)
    
print(my_abs(3.5))
print(my_abs(-7.0))