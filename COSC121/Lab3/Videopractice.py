"""quadratic formular"""

import math

print("Ax^2 + Bx + C")

a = int(input("What is A "))
b = int(input("What is B "))
c = int(input("What is C ")) 

def print_quad(a, b, c):
   
    if a == 0:
        print("Not Quadratic")
    else:
        discrim = b**2 - 4 * a * c
        if discrim >= 0:
            root1 = (-b - math.sqrt(discrim)) / (2 * a)
            root2 = (-b + math.sqrt(discrim)) / (2 * a)
            print("The roots are: x = {0:.2f} and x = {1:.2f}".format(root1, root2))
        else:
            print("It is imaginary")
            
print_quad(a, b, c)
        
        
        
        

    
    