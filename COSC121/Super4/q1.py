"""Harry Collard, 3/10/18"""
import re


def get_words_from_file(filename):
    """Unpacks the input file, then returns all valid text"""
    infile = open(filename, encoding='utf-8')
    lines = infile.readlines()
    overall_words = []
    has_started = False
    has_ended = False
    
    for iii in lines:
        iii = iii.strip()
        if iii.startswith("*** START OF") and has_started == False:
            has_started = True
            
        elif iii.startswith("*** END") and has_ended == False:
            has_ended = True 
            
        elif has_started == True and has_ended == False:
            words_on_line = re.findall("[a-z]+[-']?[a-z]+|[a-z]+[']?|[a-z]+", iii.lower())
            for iii in words_on_line:
                overall_words.append(iii)        

    return overall_words


	
# NOTE: Don't use synthetic.txt from SQ3
# - this is a new file
filename = "synthetic.txt"
words = get_words_from_file(filename)
print(filename, "loaded ok.")
print("{} valid words found.".format(len(words)))
print("Valid word list:")
for word in words:
    print(word)