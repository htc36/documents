"""Harry Collard, 3/10/18"""
import os
import re


def get_filename():
    """Returns a valid filename"""
    filename = input("Please enter filename: ")
    while not os.path.isfile(filename):
        print(filename, "not found...")
        filename = input("Please enter filename: ")
    return filename


def get_words_from_file(filename):
    """Unpacks the input file, then returns all valid text"""
    infile = open(filename, encoding='utf-8')
    lines = infile.readlines()
    overall_words = []
    has_started = False
    has_ended = False
    
    for iii in lines:
        iii = iii.strip()
        if iii.startswith("*** START OF") and has_started == False:
            has_started = True
            
        elif iii.startswith("*** END") and has_ended == False:
            has_ended = True 
            
        elif has_started == True and has_ended == False:
            words_on_line = re.findall("[a-z]+[-']?[a-z]+|[a-z]+[']?|[a-z]+", iii.lower())
            for iii in words_on_line:
                overall_words.append(iii)     
    print(" {} loaded ok.".format(filename))
    print()
    return overall_words

def word_counter(list_of_words):
    """prints total number of valid words"""
    word_count = len(list_of_words)
    print("Word summary (all words):")
    print(" Number of words = {}".format(word_count))

def average_len(words):
    """prints average word lenth"""
    counter = 0
    for iii in words:
        counter += len(iii)
    average = counter / len(words)
    print(" Average word length = {:.2f}".format(average))  
    
def max_word_len(words):
    """prints max word length"""
    max_len = len(words[0])
    for iii in words:
        length = len(iii)
        if length > max_len:
            max_len = length
    print(' Maximum word length =', max_len)

def word_freq(words):
    """prints the word that appears the most"""
    counts = {}
    for iii in words:
        if iii.strip() != "":
            counts[iii] = counts.get((iii), 0) + 1 
    frequency_list = []
    for key in counts:
        frequency_list.append(counts[key])
    print(" Maximum frequency = {}".format(max(frequency_list)))
    
def main():
    """ Gets the job done """
    filename = get_filename()
    list_of_words = get_words_from_file(filename)
    word_counter(list_of_words)
    average_len(list_of_words)
    max_word_len(list_of_words)
    word_freq(list_of_words)

main()








    