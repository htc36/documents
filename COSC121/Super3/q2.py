"""Q2 of superquiz"""
import os
def get_file_name():
    """gets file name"""
    file_name = input("Please enter filename: ")
    while not os.path.isfile(file_name):
        print(file_name, "not found...")
        file_name = input("Please enter filename: ")
    print()
    return file_name

def get_words_from_file(filename):
    """gets words from filename"""
    infile = open(filename, 'r', encoding="utf-8")
    strippers = '\n \'"-:,.\t?'  # Chars to strip
    word_list = infile.read().lower().split()
    infile.close()
    words = []
    for iii in word_list:
        single_word = iii.strip(strippers)  # Stripped word
        if single_word.isalpha():
            words.append(single_word)
    print(filename, "loaded ok.")
    print("{} valid words found.".format(len(words)))    
    return words

def unique_words(words):
    """returns a list of unique words"""
    unique_list = []
    for www in words:
        if www not in unique_list:
            unique_list.append(www)
    print("{} unique words found.".format(len(unique_list)))
    print()
    print("First word: {}".format(words[0]))
    print("Last word: {}".format(words[-1]))  
    return unique_list

def max_word_len(words):
    """prints max word length"""
    max_len = len(words[0])
    for www in words:
        length = len(www)
        if length > max_len:
            max_len = length
    print('Max word length =', max_len)

def average_len(words):
    """prints average word lenth"""
    counter = 0
    for www in words:
        counter += len(www)
    average = counter / len(words)
    print("Average word length = {:.1f}".format(average))   
    
def average_unique_len(words):
    """prints average unique word length"""
    counter = 0
    for www in words:
        counter += len(www)
    average = counter / len(words)
    print("Average unique word length = {:.1f}".format(average))    
    
def main():
    """main function that calls everything"""
    filename = get_file_name()
    word = get_words_from_file(filename)
    unique_word = unique_words(word)
    max_word_len(word)
    average_len(word)
    average_unique_len(unique_word)
main()