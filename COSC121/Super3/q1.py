"""Q1 of superquiz"""
def get_words_from_file(filename):
    """gets words from filename"""
    infile = open(filename, 'r', encoding="utf-8")
    strippers = '\n \'"-:,.\t?'  # Chars to strip
    word_list = infile.read().lower().split()
    infile.close()
    words = []
    for iii in word_list:
        single_word = iii.strip(strippers)  # Stripped word
        if single_word.isalpha():
            words.append(single_word)
    return words
            
filename = 'pg24485.txt'
words = get_words_from_file(filename)
print(filename, "loaded ok.")
print("{} valid words found.".format(len(words)))