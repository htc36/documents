

import os
f = input("Please enter filename: ")
while not os.path.isfile(f):
    print(f, "not found...")
    f = input("Please enter filename: ")
print()
d = open(f, 'r', encoding="utf-8")
ts = '\n \'"-:,.\t?'  # Chars to strip
ws = d.read().lower().split()
d.close()
words = []
for w in ws:
    sw = w.strip(ts)  # Stripped word
    if sw.isalpha():
        words.append(sw)
print(f, "loaded ok.")
print("{} valid words found.".format(len(words)))
uw = []
for w in words:
    if w not in uw:
        uw.append(w)
print("{} unique words found.".format(len(uw)))
print()
print("First word: {}".format(words[0]))
print("Last word: {}".format(words[-1]))
lw = ''
ml = len(words[0])
n = 0
for w in words:
    l = len(w)
    if l > ml:
        ml = l
        lw = w
print('Max word length =', ml)
s = 0
for w in words:
    s += len(w)
a = s / len(words)
print("Average word length = {:.1f}".format(a))
s = 0
for w in uw:
    s += len(w)
a = s / len(uw)
print("Average unique word length = {:.1f}".format(a))

