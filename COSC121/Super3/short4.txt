The last farewell was affecting in the extreme. From the belfries far
and near the funereal deathbell tolled unceasingly while all around the
gloomy precincts rolled the ominous warning of a hundred muffled drums
punctuated by the hollow booming of pieces of ordnance. The deafening
claps of thunder and the dazzling flashes of lightning which lit up
the ghastly scene testified that the artillery of heaven had lent its
supernatural pomp to the already gruesome spectacle. A torrential rain
poured down from the floodgates of the angry heavens upon the
bared heads of the assembled multitude which numbered at the
lowest computation five hundred thousand persons. A posse of Dublin
Metropolitan police superintended by the Chief Commissioner in person
maintained order in the vast throng for whom the York street brass and
reed band whiled away the intervening time by admirably rendering on
their blackdraped instruments the matchless melody endeared to us from
the cradle by Speranza's plaintive muse. Special quick excursion trains
and upholstered charabancs had been provided for the comfort of our
country cousins of whom there were large contingents. Considerable
amusement was caused by the favourite Dublin streetsingers L-n-h-n and
M-ll-g-n who sang _The Night before Larry was stretched_ in their usual
mirth-provoking fashion. Our two inimitable drolls did a roaring trade
with their broadsheets among lovers of the comedy element and nobody
who has a corner in his heart for real Irish fun without vulgarity
will grudge them their hardearned pennies. The children of the Male and
Female Foundling Hospital who thronged the windows overlooking the scene
were delighted with this unexpected addition to the day's entertainment
and a word of praise is due to the Little Sisters of the Poor for their
excellent idea of affording the poor fatherless and motherless children
a genuinely instructive treat. The viceregal houseparty which included
many wellknown ladies was chaperoned by Their Excellencies to the most
favourable positions on the grandstand while the picturesque foreign
delegation known as the Friends of the Emerald Isle was accommodated
on a tribune directly opposite. The delegation, present in full force,
consisted of Commendatore Bacibaci Beninobenone (the semiparalysed
_doyen_ of the party who had to be assisted to his seat by the aid of a
powerful steam crane), Monsieur Pierrepaul Petitépatant, the Grandjoker
Vladinmire Pokethankertscheff, the Archjoker Leopold Rudolph von
Schwanzenbad-Hodenthaler, Countess Marha Virága Kisászony Putrápesthi,
Hiram Y. Bomboost, Count Athanatos Karamelopulos, Ali Baba Backsheesh
Rahat Lokum Effendi, Senor Hidalgo Caballero Don Pecadillo y Palabras
y Paternoster de la Malora de la Malaria, Hokopoko Harakiri, Hi Hung
Chang, Olaf Kobberkeddelsen, Mynheer Trik van Trumps, Pan Poleaxe
Paddyrisky, Goosepond Prhklstr Kratchinabritchisitch, Borus
Hupinkoff, Herr Hurhausdirektorpresident Hans Chuechli-Steuerli,
Nationalgymnasiummuseumsanatoriumandsuspensoriumsordinaryprivatdocent
-generalhistoryspecialprofessordoctor Kriegfried Ueberallgemein. All the
delegates without exception expressed themselves in the strongest
possible heterogeneous terms concerning the nameless barbarity which
they had been called upon to witness. An animated altercation (in which
all took part) ensued among the F. O. T. E. I. as to whether the eighth
or the ninth of March was the correct date of the birth of Ireland's
patron saint. In the course of the argument cannonballs, scimitars,
boomerangs, blunderbusses, stinkpots, meatchoppers, umbrellas,
catapults, knuckledusters, sandbags, lumps of pig iron were resorted to
and blows were freely exchanged. The baby policeman, Constable
MacFadden, summoned by special courier from Booterstown, quickly
restored order and with lightning promptitude proposed the seventeenth
of the month as a solution equally honourable for both contending
parties. The readywitted ninefooter's suggestion at once appealed to all
and was unanimously accepted. Constable MacFadden was heartily
congratulated by all the F.O.T.E.I., several of whom were bleeding
profusely. Commendatore Beninobenone having been extricated from
underneath the presidential armchair, it was explained by his legal
adviser Avvocato Pagamimi that the various articles secreted in his
thirtytwo pockets had been abstracted by him during the affray from the
pockets of his junior colleagues in the hope of bringing them to their
senses. The objects (which included several hundred ladies' and
gentlemen's gold and silver watches) were promptly restored to their
rightful owners and general harmony reigned supreme.