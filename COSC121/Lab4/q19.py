def long_enough(strings, min_length):
    """filtering out short words"""
    result = []
    for letter in strings:
        if len(letter) >= min_length:
            result.append(letter)
    return result
    
strings = ['a', 'bc', 'def', 'ghij', 'klmno']
print(long_enough(strings, 3))    