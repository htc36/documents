def cubes(data):
    """cubing stuff with for loops"""
    result = []
    for number in data:
        result.append(number * number * number)
    return result

cubes_list = cubes([5])
print(cubes_list)

words = 'This is a SENTENCE'.split()
print(words)

lower_case_words = []

for word in words:
    word = word.lower()  # Convert to lower case
    lower_case_words.append(word)
    
alphabetical = 'In alphabetical order:'
for word in lower_case_words:
    alphabetical += ' ' + word
    
print(alphabetical)