def evens(numbers):
    """returns even numbers only"""
    result = []
    for number in numbers:
        if number % 2 == 0:
            result.append(number)
    return result
    
print(evens([1, 2, 3, 4, 5, 6, 10, 11]))

