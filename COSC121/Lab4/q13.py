def my_max(data):
    """finding maximum number in array"""
    maxvalue = -999999999999
    for numbers in data:
        if numbers > maxvalue:
            maxvalue = numbers
            
    return maxvalue
        
    
print(my_max([-3, -5, -9, -10]))
