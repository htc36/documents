def lower_case(strings):
    """making lower case words in list"""
    lowercase = []
    for word in strings:
        word = word.lower()
        lowercase.append(word)
    return lowercase

data = ['HEY', 'Harry']
print(lower_case(data))