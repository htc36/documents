"""functions for converting to cat language"""
def cat_latin_from_sentence(sentance):
    """converting to cat language"""
    words = sentance.split()
    constan = ('b', 'c', 'd', 'f', 'g', 'h', 
               'j', 'k', 'l', 'm', 'n', 'p',
               'q', 'r', 's', 't', 'v', 'x', 
               'z', 'y', 'w')
    new_sentance = str()
    
    for iii in words:
        iii = iii.lower()
        if iii.startswith(constan) and len(iii) != 1:
            convert = iii[1 : :] + iii[0] + "eeoow"
            new_sentance = new_sentance + " " + convert
        elif iii.startswith(constan) and len(iii) == 1:
            convert3 = iii + "eeoow"
            new_sentance = new_sentance + " " + convert3
        else:
            convertnon = iii + "meeoow"
            new_sentance = new_sentance + " " + convertnon
    return(new_sentance[1: :])

def main():
    """the main fuction for inputs"""
    sentan = input("Enter English sentence: ")
    if sentan != "q":
        new_sentance = cat_latin_from_sentence(sentan)
        print("Cat latin = {0}".format(new_sentance))
        main()
    else:
        print(cat_latin_from_sentence("goodbye"))
          
main()