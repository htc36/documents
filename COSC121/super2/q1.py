def cat_latin_from_sentence(sentance):
    """converting to cat language"""
    words = sentance.split()
    constan = ('b', 'c', 'd', 'f', 'g', 'h', 
               'j', 'k', 'l', 'm', 'n', 'p',
               'q', 'r', 's', 't', 'v', 'x', 
               'z', 'y', 'w')
    new_sentance = str()
    
    for iii in words:
        iii = iii.lower()
        if iii.startswith(constan) and len(iii) != 1:
            convert = iii[1 : :] + iii[0] + "eeoow"
            new_sentance = new_sentance + " " + convert
        elif iii.startswith(constan) and len(iii) == 1:
            convert3 = iii + "eeoow"
            new_sentance = new_sentance + " " + convert3
        else:
            convertnon = iii + "meeoow"
            new_sentance = new_sentance + " " + convertnon
    return(new_sentance[1: :])

cat_latin = cat_latin_from_sentence("man an this eggceptionally boring test case a")
print(cat_latin)
