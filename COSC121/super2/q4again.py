def english_sentence(sentance):
    """converting to cat language"""
    words = sentance.split()
    version1 = []
    sentan = ""
    for iii in words:
        iii = iii.lower()
        
        if iii.endswith('meeoow'):
            version1 = (iii[-6])
            version1 = version1 + iii[0 : -6]
            version2 = iii[0 : -6]
            overall = ("({0} or {1})".format(version1, version2))
    
        elif iii.endswith('eeoow'):
            iii = iii[:-5]
            start = iii[-1]
            overall = start + iii[0:-1]
        sentan = sentan + " " + overall
    return(sentan[1 : :])

def file_in_english(filename, character_limit):
    """printing converted with limit"""
    infile = open(filename)
    characters = 0
    count = 0
    converted_sentance = ""
    lines = infile.readlines()
    while (count < (len(lines))) and (characters <= character_limit):
        converted_sentance += "\n" + english_sentence(lines[count])
        characters = len(converted_sentance)
        count = count + 1
    message = '\n'.join(converted_sentance.split('\n')[1:])
    if characters >= character_limit:
        converted_sentance = '\n'.join(converted_sentance.split('\n')[1:-1])
        message = ("{0}\n<<Output limit exceeded>>".format(converted_sentance))
    if count == 1:
        message = "<<Output limit exceeded>>"

    return(message)
    
           
	
ans = file_in_english('test1.txt', 92)
print(ans)