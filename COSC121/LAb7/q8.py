def find_first(value, array, start_index=0):
    """Not a valid solution, does it find the first instance?"""
    found_index = -1
    for i in range(start_index, len(array)):
        if value == array[i]:
            found_index = i
    return found_index

print(find_first(15, [12, 13, 14, 15], 3))