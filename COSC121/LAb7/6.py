def print_quiz_mark(mark_gained, max_mark = 40):
    """printing marks"""
    
    mark_gained = float(mark_gained)
    max_mark = float(max_mark)
    percentage = float((mark_gained / max_mark) * 100)
    print("Your mark: {}/{} ({:.1f}%)".format(mark_gained, max_mark, percentage))

print_quiz_mark(15, 20)