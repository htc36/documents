def row_sums(square):
    """adding rows"""
    rowsum = []
    for iii in square:
        rowsum.append(0)
    check = -1   
    for iii in square:
        check += 1
        for jjj in iii:
            rowsum[check] += jjj
    return(rowsum)

square = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 16]
]
print(row_sums(square))