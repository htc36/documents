def print_names2(people):
    """Print a list of people's names, which each person's name
       is itself a list of names (first name, second name etc)
    """
    i = 0
    while i < len(people):
        space = ' '
        if len(people[i]) == 2:
            print(people[i][0] + space + people[i][1])
        else:
            print(people[i][0])
        i += 1

        
print_names2([['Bilbo', 'Baggins'], ['Gollum'], ['Tom', 'Bombadil'], ['Aragorn']])
