def print_squares_to_number(number):
    """times tables"""
    if number < 1:
        return print("ERROR: number must be at least 1")
    for iii in (range(1, number + 1)):
        iii = int(iii)
        print('{0} * {0} = {1}'.format(iii, (iii*iii)))

print_squares_to_number(10)



