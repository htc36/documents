def num_rushes(slope_height, rush_height_gain, back_sliding):
    """Herbert the Heffalump"""
    current_height = 0
    rushes = 0
    while current_height < slope_height:
        current_height += rush_height_gain
        rush_height_gain = rush_height_gain * 0.95
        rushes += 1
        if current_height >= slope_height:
            return rushes
        else:
            current_height = current_height - back_sliding
            back_sliding = back_sliding * 0.95
    return rushes

ans = num_rushes(100, 15, 7)
print(ans)