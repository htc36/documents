def max_num_in_file(filename):
    """finding max number from external txt"""
    numbers = []
    infile = open(filename)
    lines = (infile.readlines())
    for iii in lines:
        numbers.append((int(iii)))
    return(max(numbers))

answer = max_num_in_file('max_num_in_file_test_01.txt')
print(answer)