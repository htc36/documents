def generate_daily_totals(input_filename, output_filename):
    infile = open(input_filename)
    lines = (infile.readlines())
    outfile = open(output_filename, 'w')
    count = 0
    for iii in lines:
        pieces = iii.split(",")
        summ = pieces[1: :]
        result = 0
        for jjj in summ:
            result = result + float(jjj)
        count = count + 1
        if (len(lines)) == count:
            form = ("{0} = {1:.2f}".format(pieces[0],result))
        else:
            form = ("{0} = {1:.2f}\n".format(pieces[0],result))
        outfile.write(form)
        
    infile.close()
    outfile.close()
    
    
    
    
generate_daily_totals('data60.txt', 'totals60.txt')
checker = open('totals60.txt')
print(checker.read())
checker.close()