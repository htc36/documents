def write_reversed_file(input_filename, output_filename):
    """reversed but writing to file"""
    infile = open(input_filename)
    count = 0
    lines = (infile.readlines())
    rev = lines[-1: 0 : -1]
    result = []
    outfile = open(output_filename, 'w')
    for iii in rev:
        result.append(iii.rstrip())
    result.append(lines[0].rstrip())    
    for jjj in result:
        count = count + 1
        outfile.write(jjj)
        if count != len(result):
            outfile.write("\n")
        
import os.path
write_reversed_file('data.txt', 'reversed1.txt')
if not os.path.exists('reversed1.txt'):
    print("You don't seem to have created the required output file!")
else:
    print(open('reversed1.txt').read())

"""def print_reversed(filename):
  
    infile = open(filename)
    lines = (infile.readlines())
    rev = lines[-1: 0 : -1]
    for iii in rev:
        print(iii.rstrip())
    print(lines[0].rstrip())
          
print_reversed('data.txt')"""