def sum_numbers_in_file(filename):
    """summing numbers from external text file"""
    infile = open(filename)
    summ = 0
    lines = (infile.readlines())
    for iii in lines:
        iii = int(iii)
        summ = summ + iii
    return summ
    

    
answer = sum_numbers_in_file('sum_nums_test_01.txt')
print(answer)
    