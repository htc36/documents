"""practising mstuf"""

def fun_function(n_items, cost_per_item, discount_percent, discount_threshold):
    cost = n_items * cost_per_item     
    if n_items > discount_threshold:           
        cost = cost * (1 - discount_percent / 100)    
        return cost
    return cost
    

def main():
    # First initialise all variables
    cost_per_item = 27      # Without discount
    discount_percent = 10
    discount_threshold = 20

    # Get the number of items in this delivery
    n_items = int(input("Count of items? "))
    cost = fun_function(n_items, cost_per_item, discount_percent, discount_threshold)
    # Print the results
    print('{} items cost ${:.2f}'.format(n_items, cost))    


# Call the main function to run the program
main()