def find_key(input_dict, value):
    """inverting the dictionary"""
    inverted = {}
    for number, letter in input_dict.items():
        inverted[letter] = number
    return inverted.get(value)
    
  
test_dictionary = {100:'a', 20:'b', 3:'c', 400:'d'}
print(find_key(test_dictionary, 'e'))