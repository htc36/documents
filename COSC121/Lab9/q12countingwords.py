def generate_dictionary(filename):
    """counts words from imported list"""
    infile = open(filename)
    lines = infile.readlines()
    counts = {}
    for iii in lines:
        iii = iii.strip()
        if iii.strip() != "":
            counts[iii] = counts.get((iii), 0) + 1
            
    return counts

	
	
	
	
	
	
# Testing with the example data in the question
dictionary = generate_dictionary('data.txt')
for key in dictionary:
    print(key + ': ' + str(dictionary[key]))