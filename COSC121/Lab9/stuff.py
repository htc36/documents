def character_set_counts(string_1, string_2, string_3):
    """ common strings in 3 sets of strings"""
    common1 = set(string_1) & set(string_2)
    common2 = common1 - set(string_3)
    return(len(common2))

answer = character_set_counts('abcdef', 'bcd', 'ace')
print(answer)

myset1 = {1, 2, 3}
myset2 = {4, 5, 6}
print(myset1.isdisjoint(myset2))
print(myset1.symmetric_difference(myset2) == myset1.union(myset2))

print(len(myset1.intersection(myset2)) == 0)

word_freq = {"this": 4, "is": 2, "a": 3, "list": 1}
print(word_freq["a"] - word_freq["list"])
print(word_freq.get('that', 2))    
print(word_freq.get("is"))
print(list(word_freq.items())[1][1])   
print(sorted(word_freq.values())[2])
print(sorted(word_freq.items())[2][1] + 1)
   