import re
def print_word_counts(filename):
    """ printing dictonarys"""
    input_file = open(filename, 'r')
    source_string = input_file.read().lower()
    input_file.close()
    words = re.findall('[a-zA-Z]+', source_string)
    counts = {}
    for iii in sorted(words):
        iii = iii.strip()
        if iii.strip() != "":
            counts[iii] = counts.get((iii), 0) + 1
    for key, value in counts.items():  
        print("{}: {}".format(key, value))


"""Test using a file containing the following 2 lines:
This is the way, this is.
That was the way, that was.
"""
print_word_counts('dictionaryinputtestfile.txt')