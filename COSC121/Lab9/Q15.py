def generate_index(words_on_page):
    """inverting diconarty"""
    inverted = {}
    for page_num, words in words_on_page.items():
        for iii in words:
            iii = iii.lower()
            if iii in inverted:
                inverted[iii].append(page_num)
            else:
                inverted[iii] = [page_num]
    return inverted

input_dict = {
   1: ['hi', 'There', 'fred'], 
   2: ['there', 'we', 'go'],
   3: ['fred', 'was', 'there']}
output_dict = generate_index(input_dict)
for word in sorted(output_dict.keys()):
    print(word + ': ' + str(output_dict[word]))
    
