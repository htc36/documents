from tkinter import *
from tkinter.ttk import *

# Insert some code here (hint: event handlers)

def main():
    """Construct the GUI and run it"""

    global message_label
    window = Tk()
    message_label = Label(window, text="Click a button!")
    message_label.grid(row=0, column=0)

    # Insert some code here (hint: buttons)

    window.mainloop()

main()