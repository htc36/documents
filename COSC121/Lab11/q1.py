"""Display a windowing saying 'COSC121'"""
from tkinter import *
from tkinter.ttk import *

def main():
    """Construct the GUI and start it running"""
    window = Tk()
    my_label = Label(text="Hello")  # Initial value
    ... 
    my_label.configure(text="Goodbye")  # Later    
    window.mainloop()

main()