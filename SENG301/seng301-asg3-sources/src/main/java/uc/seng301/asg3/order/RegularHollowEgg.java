package uc.seng301.asg3.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uc.seng301.asg3.egg.ChocolateEggFactory;
import uc.seng301.asg3.egg.ChocolateType;
import uc.seng301.asg3.packaging.PackagingType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public class RegularHollowEgg implements CreateContainer {
    private PreparingOrder prepOrder;
    private final Logger logger = LogManager.getLogger(RegularBox.class.getName());
    private int fillingOptions;
    private int factoryIndex;
    private int hollowsUsed;


    /**
     * Default constructor, sets up hollow filling options and factorys
     * Sets the outside egg that all the eggs will be contained in
     *
     * @param prepOrder PreparingOrder object
     */

    public RegularHollowEgg(PreparingOrder prepOrder) {
        this.prepOrder = prepOrder;
        fillingOptions = prepOrder.getStuffedEggFactory().getFillingOptions(prepOrder.containsAlcohol) + 1;
        hollowsUsed = 0;
        factoryIndex = ThreadLocalRandom.current().nextInt(fillingOptions);

        ChocolateType outsideEgg;
        if (prepOrder.chocolateType == ChocolateType.CRUNCHY) {
            outsideEgg = ChocolateType.WHITE;
        }else {
            outsideEgg = prepOrder.chocolateType;
        }
        prepOrder.packaging.addChocolateEgg(prepOrder.produceEgg(prepOrder.getHollowEggFactory(), outsideEgg, false));
    }

    /**
     * Gets the next factory according to the specs, if user selects no stuffed eggs, will only output
     * HollowChocolateEgg factories, if stuffed eggs are selected output 20-30%
     *
     * @param isStuffed boolean value of users choice if they want stuffed eggs or not
     */

    @Override
    public ChocolateEggFactory getNextFactory(boolean isStuffed) {
        ChocolateEggFactory chocEggFactory;
        if (!prepOrder.stuffed) {
            return prepOrder.getHollowEggFactory();
        }
        if (isThereAnyHollowsLeft()) {
            chocEggFactory =  factoryIndex++ % fillingOptions != 0 && isStuffed ?
                    prepOrder.getStuffedEggFactory() : prepOrder.getHollowEggFactory();
            if (chocEggFactory == prepOrder.getHollowEggFactory()) {
                hollowsUsed++;
            }
        }else {
            chocEggFactory = prepOrder.getStuffedEggFactory();
        }
        return  chocEggFactory;
    }
    @Override
    public ChocolateType nextChocolateType() {
        return prepOrder.chocolateType;
    }

    /**
     * Helper function to check if any more hollow chocolate eggs can be added
     *
     * @return boolean, true if a hollow egg can be added, false if not
     */
    public boolean isThereAnyHollowsLeft() {
        return hollowsUsed < (prepOrder.quantity * 0.25);
    }
}
