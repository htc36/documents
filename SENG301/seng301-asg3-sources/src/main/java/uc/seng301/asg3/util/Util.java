package uc.seng301.asg3.util;

import uc.seng301.asg3.egg.ChocolateEgg;
import uc.seng301.asg3.packaging.EggIterator;

public class Util {

    /**
     * Takes in an EggIterator and a chocolate egg to be added.
     * Finds an index in the list of the iterator where the new egg is not
     * the same as the eggs it is placed next to in the list
     *
     * @param egg the egg to be added
     * @param eggs the egg iterator object
     *
     */
    public static void eggSorter(EggIterator eggs, ChocolateEgg egg) {
        ChocolateEgg nextEgg;

        if (eggs.getEggSize() == 0) {
            eggs.addEgg(egg);
            return;
        }
        if (eggs.getEggSize() == 2) {
            if (eggs.current().checkEggsEqual(eggs.next()) && !eggs.current().checkEggsEqual(egg)) {
                eggs.incrementIndex();
                eggs.addEgg(egg);
                return;
            }
        }

        if (!eggs.current().checkEggsEqual(egg)) {
            eggs.addEgg(egg);
            return;
        }

        while (eggs.hasNext()) {
            nextEgg = eggs.next();
            if (!eggs.current().checkEggsEqual(egg) && !nextEgg.checkEggsEqual(egg)) {
                eggs.addEgg(egg);
                return;
            }else {
                eggs.incrementIndex();

            }
        }
        eggs.addEgg(egg);
        eggs.reset();
    }
}
