package uc.seng301.asg3.order;

import uc.seng301.asg3.egg.ChocolateEggFactory;
import uc.seng301.asg3.egg.ChocolateType;

/**
 * This interface specifies how different containers can be compared
 *
 */

public interface CreateContainer {
    ChocolateEggFactory getNextFactory(boolean isStuffed);
    ChocolateType nextChocolateType();
}
