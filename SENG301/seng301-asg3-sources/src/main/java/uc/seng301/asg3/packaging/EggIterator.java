package uc.seng301.asg3.packaging;

import uc.seng301.asg3.egg.ChocolateEgg;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class EggIterator implements Iterator {
    private List<ChocolateEgg> eggList;
    private int index;

    /**
     * Default constructor, create empty list for eggs, set index to 0
     *
     */

    public EggIterator() {
        this.eggList = new CopyOnWriteArrayList<>();
        index = 0;
    }

    /**
     * Checks if index is at the end of the list
     *
     * @return true if at the end of the list and false if not
     */
    @Override
    public boolean hasNext() {
        return (index) + 1 < eggList.size();
    }

    /**
     * Gets the next Chocolate Egg object in the list
     *
     * @return ChocolateEgg, the egg after the current egg
     */
    @Override
    public ChocolateEgg next() {
        return eggList.get(index + 1);
    }

    /**
     * Resets the index of the current to the start of the chocolate egg list
     *
     */
    public void reset() {
        index = 0;
    }

    /**
     * Gets the amount of chocolate eggs that are already added
     *
     * @return int, the number of chocolate eggs
     */
    public int getEggSize() {
        return this.eggList.size();
    }

    /**
     * Returns the list of chocolate eggs that are already added
     *
     * @return List<ChocolateEgg>, list of chocolate eggs that have been added
     */
    public List<ChocolateEgg> getEggs() {
        return this.eggList;
    }

    /**
     * Returns the chocolate egg that the index is currently on
     *
     * @return List<ChocolateEgg>, list of chocolate eggs that have been added
     */
    public ChocolateEgg current() {
        return eggList.get(index);
    }

    /**
     * Adds a chocolate egg to the list of eggs
     *
     * @param egg the egg to be added
     */
    public void addEgg(ChocolateEgg egg) {
        eggList.add(index , egg);
    }

    /**
     * Increments the index by one
     *
     */
    public void incrementIndex() {
        index++;
    }

    /**
     * Removes an egg from the list of eggs
     *
     * @param egg the egg to be removed
     */
    public boolean removeEgg(ChocolateEgg egg) {
        return eggList.remove(egg);
    }
}
