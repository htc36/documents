package uc.seng301.asg3.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uc.seng301.asg3.egg.ChocolateEggFactory;
import uc.seng301.asg3.egg.ChocolateType;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public class RegularBox implements CreateContainer{
    private PreparingOrder prepOrder;
    private int fillingOptions;
    private int factoryIndex;

    /**
     * Default constructor. set the preparing order object get filling options and random factory index
     *
     * @param prepOrder the Preparing Order object
     */

    public RegularBox(PreparingOrder prepOrder) {
        this.prepOrder = prepOrder;
        fillingOptions = prepOrder.getStuffedEggFactory().getFillingOptions(prepOrder.containsAlcohol) + 1;
        factoryIndex = ThreadLocalRandom.current().nextInt(fillingOptions);
    }


    /**
     * Gets the next factory according to the specs, makes sure the correct amount of hollow and stuffed
     * eggs are assigned
     *
     * @param isStuffed boolean value of users choice if they want stuffed eggs or not
     */
    @Override
    public ChocolateEggFactory getNextFactory(boolean isStuffed) {
    return factoryIndex++ % fillingOptions != 0 && isStuffed ?
            prepOrder.getStuffedEggFactory() : prepOrder.getHollowEggFactory();
    }

    /**
     * Gets the next chocolate type, this will always return the users choice of chocolate type
     *
     */
    @Override
    public ChocolateType nextChocolateType() {
        return prepOrder.chocolateType;
    }


}
