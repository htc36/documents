package uc.seng301.asg3.order;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uc.seng301.asg3.egg.ChocolateEggFactory;
import uc.seng301.asg3.egg.ChocolateType;

public class MixedBox implements CreateContainer{
    private PreparingOrder prepOrder;
    private final Logger logger = LogManager.getLogger(MixedBox.class.getName());
    private int fillingOptions;
    private int chocolateTypeOptions;
    private int chocolateIndex;
    private int factoryIndex;
    private int crunchiesUsed;

    /**
     * Default constructor. set the preparing order object get filling options and random factory index
     *
     * @param prepOrder the Preparing Order object
     */
    public MixedBox(PreparingOrder prepOrder) {
        this.prepOrder = prepOrder;
        fillingOptions = prepOrder.getStuffedEggFactory().getFillingOptions(prepOrder.containsAlcohol) + 1;
        chocolateTypeOptions = prepOrder.quantity >= 10 ? 4 : 3;
        chocolateIndex = ThreadLocalRandom.current().nextInt(chocolateTypeOptions);
        factoryIndex = ThreadLocalRandom.current().nextInt(fillingOptions);
        crunchiesUsed = 0;
    }

    /**
     * Gets the next factory according to the specs, makes sure the correct amount of hollow and stuffed
     * eggs are assigned, should produce eggs that result to an even amount of hollows to each filling
     *
     * @param isStuffed boolean value of users choice if they want stuffed eggs or not
     */
    @Override
    public ChocolateEggFactory getNextFactory(boolean isStuffed) {
    return factoryIndex++ % fillingOptions != 0 && isStuffed ?
            prepOrder.getStuffedEggFactory() : prepOrder.getHollowEggFactory();
    }

    /**
     * Gets the next chocolate type, will make sure a maximum of 10% of the quantity of eggs is crunchie
     * Makes sure even distribution of chocolate types
     *
     */
    @Override
    public ChocolateType nextChocolateType() {
        ChocolateType nextChocolateType;
        if (isThereAnyCrunchiesLeft()) {
            nextChocolateType = ChocolateType.values()[chocolateIndex];
            if (nextChocolateType == ChocolateType.CRUNCHY) {
                crunchiesUsed ++;
            }
            if (!isThereAnyCrunchiesLeft()){
                chocolateIndex = 0;
                chocolateTypeOptions -= 1;
            } else {
                chocolateIndex = (chocolateIndex+1) % chocolateTypeOptions;
            }
        } else {
            nextChocolateType = ChocolateType.values()[chocolateIndex];
            chocolateIndex = (chocolateIndex+1) % chocolateTypeOptions;
        }
        return nextChocolateType;

    }


    /**
     * Helper function to check if any more chruchie chocolate types can be added
     *
     * @return boolean, true if crunchie chocolate type can be added false if not
     */
    public boolean isThereAnyCrunchiesLeft() {
        return crunchiesUsed < (prepOrder.quantity / 10);
    }
}
