# SENG301 Assignment 1 (2020) - student answers


## Task 1 - Identify the patterns

### Pattern 1 -  Abstract Factory

- Abstract Factory
- The pattern allows for the deferral of instantiation to the subclasses. This means we can separate
the implementation for Hollow Chocolate Egg and Stuffed Chocolate Egg. Hence we can have a 
HollowEggFactory and StuffedEggFactory implement a main ChocolateEggFactory interface. The 
subsequent subclasses of the factory's can then implement different egg types, which allows us
to seperate concerns, reduce allot of coupling and have the application open to extension. As it
is easy to add a new class which implements the main factory
- Name of UML Class diagram attached: pattern1.png
- Mapping to GoF pattern elements:

| GoF element           | Code element          |
|-----------------------|-----------------------|
| Abstract Creator      |ChocolateEggFactory    |
| Concrete Creator      |HollowEggFactory       |
| Concrete Creator      |StuffedEggFactory      |
| Abstract Product      |ChocolateEgg           |
| Concrete Product      |StuffedChocolateEgg    |
| Concrete Product      |HollowChocolateEgg     |
| Factory Method        |createChocolateEgg()   |

### Pattern 2 - Command

- Command Pattern
- The command pattern allows being able to create a sequence of commands by providing a 
 queue system. In context this means that the Counter class can queue Order objects, allowing 
 for many objects to be given to the counter, this in turn will allow less coupling between order
 and Counter.

- Name of UML Class diagram attached: pattern2.png
- Mapping to GoF pattern elements:

|GoF element         | Code element          |
|--------------------|-----------------------|
|Invoker             |Counter                |
|Command             |Order                  |
|Concrete Command    |Preparing Order        |
|Receiver            |Chocolatier            |
|Client              |Client                 |


### Pattern 3 - Composite 

- What is its goal in the code?
- The pattern allows for the accessing class to ignore any differences between the composition of 
the individual objects. In context this means that the Client class does not need to know weather
it is dealing with an stuffed egg or a hollow egg. It also adheres to the open closed principle 
where it is open to extension as you can easily add a new type of chocolate egg, but closed 
for modification of the individual classes. 

- Name of UML Class diagram attached:pattern3.png
- Mapping to GoF pattern elements:

| GoF element           | Code element          |
|-----------------------|-----------------------|
|Component              |ChocolateEgg           |
|Composite              |HollowChocolateEgg     |
|Leaf                   |StuffedChocolateEgg    |
|Client                 |App                    |

## Task 2 - Full UML Class diagram

- Name of file of full UML Class diagram attached: fullUML.png

## Task 3 - Implement new features

### Task 3.1 - balanced packaging 

- Strategy Pattern
- The goal of the strategy pattern in this situation is to keep unique implementation of the same methods
separate. A CreateContainer interface can be created which links all the common methods together 
to allow polymorphism to occur. Each different type of packaging has a different way of implementing
getting their next factory and next chocolate type, hence the strategy pattern will allow these
methods to be separated and implemented separately in their class, while keeping common implementation
together. This allows for extension, as you could easily create a new class for a new packaging type, 
while not having to modify any existing code
- Name of UML Class diagram attached: task3-1.png
- Mapping to GoF pattern elements:

| GoF element           | Code element          |
|-----------------------|-----------------------|
|Context                |PreparingOrder         |
|Strategy               |CreateContainer        |
|ConcreteStrategy1      |MixedBox               |
|ConcreteStrategy2      |RegularBox             |
|ConcreteStrategy3      |MixedHollowEgg         |
|ConcreteStrategy4      |RegularHollowEgg       |
|Method1                |getNextFactory()       |
|Method2                |getNextChocolateType() |


### Task 3.2 - fill in the packages with eggs

- Iterator pattern
- What is its goal and why is it needed here?
- The goal of the iterator pattern is to be able cycle through a collection of items, while also
moving the responsibility of traversing away from logic class. It is used in this scenario as we
 have to check if any eggs next to each other are the same. As the eggs are stored in a list, 
when adding new eggs, we can iterate through the list to find a location where new egg is not the
same as its neighbours
- Name of UML Class diagram attached: task: 3.2.png
- Mapping to GoF pattern elements:

| GoF element           | Code element          |
|-----------------------|-----------------------|
| Collection            |List                   |
| ConcreteCollection    |ArrayList              |
| ConcreteIterator      |EggIterator            |
| Client                |Packaging              |
| Client                |HollowChocolateEgg     |
