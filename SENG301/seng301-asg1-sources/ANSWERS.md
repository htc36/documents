# SENG301 Assignment 1 (2020) - student answers

## Architecture decisions

The architecture used is similar to a domain model. Whereby there is an accessor layer which is the only layer that talks to the database. The next layer is the service layer which is the is the middleman between the accessor and app.java. This handles all the logic of the user stories. Finally, there is the App.java this is where all user input 
 is processed, this layer was changed slightly from the template as it has logic in it as well, however, this was usually just processing the output and error codes from the service layer.

## Task 1 - U1-AC6

Briefly explain where you modified the code (which method/s) and what modifications 
you applied using the form:
- `service.IngredientService.addIngredient` update behaviour if accessor.ingredientExists(name), was false return -2
- `App.addIngredientMenu` update behaviour to be able to handle -2 from addIngredient method, if it was -2 return the appropriate message


## Task 2 - U2

### Task 1 - Create stubs for basic feature files with empty tests 

- Estimation (in hours) : 1
- Time spent (in hours) : 1
- Description: Add to `Ingredient.feature` and create `Recipe.feature`, add basic outlining tests
based on the acceptance criteria


### Task 2 - The user can input a recipe name

- Estimation (in hours) : 2
- Time spent (in hours) : 2.5
- Description: Add a function in `App` that asks the user to input name for recipe, Create a new service
file for recipes, create skeleton code with similar functionality to `IngredientService` 

### Task 3 - Check if Recipe is unique to the database

- Estimation (in hours) : 1
- Time spent (in hours) : 1
- Description: create a function that checks if the recipe exists in the database

### Task 4 - Enforce recipe to be in the correct format

- Estimation (in hours) : 1
- Time spent (in hours) : 2
- Description: A recipe name cannot be empty or contain anything that's not a letter or a space

### Task 5 - Create loop until user inputs correct recipe name

- Estimation (in hours) : 1
- Time spent (in hours) : 1.5
- Description: Check if name passes task 3 and task 4, if not produce error message and ask to re-input 
a recipe name 

### Task 6 - Create preparation steps

- Estimation (in hours) : 1.5
- Time spent (in hours) : 2
- Description: Allow input for a description and ingredients that exist or create a new one, if creating a 
new ingredient it must be in the correct format just like before.

### Task 7 - Create loop of creating multiple preparation steps

- Estimation (in hours) : 0.8
- Time spent (in hours) : 1.2
- Description: Allow for multiple steps that keep being saved to the recipe as they are being created
also make sure each preparation step is numbered, starting from the number 1

### Task 8 - Duplicate recipe name implementation

- Estimation (in hours) : 0.5
- Time spent (in hours) : 1
- Description: Show a warning message with a summary of the already existing recipe, give the option
to create a new recipe with the same name

### Task 9 - Save recipe to database

- Estimation (in hours) : 1
- Time spent (in hours) : 1
- Description: Save the recipe to the database and print its id

## Task 3 - retrospect

### What went well?
I tried to stick to the plan that I created as much as I could, which is quite unusual for me because most of the time I
don't really plan at all, however this time using a plan allowed me to have a clear picture on what I have done
and what I have yet to do. I kept most of the architecture given to us the very similar to the template and expanded 
it using the same principles, for example creating the recipe accessor class. I could easily get my head around what was 
required of the assignment and felt I could easily write good unit and cucumber tests so I can be ensured that my implementation is correct 

### What needs to be improved?

I had several issues regarding lazy initialisation when trying to print out a recipe object. This took a vast amount of time 
to fix as I was not sure what was going on. Hence I need to work on my problem solving skills as I found out later on 
that many other students had encountered the same issue, meaning if I went to the help sessions or contacted other students, I
would of found a solution earlier.

### Action items

When stuck on a bug or issue that takes longer than 40 minutes, take a break,eat some food amd take a walk outside.
Before starting work, clear desk of all distractions and only have equipment on my desk that I am going to use
Use test driven development for my programming, before starting the project write my tests first then test functions as I make them


