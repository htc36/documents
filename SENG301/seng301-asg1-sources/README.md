# SENG301 Assignment 1 (2020)

## Context

This material is part of the SENG301 assignment 1. It contains a series of Java 
classes and tests meant to set the framework for students to implement the full 
set of features covered by the assignment. 

This assignment is meant to implement a simplified electronic recipe book with 
the following features:

- Add new ingredient (partially implemented for you)
- Add new recipe 
- Display the content of the database (for debug purposes)

## Authors

Initial contribution by Fabian Gilson

## Test and run the project

This project relies on gradle (version 5 or later). See `build.gradle` file for 
full list of dependencies. You can use the built-in scripts to bootstrap the 
project (`gradlew` on Linux/Mac or `gradlew.bat` on Windows).

To build the project (including running the tests), place yourself at the root 
folder of this project, then, in a command line:

- On Windows: type `gradlew.bat build` 
- On Linux/Mac: type `./gradlew build` 

To run the unit tests only, from the root folder:

- On Windows: type `gradlew.bat test` 
- On Linux/Mac: type `./gradlew test`

To run the acceptance tests, from the root folder:

- On Windows: type `gradlew.bat cucumber` 
- On Linux/Mac: type `./gradlew cucumber`

To run the Common Line Interface application, from the root folder:

- On Windows: type `gradlew.bat --console=PLAIN run` 
- On Linux/Mac: type `./gradlew --console=PLAIN run`

The option `--console=PLAIN` is passed to suppress part of the coloured output 
of gradle that may interfere with the CLI. More details about gradle, see 
[Gradle Website](https://gradle.org/).

## Copyright notice

Copyright (c) 2020. University of Canterbury

This file is part of SENG301 lab material. 

The lab material is free software: you can redistribute it and/or modify it under 
the terms of the GNU Lesser  General Public License as published by the Free 
Software Foundation, either version 3 of the License, or (at your  option) any 
later version.

The lab material is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along 
with this material.  If not, see https://www.gnu.org/licenses/. 