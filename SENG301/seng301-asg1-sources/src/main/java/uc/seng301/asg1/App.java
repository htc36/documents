
/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import uc.seng301.asg1.service.IngredientService;
import uc.seng301.asg1.service.RecipeService;
import uc.seng301.asg1.entity.Recipe;
import uc.seng301.asg1.entity.Ingredient;
import uc.seng301.asg1.entity.PreparationStep ;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import java.util.Scanner;

/**
 * This class contains the main method of MyRecipe App. It handles
 * <ul>
 * <li> all Command Line Interface (CLI) menu items</li>
 * <li> the management of Java Persistence API (JPA) sessions</li>
 * </ul>
 */
public class App {

  // a JPA session factory is needed to access the actual data repository
  private final SessionFactory ourSessionFactory;

  // the scanner for the CLI
  private final Scanner cli;

  // a logger is useful to print messages in a configurable and flexible way
  // see resources/log4j2.xml configuration file
  private final Logger logger = LogManager.getLogger(App.class.getName());

  /**
   * Default constructor
   */
  public App() {
    Configuration configuration = new Configuration();
    configuration.configure();
    ourSessionFactory = configuration.buildSessionFactory();
    cli = new Scanner(System.in);
  }

  /**
   * Get a JPA session factory (i.e. to create sessions to interact with the database)
   * @return the JPA session factory
   * @see org.hibernate.SessionFactory
   */
  public SessionFactory getSessionFactory() {
    return ourSessionFactory;
  }

  /**
   * Get the command line reader to read input from users
   * @return the Scanner opened on the command line
   */
  public Scanner getScanner() {
    return cli;
  }

  /**
   * Main method, starts the Command Line Interface client for MyRecipe app
   * @param args none expected
   */
  public static void main(final String[] args) {
    App app = new App();
    IngredientService ingredientService = new IngredientService(app.getSessionFactory());
    RecipeService recipeService = new RecipeService(app.getSessionFactory());

    System.out.println(app.welcome());

    boolean quit = false;
    while (!quit) {
      System.out.println(app.mainMenu());
      String input = app.getScanner().nextLine();
      int menuItem;
      try {
        menuItem = Integer.parseInt(input);
      } catch (NumberFormatException e) {
        menuItem = -1;
      }
      switch (menuItem) {
        case 1:
          app.addIngredientMenu(app.getScanner(), ingredientService);
          break;

        case 2:
          app.addRecipeMenu(app.getScanner(), recipeService);
          break;

        case 9:
          System.out.println(app.serializeDatabaseContent());
          break;

        case 0:
          quit = true;
          break;

        default:
          System.out.println("Unknown value entered");
      }
    }
  }


  /*----------------
   * PRIVATE METHODS
   ----------------*/

  /**
   * Create the welcome message for the MyRecipe App
   *
   * @return the App banner
   */
  private String welcome() {
    return "\n######################################################\n\n" +
        "  Welcome to MyRecipe - the ultimate recipe book app\n\n" +
        "######################################################";
  }

  /**
   * Create the main menu of MyRecipe App
   *
   * @return the menu with all top level user options
   */
  private String mainMenu() {
    return "\nWhat do you want to do next?\n" +
        "\t 1. Add an ingredient\n" +
        "\t 2. Add a recipe\n" +
        "\t 9. Print database content\n" +
        "\t 0. Exit\n" +
        "\n" +
        "Your Answer: ";
  }

  /**
   * Menu handling the creation of a new ingredient
   * @param scanner the input stream to get user input from
   * @param service the ingredient service layer
   */
  private void addIngredientMenu(Scanner scanner, IngredientService service) {
    System.out.println("Enter a name for your ingredient:");
    String name = scanner.nextLine();
    int ingredientId;

    while ((ingredientId = service.addIngredient(name)) == -1 || ingredientId == -2) {
      if (ingredientId == -1){
        System.out.println("Invalid name for ingredient (contains other characters than letters), please try again");
      }
      if (ingredientId == -2) {
        System.out.println("Error name already exists in database, returning to the main menu");
        return;
      }

      name = scanner.nextLine();
    }
    System.out.println("Ingredient added with ID:" + ingredientId);
  }

  /**
   * Menu handling the creation of a new recipe
   * @param scanner the input stream to get user input from
   * @param service the recipe service layer
   */
  private void addRecipeMenu(Scanner scanner, RecipeService service) {
    System.out.println("Enter a name for your recipe:");
    String name = scanner.nextLine();
    int recipeCheck;
    boolean exit = false;
    boolean cont = true;

    while ((exit == false) && ((recipeCheck = service.checkRecipe(name)) == -1 || (recipeCheck == -2))) {
      if (recipeCheck == -1){
        System.out.println("Invalid name for recipe (contains other characters than letters), please try again");
        name = scanner.nextLine();
      }
      if (recipeCheck == -2) {
        System.out.println("Error name already exists in database. Current recipe is:");
        getRecipeByName(name, service);
        System.out.println("Do you want to create a recipe with the same name? Y/N");
        String answer = scanner.nextLine();
        if(answer.equals("Y")){
          exit = true;
        }else if (answer.equals("N")){
          cont = false;
          exit = true;
        }else {
          name = scanner.nextLine();
        }
      }
    }
    if(cont == false){
      System.out.println("Returning to main menu");
      return;
    }
    Recipe recipe = new Recipe();
    recipe.setName(name.trim());

    List<PreparationStep> preparationList = addPreparationStep(scanner, name, service, recipe);
    recipe.setPreparationSteps(preparationList);
    service.saveRecipe(recipe);
    for (PreparationStep prepstep : preparationList) {
      System.out.println(prepstep);
      prepstep.setRecipe(recipe);
      service.savePreparationStep(prepstep);
    }
    System.out.println("Recipe added with ID: " + recipe.getIdRecipe());
  }
  private void getRecipeByName(String name, RecipeService service){
    Recipe dRecipe = service.getRecipe(name);
    try (Session session = ourSessionFactory.openSession()) {
      session.update(dRecipe);
      System.out.println(dRecipe);
    }catch (Exception h) {
      System.out.println("There are multiple recipes with the same name");
    }


  }
  private List<PreparationStep> addPreparationStep(Scanner scanner, String name, RecipeService service, Recipe recipe) {
    String description;
    boolean ingredientPass = false;
    String ingredient;
    List<PreparationStep> preparationStepList= new ArrayList<PreparationStep>();
    Set<Ingredient> ingredientsSet = new HashSet<Ingredient>();
    short stepNumber = 0;
    preparationStepList.clear();

    while (true) {
      stepNumber++;
      ingredientPass = false;
      System.out.println("Enter preparation step description");
      description = scanner.nextLine();
      while (description.isBlank()){
          if(description.isBlank()){
            System.out.println("Preparation step description can not be blank");
            System.out.println("Please retry:");
          }
        description = scanner.nextLine();
        }

      while (!ingredientPass) {
        System.out.println("Enter list of ingredients involved seperated by commas in the format: name, name2, name3");
        String[] ingredientCommas = scanner.nextLine().split(",");
        ingredientsSet = service.addIngredientFromRecipe(ingredientCommas, getSessionFactory());
        if (ingredientsSet != null) {
          ingredientPass = true;
        }
      }
      PreparationStep preparationStep = new PreparationStep();
      preparationStep.setDescription(description);
      preparationStep.setIngredients(ingredientsSet);
      preparationStep.setStepNumber(stepNumber);
      preparationStepList.add(preparationStep);
      System.out.println("Do you wnat to add another preperation step type 'N' to stop adding");
      if(scanner.nextLine().equals("N")){
        break;
      }
    }
    return preparationStepList;

  }


  /**
   * Compile the content of all entities stored in the database into user-friendly String.<br>
   * Highly relies on all Entities's toString() method to be fully user-friendly.
   *
   * @return a serialised version of the content of the database.
   */
  private String serializeDatabaseContent() {
    StringBuilder result = new StringBuilder();
    // by using try-with-resource, any resource will be closed auto-magically at the end of the surrounding try-catch
    // see https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
    // if a resource is not opened inside a try-with-resource (or is not (Auto)Closeable, remember to
    // close it manually in the finally statement of a try-catch
    try (Session session = ourSessionFactory.openSession()) {
      logger.info("querying all managed entities in database");
      Metamodel metamodel = session.getSessionFactory().getMetamodel();
      for (EntityType<?> entityType : metamodel.getEntities()) {
        String entityName = entityType.getName();
        result.append("Content of ").append(entityName).append("\n");
        Query<Object> query = session.createQuery("from " + entityName, Object.class);
        logger.info("executing HQL query '{}'", query.getQueryString());
        for (Object o : query.list()) {
          result.append("\t").append(o.toString()).append("\n");
        }
      }

    } catch (HibernateException e) {
      result.append("Couldn't query content because of error").append(e.getLocalizedMessage());
      logger.error(e);
    }
    return result.toString();
  }

}
