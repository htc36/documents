/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.service;
import uc.seng301.asg1.entity.Recipe;
import uc.seng301.asg1.entity.Ingredient;
import uc.seng301.asg1.entity.PreparationStep;
import uc.seng301.asg1.App;
import uc.seng301.asg1.accessor.RecipeAccessor;
import uc.seng301.asg1.service.IngredientService;
import java.util.HashSet;
import java.util.Set;
import java.util.List;


import org.hibernate.SessionFactory;

public class RecipeService {
  private final RecipeAccessor accessor;



  public RecipeService(SessionFactory factory) {
    accessor = new RecipeAccessor(factory);
  }
  public int checkRecipe(String name) {
    if (!recipeNameIsValid(name)) {
      return -1;
    }
    if (accessor.recipeExists(name)) {
      return -2;
    }

    return 0;
  }

  public Set<Ingredient> addIngredientFromRecipe(String[] ingredientList, SessionFactory session) {
    IngredientService ingredientService = new IngredientService(session);
    for (String ingred : ingredientList) {
      if (!ingredientService.checkIngredient(ingred.strip())) {
        System.out.println("The ingredient '" + ingred + "' is invalid please retype ingredients correctly");
        return null;
      }
    }
    Set ingredientSet = new HashSet<Ingredient>();
    for (String ingred : ingredientList) {
      ingredientService.addIngredient(ingred.strip());
      ingredientSet.add(ingredientService.getIngredient(ingred));
    }

    return ingredientSet;
  }

  public Recipe getRecipe(String name){
    //System.out.println(accessor.getRecipeByName(name));
    return accessor.getRecipeByName(name);
  }


  public PreparationStep savePreparationStep(PreparationStep preparationStep){
    accessor.savePreparationStep(preparationStep);

    return preparationStep;

  }
  public int saveRecipe(Recipe recipe) {
    return accessor.save(recipe);
  }



  /**
   * Checks whether given name is valid for an ingredient
   * @param name an ingredient name to be checked
   * @return true if given name contains only letters or white space
   */
  private boolean recipeNameIsValid(String name) {
    return null != name && !name.isBlank()
            && name.chars().allMatch(i -> Character.isAlphabetic(i) || Character.isWhitespace(i));
  }
}

