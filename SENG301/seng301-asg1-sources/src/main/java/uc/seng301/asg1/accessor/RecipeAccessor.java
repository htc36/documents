/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.accessor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import uc.seng301.asg1.entity.Recipe;
import uc.seng301.asg1.entity.PreparationStep;


/**
 * This class handles the access to the Recipe beans
 */
public class RecipeAccessor {

  private final SessionFactory sessionFactory;
  private static final Logger logger = LogManager.getLogger(RecipeAccessor.class);

  /**
   * Default Constructor
   * @param sessionFactory the sessionFactory to access the database
   */
  public RecipeAccessor(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * Save an Recipe into the database
   * @param recipe the recipe to save
   * @return the recipe ID or -1 if an error occurred
   * @throws IllegalArgumentException if passed argument is null
   */
  public int save(Recipe recipe) {
    if (null == recipe) {
      throw new IllegalArgumentException("Recipe can't be null");
    }
    int result;
    Transaction transaction = null;
    try (Session session = sessionFactory.openSession()) {
      transaction = session.beginTransaction();
      result = (Short) session.save(recipe);
      transaction.commit();
      logger.debug("saved {}", recipe);
    } catch (Exception e) {
      result = -1;
      logger.error("Unable to save recipe", e);
      if (null != transaction) {
        transaction.rollback();
      }
    }
    return result;
  }

  /**
   * Save an Preperation into the database
   * @param prepStep the Preparation step to save
   * @return the recipe ID or -1 if an error occurred
   * @throws IllegalArgumentException if passed argument is null
   */
  public int savePreparationStep(PreparationStep prepStep) {
    if (null == prepStep) {
      throw new IllegalArgumentException("Recipe can't be null");
    }
    int result;
    Transaction transaction = null;
    try (Session session = sessionFactory.openSession()) {
      transaction = session.beginTransaction();
      result = (Short) session.save(prepStep);
      transaction.commit();
      logger.debug("saved {}", prepStep);
    } catch (Exception e) {
      result = -1;
      logger.error("Unable to save Preparation Step", e);
      if (null != transaction) {
        transaction.rollback();
      }
    }
    return result;
  }
  /**
   * Retrieve an recipe by its given name
   * @param name a name to look for
   * @return the Recipe if it exists in database, null otherwise
   */
  public Recipe getRecipeByName(String name) {
    Recipe result = null;
    try (Session session = sessionFactory.openSession()) {
      result = session.createQuery("FROM Recipe WHERE name ='" + name + "'", Recipe.class).uniqueResult();
    } catch (Exception e) {
      logger.error("Unable to retrieve Recipe with name " + name, e);
    }
    return result;
  }

  /**
   * Checks whether an recipe with given name already exists
   * @param name an recipe name
   * @return true if an recipe with given name already exists, false otherwise
   */
  public boolean recipeExists(String name) {
    boolean result = false;
    try (Session session = sessionFactory.openSession()) {
      result = session.createQuery(
          "SELECT COUNT(id_recipe) FROM Recipe WHERE name ='" + name + "'", Long.class)
          .uniqueResult() >= 1L;

    } catch (Exception e) {
      logger.error("Unable to retrieve recipe with name " + name, e);
    }
    return result;
  }

}
