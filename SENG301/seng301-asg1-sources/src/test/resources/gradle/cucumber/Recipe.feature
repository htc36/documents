#
# Copyright (c) 2020. University of Canterbury
#
# This file is part of SENG301 lab material.
#
#  The lab material is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  The lab material is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
#

#
# This feature file contains all acceptance scenarios related to CRUD management of recipe
#

Feature: Recipe

Scenario: Add basic recipe
  Given The recipe "Nachos" does not exist
  When the user adds the recipe "Nachos" to the database
  Then The recipe "Nachos" exists in the database

Scenario: Add new recipe
  Given The recipe "Meatballs" does not exist
  And The ingredients "Mince" and "Onions" do not exist
  And The user adds a recipe called "Meatballs"
  And The user adds the preparation step description "Cut onions and mix with mince"
  And The inserts the ingredients "Mince,Onions"
  When The user finalises the recipe
  Then The recipe "Meatballs" exists in the database
  And The ingredients "Mince" and "Onions" exist in the database
  And The "Meatballs" recipe has preparation step "Cut onions and mix with mince" with ingredients "Mince" and "Onions"

Scenario: AddInvalidRecipe
  Given The recipe "^%$$^ Orange Ruffy" does not exist
  When The user inserts an invalid recipe "^%$$^ Orange Ruffy"
  Then The recipe "^%$$^ Orange Ruffy" is not saved to the database

Scenario: AddDuplicateRecipe
  Given The recipe "Orange Ruffy" already exists in the database
  When The user adds a recipe called "Orange Ruffy"
  Then The recipe "Orange Ruffy" exists in the database





