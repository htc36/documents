
/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package gradle.cucumber.asg1.scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.sl.In;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Assert;
import uc.seng301.asg1.accessor.IngredientAccessor;
import uc.seng301.asg1.accessor.RecipeAccessor;
import uc.seng301.asg1.entity.Ingredient;
import uc.seng301.asg1.entity.PreparationStep;
import uc.seng301.asg1.entity.Recipe;
import uc.seng301.asg1.service.IngredientService;
import uc.seng301.asg1.service.RecipeService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AddRecipeScenario {

  private SessionFactory factory;
  private RecipeService service;
  private IngredientService ingredientService;
  private IngredientAccessor ingredientAccessor;
  private RecipeAccessor accessor;
  private PreparationStep testPrepStep;
  private Recipe testRecipe;
  private Set<Ingredient> ingredientSet;

  @Before
  public void setUp() {
    // see test/resource file that is using an embedded H2 database (see https://www.h2database.com/html/main.html)
    // with this h2 database, you are not using the sqlite file and therefore can create self-contained tests
    // you can simply create a test-dedicated environment by putting test resources under the test folder
    Configuration configuration = new Configuration();
    configuration.configure();

    // for acceptance tests, you may also use Mockito (see Lab.5) instead of relying on an H2 database.
    factory = configuration.buildSessionFactory();
    service = new RecipeService(factory);
    ingredientService = new IngredientService(factory);
    ingredientAccessor = new IngredientAccessor(factory);
    accessor = new RecipeAccessor(factory);
    testPrepStep = new PreparationStep();
    testRecipe = new Recipe();
    ingredientSet = new HashSet<>();
  }

  @Given("The recipe {string} does not exist")
  public void theRecipeDoesNotExist(String name) {
    Assert.assertFalse(accessor.recipeExists(name));
  }


  @And("The ingredients {string} and {string} do not exist")
  public void theIngredientsAndDoNotExist(String ingred1, String ingred2) {
    Assert.assertFalse(ingredientAccessor.ingredientExists(ingred1));
    Assert.assertFalse(ingredientAccessor.ingredientExists(ingred2));
  }

  @And("The user adds a recipe called {string}")
  public void theUserAddsARecipeCalled(String name) {
    Assert.assertNotEquals(-1, service.checkRecipe(name));
    testRecipe.setName(name);
  }

  @And("The user adds the preparation step description {string}")
  public void theUserAddsTheDescription(String description) {
    testPrepStep.setDescription(description);
  }

  @And("The inserts the ingredients {string}")
  public void theInsertsTheIngredients(String ingredients) {
    ingredientSet = service.addIngredientFromRecipe(ingredients.split(","), factory);
    testPrepStep.setIngredients(ingredientSet);
  }

  @When("The user finalises the recipe")
  public void theUserSelectsToAddAnotherPreparationStep() {
    List<PreparationStep> prepList = new ArrayList<>();
    prepList.add(testPrepStep);
    //testRecipe.setPreparationSteps((List<PreparationStep>) testPrepStep);
    testRecipe.setPreparationSteps(prepList);
    service.saveRecipe(testRecipe);
    testPrepStep.setRecipe(testRecipe);
    service.savePreparationStep(testPrepStep);
  }

  @Then("The recipe {string} exists in the database")
  public void theRecipeWithTheExistInTheDatabase(String name) {
    Assert.assertTrue(accessor.recipeExists(name));
  }

  @And("The ingredients {string} and {string} exist in the database")
  public void theIngredientsAndExistInTheDatabase(String ingred1, String ingred2) {
    Assert.assertTrue(ingredientAccessor.ingredientExists(ingred1));
    Assert.assertTrue(ingredientAccessor.ingredientExists(ingred2));
  }

  @And("The {string} recipe has preparation step {string} with ingredients {string} and {string}")
  public void theRecipeHasPreparationStepWithIngredientsAnd(String recipeName, String prepStep, String ingred1, String ingred2) {
    Recipe recipeObject = service.getRecipe(recipeName);
    try (Session session = factory.openSession()) {
      session.update(recipeObject);
      Assert.assertEquals(recipeObject.getName(), recipeName);
      Assert.assertEquals(recipeObject.getPreparationSteps().get(0).getDescription(), prepStep);
      Assert.assertEquals(recipeObject.getPreparationSteps().get(0).getIngredients(), ingredientSet);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  @When("The user inserts an invalid recipe {string}")
  public void theUserInsertsAnInvalidRecipe(String name) {
    Assert.assertEquals(service.checkRecipe(name), -1);
  }

  @Then("The recipe {string} is not saved to the database")
  public void theRecipeIsNotSavedToTheDatabase(String name) {
    Assert.assertEquals(service.checkRecipe(name), -1);
  }

  @Given("The recipe {string} already exists in the database")
  public void theRecipeAlreadyExistsInTheDatabase(String name) {
    testRecipe.setName(name);
    accessor.save(testRecipe);
    Assert.assertTrue(accessor.recipeExists(name));
  }

  @When("the user adds the recipe {string} to the database")
  public void theUserAddsTheRecipeToTheDatabase(String name) {
    testRecipe.setName(name);
    Assert.assertNotEquals(accessor.save(testRecipe), -1);


  }
}
