def line_edits(s1, s2):
    """edit distance algorithm"""
    l1 = [''] + s1.splitlines()
    l2 = [''] + s2.splitlines()
    n_rows = len(l1)
    n_cols = len(l2)
    solutions = [[None] * n_cols for _ in range(n_rows)]
    for rows in range(n_rows):
        for cols in range(n_cols):
            if rows == 0 and cols == 0:
                solutions[rows][cols] = 0
            elif rows == 0:
                solutions[rows][cols] = cols
            elif cols == 0:
                solutions[rows][cols] = rows
            elif l1[rows] == l2[cols]:
                solutions[rows][cols] = solutions[rows-1][cols-1]
            else:
                solutions[rows][cols] = min(solutions[rows-1][cols],\
                solutions[rows][cols-1], solutions[rows-1][cols-1]) + 1
    results = []
    n_rows += -1
    n_cols += -1
    while n_rows >= 0:
        deletion = solutions[n_rows-1][n_cols]
        insertion = solutions[n_rows][n_cols-1]
        sub = solutions[n_rows-1][n_cols-1]
        minn = min(deletion, insertion, sub) 
        if l1[n_rows] == l2[n_cols]:
            tup = ('T', l1[n_rows], l2[n_cols])
            n_rows += -1
            n_cols += -1
            results.append(tup)
        elif deletion == minn:
            tup = ('D', l1[n_rows], '')
            n_rows += -1
            results.append(tup)

        elif insertion == minn:
            tup = ('I', '', l2[n_cols])
            n_cols += -1
            results.append(tup)
            
        else:
            lcs = longest_common_substring(l1[n_rows], l2[n_cols])
            line1 = helper(l1[n_rows], lcs)
            line2 = helper(l2[n_cols], lcs)
            tup = ('S', line1, line2)
            n_cols += -1
            n_rows += -1
            results.append(tup)
    return reversed(results[:-1])

def helper(string, lcs):
        index = []
        for c in range(len(string)):
            if len(lcs) != 0 and lcs[0] == string[c]:
                lcs = lcs[1 :]
            else:
                index.append(c)
        count = 0
        for iii in index:
            location = iii + count
            string = string[: location] + "[[" + string[location] + "]]" + string[location+1 :]
            count += 4
        return string 

def longest_common_substring(s1, s2):
    """checks for longest common substring"""
    cache = {}
    def helper(s1, s2):
        """recusion function for checking largest substring"""
        if len(s1) == 0 or len(s2) == 0:
            return ''
        elif (s1, s2) in cache:
            return cache[(s1, s2)]

        elif s1[-1] == s2[-1]:
            cache[(s1, s2)] = helper(s1[: -1], s2[: -1]) + s1[-1]

            return cache[(s1, s2)]
        elif (s1, s2[: -1]) in cache and (s1[: -1], s2) in cache:
            return max(cache[(s1, s2[: -1])], cache[(s1[: -1], s2)], key=len)
        else:
            cache[(s1, s2[: -1])] = helper(s1, s2[: -1])
            cache[(s1[: -1], s2)] = helper(s1[: -1], s2)
            return max(cache[(s1[: -1], s2)], cache[(s1, s2[: -1])], key=len)
    return helper(s1, s2)
s1 = "Line1\nLine 2a\nLine3\nLine4\n"
s2 = "Line5\nline2\nLine3\n"
table = line_edits(s1, s2)
for row in table:
    print(row)
