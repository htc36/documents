def line_edits(s1, s2):
    """line edits"""
    l1, l2 = [''] + s1.splitlines(), [''] + s2.splitlines()
    s = [[None] * len(l2) for _ in range(len(l1))]
    for rows in range(len(l1)):
        for cols in range(len(l2)):
            if rows == 0 and cols == 0:
                s[rows][cols] = 0
            elif rows == 0:
                s[rows][cols] = cols
            elif cols == 0:
                s[rows][cols] = rows
            elif l1[rows] == l2[cols]:
                s[rows][cols] = s[rows-1][cols-1]
            else:
                s[rows][cols] = min(s[rows-1][cols], s[rows][cols-1], s[rows-1][cols-1]) + 1
    results = []
    n_rows, n_cols = len(l1) -1, len(l2) -1
    while n_rows >= 0:
        minn = min(s[n_rows-1][n_cols], s[n_rows][n_cols-1], s[n_rows-1][n_cols-1]) 
        if l1[n_rows] == l2[n_cols]:
            tup = ('T', l1[n_rows], l2[n_cols])
        elif s[n_rows-1][n_cols] == minn:
            tup = ('D', l1[n_rows], '')
            n_cols += 1
            

        elif s[n_rows][n_cols-1] == minn:
            tup = ('I', '', l2[n_cols])
            n_rows += 1
            
        else:
            tup = ('S', l1[n_rows], l2[n_cols])
        n_rows += -1
        n_cols += -1
        
        results.append(tup)
    return reversed(results[:-1])

    
s1 = "Line1\nLine2\nLine3\nLine4\n"
s2 = "Line5\nLine4\nLine3\n"
table = line_edits(s1, s2)
for row in table:
    print(row)



s1 = "Line1\nLine2\nLine3\nLine4\n"
s2 = "Line1\nLine3\nLine4\nLine5\n"
table = line_edits(s1, s2)
for row in table:
    print(row)
#s2 = "A\nT\nC\nT\nC\nC\nG\n"
#s1 = "G\nA\nC\nT\nG\nC\n"
#line_edits(s1, s2)
