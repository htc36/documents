def longest_common_substring(s1, s2):
    """gets longest substring without for loops"""
    n_rows = len(s1) + 1
    n_cols = len(s2) + 1
    solutions = [[None] * n_cols for _ in range(n_rows)]
    for row in range(n_rows):
        for coloum in range(n_cols):
            if row == 0 or coloum == 0:
                solutions[row][coloum] = 0
            elif s1[row -1] == s2[coloum -1]:
                solutions[row][coloum] = solutions[row-1][coloum-1] + 1
            else:
                solutions[row][coloum] = max(solutions[row][coloum-1], solutions[row-1][coloum])
    n_rows += -1
    n_cols += -1
    string = ''
    
    while n_rows > 0 and n_cols > 0:
        if s1[n_rows-1] == s2[n_cols-1]:
            string = s1[n_rows-1] + string
            n_rows += -1
            n_cols += -1
        else:
            if solutions[n_rows-1][n_cols] > solutions[n_rows][n_cols-1]:
                n_rows += -1
            else:
                n_cols += -1
    return(string)        



s1 = 1500 * 'x'
s2 = 1500 * 'y'
print(longest_common_substring(s1, s2))
s1 = "Look at me, I can fly!"
s2 = "Look at that, it's a fly"
print(longest_common_substring(s1, s2))
