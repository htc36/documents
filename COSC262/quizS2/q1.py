def longest_common_substring(s1, s2):
    """checks for longest common substring"""
    cache = {}
    def helper(s1, s2):
        """recusion function for checking largest substring"""
        if len(s1) == 0 or len(s2) == 0:
            return ''
        elif (s1, s2) in cache:
            return cache[(s1, s2)]

        elif s1[-1] == s2[-1]:
            cache[(s1, s2)] = helper(s1[: -1], s2[: -1]) + s1[-1] 

            return cache[(s1, s2)]
        elif (s1, s2[: -1]) in cache and (s1[: -1], s2) in cache: 
            return max(cache[(s1, s2[: -1])], cache[(s1[: -1], s2)], key=len)
        else:
            cache[(s1, s2[: -1])] = helper(s1, s2[: -1])
            cache[(s1[: -1], s2)] = helper(s1[: -1], s2)
            return max(cache[(s1[: -1], s2)], cache[(s1, s2[: -1])], key=len)
    return helper(s1, s2)
s1 = "Look at me, I can fly!"
s2 = "Look at that, it's a fly"
#s1 = "abcdefghijklmnopqrstuvwxyz"
#s2 = "ABCDEFGHIJKLMNOPQRSTUVWXYS"
#s1 = "balderdash!"
#s2 = "balderdash!"
print(longest_common_substring(s1, s2))



