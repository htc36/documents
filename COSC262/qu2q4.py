def key_positions(seq,key):
    k = max(key(a) for a in seq)
    
    c = [iii for iii in range(k+1)]
    
    for iii in range(0,k):
        c[iii] = 0
    print(c)
    for a in seq:
        c[key(a)] = c[key(a)] + 1
    print(c)
    summ = 0
    
    for iii in range(k+1):
        c[iii], summ = summ, summ + c[iii]
    print(c)
    return c

print(key_positions([2, 1, 0], lambda x: x))

def sorted_array(seq, key, positions):

    b = [iii for iii in range(len(seq))]
    p = key_positions(seq, key)
    for a in seq:
        b[p[key(a)]] = a
        p[key(a)] = p[key(a)] + 1
    return b


