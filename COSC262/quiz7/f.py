class Item:
    """An item to (maybe) put in a knapsack"""
    def __init__(self, value, weight):
        self.value = value
        self.weight = weight
        
def max_value(items, capacity):
    """The maximum value achievable with a given list of items and a given
       knapsack capacity."""
    n_rows = len(items)  +1
    n_coloums = capacity + 1
    solutions = [[None] * n_coloums  for _ in range(n_rows)]
    
    
    for rows in range(n_rows):
        usedIndex = [None] * n_rows 
        for coloums in range(n_coloums):
            if rows == 0 or coloums == 0:
                solutions[rows][coloums] = 0
            else:
                item = items[rows -1]
                size = -1
                if coloums - item.weight >= 0:
                    if usedIndex[rows-1] != None:
                        if usedIndex[rows-1][0] == coloums - item.weight:
                            size = solutions[rows][coloums-1] 
                        else:
                            size = item.value + solutions[rows][coloums - item.weight]
                    else:

                        size = solutions[rows][coloums - item.weight] + item.value
                    
                if size > solutions[rows-1][coloums]:
                    tup = (coloums, rows-1)
                    usedIndex[rows-1] = tup 
                    solutions[rows][coloums] = size
                else:
                    solutions[rows][coloums] = solutions[rows-1][coloums]
                        
                

                solutions[rows][coloums] = max(solutions[rows-1][coloums], size)
            
    print(solutions)
            
            
            
    
    
    
    
       
       



# The example in the lecture notes
items = [Item(45, 3),
         Item(45, 3),
         Item(80, 4),
         Item(80, 5),
         Item(100, 8)]
print(max_value(items, 10)) 
