from math import inf as INFINITY

def read_grid(filename):
    """Read from the given file an n x m grid of integer weights.
       The file must consist of n lines of m space-separated integers.
       n and m are inferred from the file contents.
       Returns the grid as an n element list of m element lists.
       THIS FUNCTION DOES NOT HAVE BUGS.
    """
    with open(filename) as infile:
        lines = infile.read().splitlines()

    grid = [[int(bit) for bit in line.split()] for line in lines]
    return grid

def grid_cost(grid):
    base = True
    n_rows = len(grid)
    n_cols = len(grid[0])
    solutions = [[None] * n_cols] * n_rows
    for row in range(n_rows):
        for coloum in range(n_cols):
            if row == 0:
                solutions[row][coloum] = grid[row][coloum]
            elif coloum == 0:
                print(row)
                print(coloum)
                solutions[row][coloum] = grid[row][coloum] + (min(grid[row - 1][int(coloum) +int(delta_col)]) for delta_col in range(0, 2))
            elif coloum == n_cols - 1:
                solutions[row][coloum] = grid[row][coloum] + (min(grid[row - 1][coloum + delta_col]) for delta_col in range(-1, 1))
            else:
                solutions[row][coloum] = grid[row][coloum] + (min(grid[row - 1][coloum + delta_col]) for delta_col in range(-1, 2))

    print(solutions)
                

                
                

           




        
    


def file_cost(filename):
    """The cheapest cost from row 1 to row n (1-origin) in the grid of integer
       weights read from the given file
    """
    return grid_cost(read_grid(filename))

print(file_cost('checkerboard.small.in.txt'))
