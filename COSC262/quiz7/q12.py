class Item:
    """An item to (maybe) put in a knapsack"""
    def __init__(self, value, weight):
        self.value = value
        self.weight = weight
        
def max_value(items, capacity):
    """The maximum value achievable with a given list of items and a given
       knapsack capacity."""
    n_rows = len(items) + 1
    n_cols = capacity + 1 
    solutions = [[None] * n_cols for _ in range(n_rows)]
    for row in range(n_rows):
        for coloum in range(n_cols):
            item = items[row -1]
            if row == 0 or coloum == 0:
                solutions[row][coloum] = 0
            elif item.weight <= coloum:
                solutions[row][coloum] = max(solutions[row-1][coloum], item.value + solutions[row-1][coloum-item.weight])
            else:
                solutions[row][coloum] = solutions[row-1][coloum]
    return solutions[n_rows-1][n_cols-1]

# The example in the lecture notes
items = [Item(45, 3),
         Item(45, 3),
         Item(80, 4),
         Item(80, 5),
         Item(100, 8)]
print(max_value(items, 10)) 
   
