def bfs_tree(adj_list, start):
    n = len(adj_list) # n will be size of the adj
    state = ["U"] * n # initialize
    parent = [None] * n
    q = []
    state[start] = "D"
    q.append(start)
    return bfs_loop(adj_list, q, state, parent)

def bfs_loop(adj_list, q, state, parent):
    while len(q) != 0:
        uuu = q.pop(0)
        for iii in adj_list[uuu]:
            numm = iii[0]
            if state[numm] == "U":
                state[numm] = "D"
                parent[numm] = uuu
                q.append(numm)
            state[uuu] = "P"
    
    return parent
                
                
# an undirected graph

adj_list = [
    [(1, None)],
    [(0, None), (2, None)],
    [(1, None)]
]

print(bfs_tree(adj_list, 0))
print(bfs_tree(adj_list, 1))
        
            
