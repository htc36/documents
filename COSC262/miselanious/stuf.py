def adjacency_list(graph_str):

    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])

    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()


            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))

            overall[int(new[0])].append(tup)

    else:

        for iii in range(0, num_of_vert):

            for jjj in raw[1 : -1]:
                new = jjj.split()

                if iii == int(new[0]):

                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)

                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))
                    overall[int(new[1])].append(tup)


    return overall

def bfs_tree(adj_list, start):
    n = len(adj_list) # n will be size of the adj
    state = ["U"] * n # initialize
    parent = [None] * n
    q = []
    state[start] = "D"
    q.append(start)
    return bfs_loop(adj_list, q, state, parent)

def bfs_loop(adj_list, q, state, parent):
    while len(q) != 0:
        uuu = q.pop(0)
        for iii in adj_list[uuu]:
            numm = iii[0]
            if state[numm] == "U":
                state[numm] = "D"
                parent[numm] = uuu
                q.append(numm)
            state[uuu] = "P"

    return parent

def adapter_chain(adapters_info, charger_plug, wall_socket):
    adj_list = adjacency_list(adapters_info)
    parent_bfs = bfs_tree(adj_list, charger_plug)
    shortest_path = shortest_path_finder(parent_bfs, charger_plug, wall_socket)
    return shortest_path



def shortest_path_finder(parent_bfs, charger_plug, wall_socket):
    if charger_plug == wall_socket:

        return [charger_plug]
    else:
        if parent_bfs[wall_socket] == None:
            return "CS Unplugged!"
        else:
            return shortest_path_finder (parent_bfs, charger_plug, parent_bfs[wall_socket]) + [wall_socket]
