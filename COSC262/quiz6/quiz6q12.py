class Node:
    """Represents an internal node in a Huffman tree. It has a frequency count
       and left and right subtrees, assumed to be the '0' and '1' children
       respectively.
    """
    def __init__(self, count, left, right):
        self.count = count
        self.left = left
        self.right = right

    def __str__(self, level=0):
        return ((2 * level) * ' ' + f"Node({self.count},\n" +
            self.left.__str__(level + 1) + ',\n' +
            self.right.__str__(level + 1) + ')')

    def is_leaf(self):
        return False

class Leaf:
    """A leaf node in a Huffman encoding tree. Contains a character and its
       frequency count.
    """
    def __init__(self, count, char):
        self.count = count
        self.char = char

    def __str__(self, level=0):
        return (level * 2) * ' ' + f"Leaf({self.count}, '{self.char}')"

    def is_leaf(self):
        return True

def huffman_decode(s, tree):
    searcher = tree
    string = ""
    for iii in s:
        if int(iii) == 0:
            if searcher.left.is_leaf():
                string += (searcher.left.char)
                searcher = tree
            else:
                searcher = searcher.left
        else:
            if searcher.right.is_leaf():
                string += (searcher.right.char)
                searcher = tree
            else:
                searcher = searcher.right
    return string





tree = Node(42,
  Node(17,
    Leaf(8, 'b'),
    Leaf(9, 'a')),
  Node(25,
    Node(10,
      Node(5,
        Leaf(2, 'f'),
        Leaf(3, 'd')),
      Leaf(5, 'e')),
    Leaf(15, 'c')))
print(huffman_decode('0110011100', tree))
