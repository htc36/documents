def fractional_knapsack(capacity, items):
    amount = 0
    gear = sorted(items, key = lambda x : x[1] / x[2], reverse = True)
    for i, j, k in gear:
        if capacity - k >= 0:
            amount += j
            capacity = capacity - k
        else:
            amount += (j / k) * capacity
            capacity = 0
    return(amount)



        






    # The example from the lecture notes
items = [
    ("Chocolate cookies", 20, 5),
    ("Potato chips", 15, 3),
    ("Pizza", 14, 2),
    ("Popcorn", 12, 4)]
print(fractional_knapsack(9, items))
