def all_paths(graph_string, source, destination):
    output = []
    adj_list = adjacency_list(graph_string)
    candidate = [source]
    
    helper(candidate, adj_list, source, destination, output)
    return output
    
def helper(candidate, adj_list, source, destination, output):
    if candidate.count(candidate[-1]) > 1 or len(candidate) > len(adj_list):
        return
    
    if candidate[-1] == destination:
        output.append(candidate)
        
    else:
        for vertex in adj_list[candidate[-1]]:
            thing = candidate + [vertex[0]]
            helper(thing, adj_list, source, destination, output)
    
    
                

                
            
            
        


def adjacency_list(graph_str):
    
    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])   
    
    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()
            
    
            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))
               
            overall[int(new[0])].append(tup)
        
    else:
        
        for iii in range(0, num_of_vert):
            
            for jjj in raw[1 : -1]:
                new = jjj.split()
                
                if iii == int(new[0]):
                    
                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)
                
                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))                
                    overall[int(new[1])].append(tup)
                    
        
    return overall




# triangle graph
graph_str = """\
U 3
0 1
1 2
2 0
"""

print(sorted(all_paths(graph_str, 0, 2)))
print(all_paths(graph_str, 1, 1))
    
    
#graph_str = """\
#U 5
#0 2
#1 2
#3 2
#4 2
#1 4
#"""

#print(sorted(all_paths(graph_str, 0, 1)))
    


#from pprint import pprint

## graph in fig 5.15 of textbook
## vertices 0 to 6 correspond to A to G
#graph_str = """\
#D 7
#6 0
#6 5
#0 1
#0 2
#1 2
#1 3
#2 4
#2 5
#4 3
#5 4
#"""

#pprint(sorted(all_paths(graph_str, 6, 3)))
