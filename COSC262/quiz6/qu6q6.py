def print_shows(show_list):
    stuff = sorted(show_list, key = lambda x : x[1] +  x[2])
    finish = 0
    for i, j, k in stuff:
        if finish <= j:
            finish = j + k
            print("{}, {}, {}".format(i,j,finish))   


# The example from the lecture notes
print_shows([
    ('a', 0, 6),
    ('b', 1, 3),
    ('c', 3, 2),
    ('d', 3, 5),
    ('e', 4, 3),
    ('f', 5, 4),
    ('g', 6, 4), 
    ('h', 8, 3)])


