def change_greedy(amount, coinage):
    summ = 0 
    listt = []
    for iii in sorted(coinage)[::-1]:
        count = 0
        while amount - iii >= 0:
            amount = amount - iii
            count += 1
        if count != 0:
            tup = (count, iii)
            listt.append(tup)
    if amount != 0:
        return None
    return listt

print(change_greedy(82, [1, 10, 25, 5]))

print(change_greedy(80, [1, 10, 25]))

print(change_greedy(82, [10, 25, 5]))
