import heapq
class Node:
    """Represents an internal node in a Huffman tree. It has a frequency count
       and left and right subtrees, assumed to be the '0' and '1' children
       respectively.
    """
    def __init__(self, count, left, right):
        self.count = count
        self.left = left
        self.right = right
        self.min_char =  min(self.left.min_char, self.right.min_char)

    def __str__(self, level=0):
        return ((2 * level) * ' ' + f"Node({self.count},\n" +
            self.left.__str__(level + 1) + ',\n' +
            self.right.__str__(level + 1) + ')')

    def is_leaf(self):
        return False

    def __lt__(self, item):
        return self.min_char < item.min_char

class Leaf:
    """A leaf node in a Huffman encoding tree. Contains a character and its
       frequency count.
    """
    def __init__(self, count, char):
        self.count = count
        self.char = char
        self.min_char = char

    def __str__(self, level=0):
        return (level * 2) * ' ' + f"Leaf({self.count}, '{self.char}')"

    def is_leaf(self):
        return True
    def __lt__(self, item):
        return self.min_char < item.min_char

def huffman_tree(frequencies):
    listt = []
    for character, frequency in frequencies.items():
        heapq.heappush(listt, (frequency, Leaf(frequency, character)))

    while len(listt) != 1:
        leftNum, leftLeaf = heapq.heappop(listt)
        rightNum, rightLeaf = heapq.heappop(listt)
        parentNum = leftNum + rightNum
        if leftNum == rightNum:
            if leftLeaf.min_char > rightLeaf.min_char:
                parNode = Node(parentNum, rightLeaf, leftLeaf)
            else:
                parNode = Node(parentNum, leftLeaf, rightLeaf)
        else:
            parNode = Node(parentNum, leftLeaf, rightLeaf)
        heapq.heappush(listt, (parentNum, parNode))
    num, node = listt[0]
    return node

freqs = {'a': 9,
         'b': 8,
         'c': 15,
         'd': 3,
         'e': 5,
         'f': 2}
print(huffman_tree(freqs))
