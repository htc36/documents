def adjacency_list(graph_str):
    
    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])   
    
    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()
            
    
            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))
               
            overall[int(new[0])].append(tup)
        
    else:
        
        for iii in range(0, num_of_vert):
            
            for jjj in raw[1 : -1]:
                new = jjj.split()
                
                if iii == int(new[0]):
                    
                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)
                
                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))                
                    overall[int(new[1])].append(tup)
                    
        
    return overall

def computation_order(dependencies):
    
    return adjacency_list(dependencies)

dependencies = """\
D 3
1 2
0 2
"""

print(computation_order(dependencies))