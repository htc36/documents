def adjacency_list(graph_str):
    
    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])   
    
    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()
            
    
            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))
               
            overall[int(new[0])].append(tup)
        
    else:
        
        for iii in range(0, num_of_vert):
            
            for jjj in raw[1 : -1]:
                new = jjj.split()
                
                if iii == int(new[0]):
                    
                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)
                
                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))                
                    overall[int(new[1])].append(tup)
                    
        
    return overall



def bfs_loop(adj_list, q, state_array, parent):
    while len(q) != 0:
        uuu = q.pop(0)
        for iii in adj_list[uuu]:
            numm = iii[0]
            if state_array[numm] == "Undiscovered":
                state_array[numm] = "Discovered"
                parent[numm] = uuu
                q.append(numm)
            state_array[uuu] = "Processed"
    
    return state_array

def components(adj_list):
    vertices = len(adj_list)
    state_array = ["Undiscovered"] * vertices
    que = []
    components = []
    for iii in range(vertices):
        if state_array[iii] == "Undiscovered":
            previous = state_array[:]
            state_array[iii] = "Discovered"
            que.append(iii)
            state_array = bfs_loop(adj_list, que, state_array, previous)
            new = []
            for jjj in range(0, vertices):

                if state_array[jjj] != previous[jjj]:
                    new.append(jjj)
                    
            components.append(new)

    return components
                
        
def arrangement(direct_friendship_info):
    adj_list = adjacency_list(direct_friendship_info)
    return components(adj_list)
        
        
direct_friendship_info = """\
U 2
0 1
"""

print(sorted(sorted(tables) for tables in arrangement(direct_friendship_info)))

direct_friendship_info = """\
U 2
"""

print(sorted(sorted(tables) for tables in arrangement(direct_friendship_info)))

direct_friendship_info = """\
U 7
1 2
1 5
1 6
2 3
2 5
3 4
4 5
"""

print(sorted(sorted(tables) for tables in arrangement(direct_friendship_info)))

direct_friendship_info = """\
U 0
"""

print(sorted(sorted(tables) for tables in arrangement(direct_friendship_info)))

direct_friendship_info = """\
U 1
"""

print(sorted(sorted(tables) for tables in arrangement(direct_friendship_info)))

