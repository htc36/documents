def adjacency_list(graph_str):
    
    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])   
    
    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()
            
    
            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))
               
            overall[int(new[0])].append(tup)
        
    else:
        
        for iii in range(0, num_of_vert):
            
            for jjj in raw[1 : -1]:
                new = jjj.split()
                
                if iii == int(new[0]):
                    
                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)
                
                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))                
                    overall[int(new[1])].append(tup)
                    
        
    return overall




def dfs_loop(adj_list, start, state_array, parent_array, stack):
    
    for verticy in adj_list[start]:
        if state_array[verticy[0]] == "Undiscovered":
            state_array[verticy[0]] = "Discovered"
            parent_array[verticy[0]] = start
            dfs_loop(adj_list, verticy[0], state_array, parent_array, stack)
        
        
        
        if state_array[verticy[0]] == "Discovered":
            stack = None
            
        
    if stack != None:
        state_array[start] = "Processed"
        stack.append(start)
        
    return stack
        
        





def computation_order(dependencies):
    adj_list = adjacency_list(dependencies)
    num_of_vert = len(adj_list)
    state_array = ["Undiscovered"] * num_of_vert 
    parent_array = [None] * num_of_vert
    stack = []
    
    
     
    for iii in range(num_of_vert):
        
        if state_array[iii] == "Undiscovered":
            state_array[iii] = "Discovered"
        
            stack = dfs_loop(adj_list, iii, state_array, parent_array, stack) 
            
    return stack
        
        
        
dependencies = """\
D 2
0 1
"""

print(computation_order(dependencies))


dependencies = """\
D 3
1 2
0 2
"""

print(computation_order(dependencies) in [[2, 1, 0], [2, 0, 1]])


dependencies = """\
D 3
"""
# any permutation of 0, 1, 2 is valid in this case.
solution = computation_order(dependencies)
if solution is None:
    print("Wrong answer!")
else:
    print(sorted(solution))
    
    
dependencies = """\
D 5
2 3
3 2
"""

print(computation_order(dependencies))