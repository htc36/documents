def connection_setup_delay (numberSwitches, cableLength_km, speedOfLight_kms, dataRate_bps, messageLengthRequest_b, messageLengthResponse_b, processingTimes_s):
    propigation = cableLength_km / speedOfLight_kms
    transmissionRequestDelay = messageLengthRequest_b / dataRate_bps
    requestDelay = (propigation + transmissionRequestDelay + processingTimes_s) * (numberSwitches + 1)

    transmissionResponseDelay = messageLengthResponse_b / dataRate_bps

    responseDelay = (propigation + transmissionResponseDelay + processingTimes_s) * (numberSwitches + 1)

    return (requestDelay + responseDelay)



print("{:.4f}".format(connection_setup_delay(10, 2000, 200000, 10000000, 2000, 1000, 0.001)))


    
    






