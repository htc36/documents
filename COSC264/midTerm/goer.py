def twowrongbits (pktLength_b, bitErrorProb):
    no_errors = (1-bitErrorProb)** pktLength_b
    one_error = pktLength_b * (1-bitErrorProb)**(pktLength_b - 1) * bitErrorProb
    return 1 - no_errors - one_error
print ("{:.3f}".format(twowrongbits(1000, 0.001)))

