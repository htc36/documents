import math
def min_packet_length (cableLength_km, speedOfLight_kms, dataRate_bps):
    propigation = cableLength_km / speedOfLight_kms
    value = 2*propigation
    return math.ceil(value * dataRate_bps)

print ("{:.1f}".format(min_packet_length(1,200000,1000000)))
