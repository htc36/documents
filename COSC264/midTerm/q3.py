def packeterrorprobability (pktLength_b, bitErrorProb):
    L = pktLength_b
    P = bitErrorProb
    return 1 - (1 - bitErrorProb)**pktLength_b



def twowrongbits (pktLength_b, bitErrorProb):
    L = pktLength_b
    P = bitErrorProb
    return (1 - (1 - (bitErrorProb * bitErrorProb))**pktLength_b)

print ("{:.3f}".format(twowrongbits(1000, 0.001)))

