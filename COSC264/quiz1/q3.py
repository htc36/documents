def convert(x, base):

    if not type(x) is int:
        return -1
    elif not type(base) is int:
        return -2
    elif not x >= 0:
        return -3
    elif not base >= 2:
        return -4
    else:
        result = []
        while x >= base:
            result.append(x % base)
            x = x // base
        result.append(x)
        result.reverse()
        return result
def hexstring(x):
    if not type(x) is int:
        return -1
    elif not x >= 0:
        return -2
    else:
        letters = ['A','B','C','D','E','F']
        lisst = convert(x, 16)
        string = "0x"
        for iii in lisst:
            if iii < 10:
                string += str(iii)
            else:
                string += letters[iii - 10]
        return string


print(hexstring(1234))


