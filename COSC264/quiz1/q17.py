def encodedate (day, month, year):
    if type(day) is not int or day < 1 or day > 31:
        return -1
    if type(month) is not int or month < 1 or month > 12:
        return -1
    if type(year) is not int or year < 0 or year > 2**23-1:
        return -1
    mm = month-1 << 28
    first = (mm & 0x0FFFFFFF) | mm
    dd = day-1 << 23
    almost = (first & 0xF0800000) | dd 
    final = (almost & 0xFF800000) | year
    return(final)
    
    




print(encodedate(5,5,2017))
