def queueing_delay (rate_bps, numPackets, packetLength_b):
    bits_amount = numPackets * packetLength_b
    return bits_amount / rate_bps
print ("{:.3f}".format(queueing_delay(1000000, 7, 10000)))
