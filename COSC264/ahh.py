def http_nonpersistent_delay(html_size, num_images, image_size):
    connection_setup = 0.2
    rtt = 0.1
    header = 200
    data_rate = 10000000 
    request_transmission_delay = header / data_rate
    response_transmission_delay = (header + html_size) / data_rate
    html_time = connection_setup + request_transmission_delay + response_transmission_delay + rtt

    image_responce = (header + image_size) / data_rate
    all_images_time = ((connection_setup + request_transmission_delay + image_responce + rtt)) * num_images
    total = html_time + all_images_time
    return total



#print ("{:.4f}".format(http_nonpersistent_delay(10, 10, 150)))


def http_persistent_delay(html_size, num_images, image_size):
    connection_setup = 0.2
    rtt = 0.1
    header = 200
    data_rate = 10000000 
    request_transmission_delay = header / data_rate
    response_transmission_delay = (header + html_size) / data_rate
    html_time = connection_setup + request_transmission_delay + response_transmission_delay + rtt

    image_responce = (header + image_size) / data_rate
    all_images_time = ((request_transmission_delay + image_responce + rtt)) * num_images
    total = html_time + all_images_time
    return total
print ("{:.4f}".format(http_persistent_delay(10, 10, 150)))
