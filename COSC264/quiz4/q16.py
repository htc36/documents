import math
def last_fragment_size (messageSize_bytes, overheadPerPacket_bytes, maximumNPacketSize_bytes):
    frag_size = math.ceil(messageSize_bytes/(maximumNPacketSize_bytes -overheadPerPacket_bytes)) -1
    payload = maximumNPacketSize_bytes - overheadPerPacket_bytes
    return messageSize_bytes - (frag_size * payload) + overheadPerPacket_bytes
    
    
print (last_fragment_size(10000, 20, 1500))


def number_fragments (messageSize_bytes, overheadPerPacket_bytes, maximumNPacketSize_bytes):
    return math.ceil((messageSize_bytes) / (maximumNPacketSize_bytes - overheadPerPacket_bytes))

print (number_fragments(10000, 20, 1500))




