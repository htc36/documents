def revisedcompose (hdrlen, tosdscp, identification, flags, fragmentoffset, timetolive, protocoltype, sourceaddress, destinationaddress, payload):
    totallength = (hdrlen * 4) + len(payload)
    version = 4 
    headerchecksum = 0
    if hdrlen > (2**4) - 1 or hdrlen < 0 or hdrlen < 5:
        return 2 
    if tosdscp > (2**6) - 1 or tosdscp < 0:
        return 3 
    if identification  > (2**16) - 1 or identification < 0:
        return 5 
    if flags > (2**3) - 1 or flags < 0:
        return 6 
    if fragmentoffset > (2**13) - 1 or fragmentoffset < 0:
        return 7 
    if timetolive > (2**8) - 1 or timetolive < 0:
        return 8 
    if protocoltype > (2**8) - 1 or protocoltype < 0:
        return 9 
    if headerchecksum > (2**16) - 1 or headerchecksum < 0:
        return 10 
    if sourceaddress > (2**32) - 1 or sourceaddress < 0:
        return 11 
    if destinationaddress > (2**32) - 1 or destinationaddress < 0:
        return 12 
    splitter = (flags << 13) + fragmentoffset
    pkt = bytearray([(version << 4)+ hdrlen, 
                    tosdscp << 2 + 0,
                    totallength >> 8,
                    totallength & 0x00FF,
                    identification >> 8,
                    identification & 0x00FF,
                    splitter >> 8,
                    splitter & 0x00FF,
                    timetolive,
                    protocoltype,
                    headerchecksum >> 8,
                    headerchecksum & 0x00FF,
                    sourceaddress >> 24,
                    (sourceaddress & 0x00FF0000) >> 16,
                    (sourceaddress & 0x0000FF00) >> 8,
                    sourceaddress & 0x000000FF,
                    destinationaddress >> 24,
                    (destinationaddress & 0x00FF0000) >> 16,
                    (destinationaddress & 0x0000FF00) >> 8,
                    destinationaddress & 0x000000FF ])
    n = 0
    if hdrlen != 5:
        for iii in (range(hdrlen - 5)):
            pkt.append(0)
            pkt.append(0)
            pkt.append(0)
            pkt.append(0)
    
    for iii in range(0, len(pkt), 2):
        try:
            n += ((pkt[iii] << 8) + pkt[iii+1] )
        except IndexError as e:
            break
    x = n 
    

    while x > 0xFFFF:
        x0 = x & 0xFFFF
        x1 = x >> 16
        x = x0 + x1
    x = ~x


    pkt[10] = (x & 0xFF00) >> 8 
    pkt[11] = x & 0x00FF
    hi = pkt + payload
    return hi


print(revisedcompose (6, 24, 4711, 0, 22, 64, 0x06, 0x22334455, 0x66778899, bytearray([0x10, 0x11, 0x12, 0x13, 0x14, 0x15])))



