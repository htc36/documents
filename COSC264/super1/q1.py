def composepacket (version, hdrlen, tosdscp, totallength, identification, flags, fragmentoffset, timetolive, protocoltype, headerchecksum, sourceaddress, destinationaddress):
    if version != 4 or version < 0:
        return 1 
    if hdrlen > (2**4) - 1 or hdrlen < 0:
        return 2 
    if tosdscp > (2**6) - 1 or tosdscp < 0:
        return 3 
    if totallength > (2**16) - 1 or totallength < 0:
        return 4 
    if identification  > (2**16) - 1 or identification < 0:
        return 5 
    if flags > (2**3) - 1 or flags < 0:
        return 6 
    if fragmentoffset > (2**13) - 1 or fragmentoffset < 0:
        return 7 
    if timetolive > (2**8) - 1 or timetolive < 0:
        return 8 
    if protocoltype > (2**8) - 1 or protocoltype < 0:
        return 9 
    if headerchecksum > (2**16) - 1 or headerchecksum < 0:
        return 10 
    if sourceaddress > (2**32) - 1 or sourceaddress < 0:
        return 11 
    if destinationaddress > (2**32) - 1 or destinationaddress < 0:
        return 12 
    splitter = (flags << 13) + fragmentoffset
    yay = bytearray([(version << 4)+ hdrlen, 
                    tosdscp << 2 + 0,
                    totallength >> 8,
                    totallength & 0x00FF,
                    identification >> 8,
                    identification & 0x00FF,
                    splitter >> 8,
                    splitter & 0x00FF,
                    timetolive,
                    protocoltype,
                    headerchecksum >> 8,
                    headerchecksum & 0x00FF,
                    sourceaddress >> 24,
                    (sourceaddress & 0x00FF0000) >> 16,
                    (sourceaddress & 0x0000FF00) >> 8,
                    sourceaddress & 0x000000FF,
                    destinationaddress >> 24,
                    (destinationaddress & 0x00FF0000) >> 16,
                    (destinationaddress & 0x0000FF00) >> 8,
                    destinationaddress & 0x000000FF ])
    return yay

print(composepacket(5,5,0,4000,24200,0,63,22,6,4711, 2190815565, 3232270145))
    
print(composepacket(4,5,0,1500,24200,0,63,22,6,4711, 2190815565, 3232270145))







    
    
    
    

    
