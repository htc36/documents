def destaddress(pkt):
    address = (pkt[-4] << 24) + (pkt[-3] << 16) + (pkt[-2] << 8) + (pkt[-1])
    a = (address & 0xFF000000) >> 24 
    b = (address & 0x00FF0000) >> 16 
    c = (address & 0x0000FF00) >> 8
    d = address & 0x000000FF
    ip = "{}.{}.{}.{}".format(a,b,c,d)
    return address, ip




print(destaddress(bytearray(b'E\x00\x00\x1e\x04\xd2\x00\x00@\x06\x00\x00\x00\x124V3DUf')))


