def total_transfer_time (linkLength_km, speedOfLight_kms, processingDelay_s, dataRate_bps, maxUserDataBitsPerPacket_b, overheadBitsPerPacket_b, messageLength_b):
    size = maxUserDataBitsPerPacket_b + overheadBitsPerPacket_b

    propigation = linkLength_km / speedOfLight_kms
    transmition = size / dataRate_bps
    packetTime = (2*transmition) + (2*processingDelay_s)
    return packetTime * (messageLength_b / maxUserDataBitsPerPacket_b) 
print ("{:.4f}".format(total_transfer_time(20000, 200000, 0.001, 1000000, 1000, 100, 5000)))



