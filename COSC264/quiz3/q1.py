def connection_setup_delay (cableLength_km, speedOfLight_kms, dataRate_bps, messageLength_b, processingTimes_s):
    propogation_ab = cableLength_km / speedOfLight_kms
    transmition_b = messageLength_b / dataRate_bps
    result = (propogation_ab + transmition_b  + processingTimes_s) *4
    return result


print ("{:.4f}".format(connection_setup_delay(10000, 200000, (1/(10**-6)), 4000, 0.001)))

    
