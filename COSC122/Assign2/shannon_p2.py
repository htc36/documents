def adjacency_list(graph_str):
    
    raw = (graph_str.split("\n"))
    splitt = raw[0].split()
    num_of_vert = (int(splitt[1]))
    overall = []
    for iii in range(0, num_of_vert):
        overall.append([])   
    
    if (raw[0][0]) == "D":
        for iii in raw[1 : -1]:
            new = iii.split()
            
    
            if len(new) == 2:
                tup = (int(new[1])), None
            else:
                tup = (int(new[1])), (int(new[2]))
               
            overall[int(new[0])].append(tup)
        
    else:
        
        for iii in range(0, num_of_vert):
            
            for jjj in raw[1 : -1]:
                new = jjj.split()
                
                if iii == int(new[0]):
                    
                    if len(new) == 2:
                        tup = (int(new[1])), None
                    else:
                        tup = (int(new[1])), (int(new[2]))
                    overall[int(new[0])].append(tup)
                
                elif iii == int(new[1]):
                    if len(new) == 2:
                        tup = (int(new[0])), None
                    else:
                        tup = (int(new[0])), (int(new[2]))                
                    overall[int(new[1])].append(tup)
                    
        
    return overall

def next_vertex(in_tree, distance):
    minn = float('inf')
    
    index = None

    for iii in range(0, len(in_tree)):
        if in_tree[iii] == False and distance[iii] < minn:
            minn = distance[iii]
            index = iii
            
     
        
    return index


def dikestra(adj_list, start):
    vertices = len(adj_list)
    in_tree = [False] * vertices
    distance = [float('inf')] * vertices
    parent = [None] * vertices
    distance[start] = 0
    while not all(in_tree):
        u = next_vertex(in_tree, distance)
        
        if u == None:
            
            for iii in range(len(in_tree)):
                if in_tree[iii] == False:
                    index = iii 
            in_tree[index] = True
           
                   
        
        else:
            in_tree[u] = True
            for vert, weight in adj_list[u]:
                if in_tree[vert] == False and (distance[u] + weight) < distance[vert]:
                    distance[vert] = distance[u] + weight
                    parent[vert] = u
        
      
            
    return(distance)
        
    
    
def maximum_energy(city_map, depot_position):
    adj_list = adjacency_list(city_map)
    distance = dikestra(adj_list, depot_position)
    maxx = 0
    for iii in distance:
        if iii != float('inf') and iii > maxx:
            maxx = iii
    return maxx * 2