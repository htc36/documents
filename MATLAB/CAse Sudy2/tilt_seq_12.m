function tilts = tilt_seq_12(m, r, width)
% TILT_SEQ_1:  function to return a 'tilt sequence', a matrix
%  representing a sequence of stepped tilts to generate a square 
%  path with rounded corners.  The tilt is a vector quantity,
%  positive directly 'up the hill', see Notes, Sec 2.3.
%
% Sequence #1:  Rounded square with ball starting from rest at
%               (-w/2,-w/2) and moving off to the right; stops
%               after one lap at (w/2,-w/2).
%
% usage:        tilts = tilt_seq_1(m, r, width);
%
% inputs:  m      mass of the ball in kg
%          r      radius of the ball in m
%          width  target width (= height) for the rounded square
%                 trajectory for the ball.
% output:  tilts  Nevents x 3 matrix; col 1 = times in seconds,
%                 with first = 0; cols 2,3 = tilt (x,y components) after
%                 that time, with tilt = 0,0 before time 0.
%
% P.J. Bones     UCECE
% Last modified:  23.9.2018 

% Parameters for the tilt sequence
g = 9.81;       % gravitational acceleration m/s/s
rps = 0.75;        % rolls per second
t_c = 1;        % s, time to complete each corner bend

Nevents = 12;   % accel + 4 corners + decel (start and end for each)

% Derived quantities (see Notes, Sec 2.3)
v_t = rps * 2 * pi * r;    % target speed once acceleration completed, m.s^-1
s_f = width / 2;           % final distance for acceleration (decel), m
t_f = 2 * s_f / v_t;       % time for linear acceleration (decel), s
tilt_acc = tan( asin(7 * v_t^2 / 10 / g / s_f));  % magnitude, accel, in m.m^-1
R_c = 2 * v_t * t_c / pi;  % radius of the corner, m
tilt_c = 14 * sqrt(2) * v_t^2 / 5 / g / pi / R_c;
                 % magnitude of the fixed tilt for each corner
t_side = (width - 2 * R_c) / v_t;  % time to roll straight on each side, s

% Form the sequence
tilts = zeros(Nevents, 3);
index = 1;
% Accel
tilts(index,1) = 0; tilts(index,2) = -tilt_acc; tilts(index,3) = 0; 
index = index + 1;
tilts(index,1) = t_f; tilts(index,2) = 0; tilts(index,3) = 0; 
index = index + 1;
% Corner 1
tilts(index,1) = tilts(index-1,1) + t_side/2;
tilts(index,2) = tilt_c / sqrt(2);
tilts(index,3) = -tilt_c / sqrt(2); 
index = index + 1;
tilts(index,1) = tilts(index-1,1) + t_c; tilts(index,2) = 0;
tilts(index,3) = 0; 
index = index + 1;
% Corner 2
tilts(index,1) = tilts(index-1,1) + t_side; tilts(index,2) = tilt_c / sqrt(2);
tilts(index,3) = tilt_c / sqrt(2); 
index = index + 1;
tilts(index,1) = tilts(index-1,1) + t_c; tilts(index,2) = 0; tilts(index,3) = 0; 
index = index + 1;
% Corner 3
tilts(index,1) = tilts(index-1,1) + t_side; tilts(index,2) = -tilt_c / sqrt(2);
tilts(index,3) = tilt_c / sqrt(2); 
index = index + 1;
tilts(index,1) = tilts(index-1,1) + t_c; tilts(index,2) = 0; tilts(index,3) = 0; 
index = index + 1;
% Corner 4
tilts(index,1) = tilts(index-1,1) + t_side; tilts(index,2) = -tilt_c / sqrt(2);
tilts(index,3) = -tilt_c / sqrt(2); 
index = index + 1;
tilts(index,1) = tilts(index-1,1) + t_c; tilts(index,2) = 0; tilts(index,3) = 0; 
index = index + 1;
% Accel
tilts(index,1) = tilts(index-1,1) + t_side/2; tilts(index,2) = tilt_acc; tilts(index,3) = 0; 
index = index + 1;
tilts(index,1) = tilts(index-1,1) + t_f; tilts(index,2) = 0; tilts(index,3) = 0; 

% The tilts matrix is now fully populated

