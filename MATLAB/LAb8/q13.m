%EMTH171
%Trapezium Rule

clear 
clc

a = 2;
b = 5;
f = @(x) exp(2*x);
integral = 10985.93382238679;
eArray = [];
nArray = [10 : 1 : 200];

for iii = 1 : length(nArray)
    dx = (b-a) / nArray(iii);
    xArray = [a : dx : b];
    yArray = f(xArray);
    new = sum(yArray(2 : end-1)) ;
    xPart = xArray(2) - xArray(1);
    answer = dx * ( (0.5*(yArray(1) + yArray(end))) + new);
    error = abs(integral - answer);
    eArray = [eArray, error];
end
eArray;
plot(nArray, eArray)