function [answer] = trapRule(f, a, b, dx)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

xArray = [a : dx : b];
yArray = f(xArray);

new = sum(yArray(2 : end-1)) ;

xPart = xArray(2) - xArray(1);

answer = dx * ( (0.5*(yArray(1) + yArray(end))) + new);

end

