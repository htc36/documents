%EMTH171
%Trapezium Rule

clear 
clc

a = -6;
b = 4;
dx = 0.01;
f = @(x) 22 - 3*x.^2;


xArray = [a : dx : b];
yArray = f(xArray);

new = sum(yArray(2 : end)) ;


answer = dx *  + new

g = @(x) x.^3;
xMin = -2;
xMax = 1;
strip = 1e-2;
[ myInt] = rightHandRule(g, xMin, xMax, strip);
fprintf('Int = %.4f\n', myInt);