%EMTH171
%comparing LHR and RHR

clear 
clc

a = 2;
b = 5;
dx = 0.01;
f = @(x) exp(2*x);
actual = 10985.93382238679;

nArray = [10 : 1 : 200];
lhrArray = [];
rhrArray = [];
trapArray = [];

for iii = 1 : length(nArray)
    dx = (b-a) / nArray(iii);
    rhr = abs(rightHandRule(f, a, b, dx) - actual);
    lhr = abs(leftHandRule(f, a, b, dx) - actual);
    trap = abs(trapRule(f, a, b, dx) - actual);
    rhrArray = [rhrArray, rhr];
    lhrArray = [lhrArray, lhr];
    trapArray = [trapArray, trap];
    
end
hold on
plot(nArray,rhrArray)
plot(nArray,lhrArray)
plot(nArray, trapArray)