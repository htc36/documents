%EMTH171
%Tricky one rod length varible density, finding out estimated mass
clear 
clc

mass = 0;
filearray = load('VariableDensityRodData9.csv');
disArray = filearray(:, 1);
denArray = filearray(:, 2) .* 1000;
areaArray = filearray(:, 3) ./ 10000;
xArray = [];
dxArray = [];

f = @(x) denArray(x) * areaArray(x);
inital_mass = f(1);

for iii = 2 : length(disArray)
    dxArray(iii) = disArray(iii) - disArray(iii-1);
    xArray(iii + 1) = f(iii);
end
for jjj = 2 : length(xArray) - 1
    dx = dxArray(jjj);
    result = dx * ((xArray(jjj + 1) + xArray(jjj)) / 2);
    mass = mass + result;
end
overall_mass = mass + inital_mass