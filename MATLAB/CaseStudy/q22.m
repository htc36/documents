%EMTH171
%task one

clear 
clc


K = 1.09;
time = 10/60;
%timeStep = 0.25;
se = 0.7;
suctionHead = 11.01;
delt_angle = 0.247;
product = suctionHead * delt_angle;
initial_guess = 1;
xArray = [];
all = [];
f_t = 1;
f_t_1 = 1;
counter = 1;
f_t_inital = 0;
rfall_inc = [0.18, 0.21, 0.26, 0.32, 0.37, 0.43, 0.64, 1.14, 3.18, 1.65, 0.81, 0.52, 0.42, 0.36, 0.28, 0.24, 0.19, 0.17];
rfall_cume = [0.18];

%finding intensity
for ii = 2 : length(rfall_inc)
    rfall_cume(ii) = rfall_cume(ii-1) + rfall_inc(ii);
end

rfall_intensity = rfall_inc ./ time
%finding F
culm_infil = [0];

for iii = 2 : length(rfall_inc)
   culm_infil(iii) = culm_infil(iii - 1) + rfall_intensity(iii-1) * (time);
end
culm_infil; %F
infil_rate = [];

%finding f
for iii = 2 : length(rfall_inc)
    %if K*(((product)/culm_infil(iii)) + 1) >= rfall_intensity(iii)
    if 1 < 2
        infil_rate(iii) = K*(((product)/culm_infil(iii)) + 1);
    else

        for ii = 1 : 4
            f = @(f_t) f_t - f_t_1 - (product * log((product + f_t) / (product + f_t_1))) - K * (time*iii);
            d = @(f_t) 1 - (product/(product + f_t));
        
            f_t = f_t - ((f(f_t)) / (d(f_t)));
            d(f_t);

            xArray(ii) = f_t;
        end
        infil_rate(iii) = xArray(end);
        
        all(counter) = xArray(end);    
    end
        
    
end
infil_rate



%for timeStep = 10/60 : 10/60 : 180/60
    


  

   % x = 1;
    

%     for ii = 1 : 4
% 
%         f_t = f_t - ((f(f_t)) / (d(f_t)));
%         d(f_t);
% 
%         xArray(ii) = f_t;
%     end
%         
%     all(counter) = xArray(end);    
%     %if rfall_cume[counter] > all[counter]:
%         
%     counter = counter + 1;
% 
%     
% end
% all;
