%EMTH171
%task one

clear 
clc


K = 1.09;
timeStep = 0.25;
se = 0.7;
suctionHead = 11.01;
effective_porosity = 0.412
delt_angle = (1 - se) * effective_porosity
product = suctionHead * delt_angle;
initial_guess = 1
xArray = [];
f_t = 1
f_t_1 = 1
f = @(f_t) f_t - f_t_1 - (product * log((product + f_t) / (product + f_t_1))) - K * timeStep;
d = @(f_t) 1 - (product/(product + f_t));
x = 1;

for ii = 1 : 4
    
 
    f_t = f_t - ((f(f_t)) / (d(f_t)));
    
    xArray(ii) = f_t;

end
xArray

