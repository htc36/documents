%EMTH171
%task one

clear 
clc


K = 1.09;
time = 10/60;
%timeStep = 0.25;
se = 0.7;
suctionHead = 11.01;
delt_angle = 0.247;
product = suctionHead * delt_angle;
initial_guess = 1;
F_t = 1;
F_t_1 = 1;
f_t_inital = 0;
rfall_inc = [0.18, 0.21, 0.26, 0.32, 0.37, 0.43, 0.64, 1.14, 3.18, 1.65, 0.81, 0.52, 0.42, 0.36, 0.28, 0.24, 0.19, 0.17];
rfall_cume = rfall_inc(1);
i_rate = inf;
f = @(F_t, F_t_1) F_t - F_t_1 - (product * log((product + F_t) / (product + F_t_1))) - K * time;
d = @(F_t) 1 - (product/(product + F_t));

%finds cumulaitive rainfall using incremental
for ii = 2 : length(rfall_inc)
    rfall_cume(ii) = rfall_cume(ii-1) + rfall_inc(ii);
end

%rainfall intensity:
rfall_intensity = rfall_inc ./ time;

%calculates infiltration (F_t) and infiltration rate (f_t)

previous_inf = 0;
for iii = 1 : length(rfall_inc)
    i_rate;
    iii;
    if i_rate(iii) > rfall_intensity(iii)
        infiltration(iii) = previous_inf + rfall_intensity(iii)*time;
        previous_inf = infiltration(iii);
        i_rate(iii+1) = K*((product/infiltration(iii)) + 1);
    else
        F_t_1 = infiltration(iii-1);
        for ii = 1 : 4
 
            F_t = F_t - ((f(F_t, F_t_1)) / (d(F_t)));

        end
        infiltration(iii) = F_t;
        previous_inf = infiltration(iii);
        F_t;
        i_rate(iii+1) = K*((product/infiltration(iii)) + 1);
        run_off(iii) = rfall_cume(iii) - infiltration(iii);
        run_off_inc(iii) = run_off(iii) - run_off(iii - 1);
    end
    
end
rfall_intensity;
i_rate;
infiltration
run_off;
run_off_inc;






 time = [ 0 : 10 : 180]


x = time;
y = i_rate;
yyaxis left

plot(x,y,'-s','MarkerSize',7, 'MarkerFaceColor', 'blue');
ylabel('Infiltration rate f (cm/h)') 

r = [[0], infiltration];
yyaxis right
plot(x,r,'-s','MarkerSize',7, 'MarkerFaceColor', 'red')

legend({'Infiltration rate','Cumulative infiltration'},'Location','north')
xlabel('minutes') 
ylabel('Cumulative infiltration (cm)') 
ax = gca;
ax.YGrid = 'on';

