%EMTH171
%task two

clear 
clc


K = 0.06;   % hydraulic conductivity (cm/hr)
time = 1/4;   % time step (hours)
se = 0.7;   % effective saturation
suctionHead = 23.90;    % suction (cm)
effective_porosity =0.321
delta_theta = (1 - se) * effective_porosity
product = suctionHead * delta_theta;
f_t_inital = 0;
rfall_inc = [0.1, 0.1, 0.3, 0.5, 1.2, 0.8, 0.5, 0.2]; %wet
% rfall_inc = [0.5, 0.8, 1.2, 1.5, 0.7, 0.3, 0.1, 0.1];
rfall_cume = rfall_inc(1);  %sets initial cumulative rainfall to the first incremental rainfall value
i_rate = inf;   % infiltration rate
f = @(F_t, F_t_1) F_t - F_t_1 - (product * log((product + F_t) / (product + F_t_1))) - K * time;    % function for infiltration
d = @(F_t) 1 - (product/(product + F_t));   % derivative of the function

%finds cumulaitive rainfall using incremental rainfall values
for ii = 2 : length(rfall_inc)
    rfall_cume(ii) = rfall_cume(ii-1) + rfall_inc(ii);
end

%finds corresponding rainfall intensity rates
rfall_intensity = rfall_inc ./ time;

%calculates infiltration (F_t), infiltration rate (f_t) and run off (when
%ponding occurs)
previous_inf = 0;   %sets previous infiltration rate to 0
for iii = 1 : length(rfall_inc)
    
    % calculates infiltration and infiltration rate given that current
    % infiltration rate is more than the current rainfall intensity
    if i_rate(iii) > rfall_intensity(iii)
        infiltration(iii) = previous_inf + rfall_intensity(iii)*time;   % infiltration
        previous_inf = infiltration(iii);   % reassigns for next iii iteration
        i_rate(iii+1) = K*((product/infiltration(iii)) + 1);
        
    % calculates when current infiltration rate is less than the rainfall
    % intensity
    else
        F_t_1 = infiltration(iii-1);    % previous cumulative infiltration value
        F_t = 1;    % current cumulative infiltration
        
        % uses Newtons method to find F_t
        for ii = 1 : 4
            F_t = F_t - ((f(F_t, F_t_1)) / (d(F_t)));   % Newtons method
        end
        
        infiltration(iii) = F_t;
        previous_inf = infiltration(iii);   % reassigns for next iii iteration
        i_rate(iii+1) = K*((product/infiltration(iii)) + 1);
        run_off(iii) = rfall_cume(iii) - infiltration(iii); % cumulative run off
        run_off_inc(iii) = run_off(iii) - run_off(iii - 1); % incremental run off
    end
    
end
rfall_intensity
i_rate
infiltration
run_off
run_off_inc