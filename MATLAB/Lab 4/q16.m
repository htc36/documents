% EMTH171/MATH170
% Script to learn about nested if-statements

clear
clc

x = 0; % x and y are used to set z
y = 0;
z = 0; % initialise z

if x > 100
    z = 1;
elseif y > 100;
     z = 1000;
end

% display results
disp(z);