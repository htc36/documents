% EMTH 171
% Checking for oddness

clear 
clc

windSpeed = 15;
maxGustSpeed = 20;

if windSpeed > 12 && maxGustSpeed <= 20
    disp('yes');
else
    disp('no');
end
