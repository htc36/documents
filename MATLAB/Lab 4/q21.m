%EMTH171
%cos function

clear
clc

x = 0.6;
nTerms = 100;
term = 1;
partialSum = 0;
relTol = 0.001;

for ii = 1 : 1 : nTerms 
    partialSum = partialSum + term;
    if abs(term) < abs(relTol * partialSum)
        break
    end
    term = term * ((-1 * x^2)/((2*ii)*(2*ii - 1)));
end
termsUsed = ii 
partialSum