%EMTH171
%Quadratic equation

clear
clc

a = 0.1;
b = 0.6;
c = 0.9;

discrim = b^2 - (4 * a * c)

if (a == 0 && b == 0 && c == 0) 
    disp('Infinite number of solutions');
    
elseif (a == 0) && ~(b == 0)
    disp('A single real root');
    
elseif a == 0 && b == 0 && ~(c == 0)
    disp('No solution');
    
elseif discrim < 0 
   disp('Complex roots')
   
elseif discrim == 0
    disp('One real repeated root');
elseif discrim > 0 
    disp('Two real roots');
end