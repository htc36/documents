% % EMTH 171
% % Checking for oddness
% 
% clear 
% clc
% 
% windSpeed = 10;
% 
% if windSpeed > 12
%     disp('yes');
% elseif (windSpeed <= 12) && (windSpeed > 10)
%     disp('almost')
% else
%     disp('no');
% end

% EMTH171
% Lab 4
% Practising my if-statements

clear
clc

thresholdAplus = 90; % min mark for A+
passMark = 50; % min mark for pass
mark = 40; % student mark

if (mark >= thresholdAplus)
    disp('Brilliant work');
elseif (mark >= passMark)
    disp('Well done - passed');
else 
    disp('Sorry - failed');
end