% % EMTH171
% % Lab 4
% % Practising my if-statements
% 
% clear
% clc
% 
% thresholdAplus = 90;
% passMark = 50;
% mark = 49;
% 
% if mark >= 90
%     disp('Brilliant work')
% elseif mark >= 50
%     disp('Well done - passed')
% else
%     disp('Sorry - failed')
% end

% EMTH171
% Lab 4
% Fix this code

clear
clc

thresholdAplus = 90; % min mark for A+
passMark = 50; % min mark for pass
mark = 90; % student mark

if (mark >= thresholdAplus)
    disp('Brilliant work');
elseif (mark >= passMark)
    disp('Well done - passed');
else 
    disp('Sorry - failed');
end