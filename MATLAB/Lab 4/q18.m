% EMTH 171
% Checking for oddness

clear 
clc

currentDirection =  mod(20, 360);
homeDirection = mod(270, 360);
rubbishWeight = 51;
fuelCellLevel = 60;
distanceHome = 200;

if rubbishWeight >= 50 || (fuelCellLevel < 30 && distanceHome >= 200)
    turningAngle = homeDirection - currentDirection;
    currentDirection = mod((currentDirection + turningAngle),360);
    
elseif (fuelCellLevel < 30 && distanceHome < 200)
    turningAngle = homeDirection - currentDirection - 15;
    currentDirection = mod((currentDirection + turningAngle),360);
    
elseif (fuelCellLevel >= 30 && fuelCellLevel < 60 && distanceHome >= 200)
    turningAngle = homeDirection - currentDirection - 25;
    currentDirection = mod((currentDirection + turningAngle),360);
    
elseif (fuelCellLevel >= 30 && fuelCellLevel < 60 && distanceHome < 200)
    turningAngle = -40;
    currentDirection = mod((currentDirection + turningAngle),360);
    
else 
    turningAngle = +40;
    currentDirection = mod((currentDirection + turningAngle),360);
end

fprintf('Turning Angle is %.0f degrees\n', turningAngle)
fprintf('New Current Direction is %.0f degrees\n', currentDirection)