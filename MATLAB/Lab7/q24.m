% EMTH171
% EUlers method

clear
clc 

f = @(t, m, e) ((2/3) * m) - ((4/3) * m *e);
g = @(t, m, e) -e + m*e;

t0 = 0;
m0 = 1;
e0 = 1;
h = 0.01;
tf = 15;
xArray = [m0];
yArray = [e0];
tArray = [t0];
values = [t0 : h : tf - h ];
for iii = values
    x = m0 + h * f(t0,m0,e0);
    y = e0 + h * g(t0,m0,e0);
    t0 = t0 + h;
    m0 = x;
    e0 = y;
    xArray = [xArray, x];
    yArray = [yArray, y];
    tArray = [tArray, t0];
end
hold on
plot(tArray, xArray)
plot(tArray, yArray)



