% EMTH171
% EUlers method

clear
clc 

f = @(t, x) (1/x^3)+ t^2 + tanh(2*t) - 5;

t0 = 0;
x0 = 4;
tf = 5;
h = 0.02;
xArray = [x0];
tArray = [t0];
values = [t0 : h : tf - h];
for iii = values
    x0 = x0 + h*f(t0,x0);
    t0 = t0 + h;
    xArray = [xArray, x0];
    tArray = [tArray , t0];
end
xArray(end)