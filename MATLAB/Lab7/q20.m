% EMTH171
% EUlers method

clear
clc 

f = @(t, x, y) y*t;
g = @(t, x, y) x - y;

t0 = 1;
x0 = 0.1;
y0 = 0.5;
h = 0.2;
tf = 2;
xArray = [x0];
yArray = [y0];
tArray = [t0];
values = [t0 : h : tf - h ];
for iii = values
    x = x0 + h * f(t0,x0,y0);
    y = y0 + h * g(t0,x0,y0);
    t0 = t0 + h;
    x0 = x;
    y0 = y;
    xArray = [xArray, x];
    yArray = [yArray, y];
    tArray = [tArray, t0];
end
tArray
xArray
yArray

