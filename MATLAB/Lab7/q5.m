% EMTH171
% EUlers method

clear
clc 

%f = @(t, x) -3*t*x;
f = @(t, x) t.*x^2.*cos(t^2);

t0 = 0;
x0 = 1;
tf = 3;
h = 0.1;
xArray = [x0];
tArray = [t0];
values = [t0 : h : tf - h];
for iii = values
    x0 = x0 + h*f(t0,x0);
    t0 = t0 + h;
    xArray = [xArray, x0];
    tArray = [tArray , t0];
end
tArray
xArray
%plot(tArray, xArray)
% formatSpec = '%1.4f\n';
% fprintf(formatSpec, t0)
% fprintf(formatSpec, x0)

