% EMTH171
% EUlers method

clear
clc 

f = @(t, x) t.*x^2.*cos(t^2);

t0 = 0;
x0 = 1;
tf = 3;


hArray = [1e-1, 5e-2, 1e-2];
nh = length(hArray);

%hold on
for iii = 1 : nh
    h = 1e-5;
    [ tArray, xArray ] = ...
    solveODEEulers( f, t0, x0, tf, h );
    %plot(tArray, xArray);
end
xArray
tArray;