% EMTH171
% EUlers method

clear
clc 

f = @(y) -0.043*(y - 20);

results = [];

x0 = 0;
y0 = 100;
tf = 70;
h = 0.01;
targetTemp = 80;
xArray = [y0];
tArray = [x0];
values = [x0 : h : tf - h];
for iii = values
    y0 = y0 + h*f(y0);
    x0 = x0 + h;
    xArray = [xArray, y0];
    tArray = [tArray , x0];
end
value = find(xArray < targetTemp, 1);
tArray(value)
xArray(value)


