function [tArray,xArray] = solveODEEulers(f, t0, x0, tf, h)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
xArray = [x0];
tArray = [t0];
values = [t0 : h : tf - h];
for iii = values
    x0 = x0 + h*f(t0,x0);
    t0 = t0 + h;
    xArray = [xArray, x0];
    tArray = [tArray , t0];
end
end

