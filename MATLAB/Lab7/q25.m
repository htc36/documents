% EMTH171
% EUlers method

clear
clc 
S = 10;
B = 8/3;
R = 613;

f = @(t, x, y, z) S*(y - x);
g = @(t, x, y, z) (R * x) - y - (x * z);
j = @(t, x, y, z) (x * y) - (B * z);


t0 = 0;
x0 = 0.5;
y0 = 0.5;
z0 = 0.5;
h = 0.001;
tf = 2;
xArray = [x0];
yArray = [y0];
tArray = [t0];
zArray = [z0];
values = [t0 : h : tf - h ];
for iii = values
    x = x0 + h * f(t0,x0,y0,z0);
    y = y0 + h * g(t0,x0,y0,z0);
    z = z0 + h * j(t0,x0,y0,z0);
    t0 = t0 + h;
    x0 = x;
    y0 = y;
    z0 = z;
    xArray = [xArray, x];
    yArray = [yArray, y];
    zArray = [zArray, z];
    tArray = [tArray, t0];
end
figure(1)
    plot(xArray, yArray)
figure(2)
    plot(xArray, zArray)
figure(3)
    plot(yArray, zArray)
figure(4)
plot3(xArray, yArray, zArray)
    