%EMTH171
% More for loop arrays

clear
clc

x = 2.6;
partialSum = 0;
fact = 1;
nTerms = 5;

for n = [1 : nTerms]
    fact = (-1)*fact * (2*n + 1) * (2*n);
    expression = fact / ((x^(3*n - 2)) * 2^(n+1));
    partialSum = partialSum + expression  
end