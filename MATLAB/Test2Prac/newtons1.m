%EMTH171
%NEwtons Method
clear
clc
close all

C = -1.2;
f = @(x,C) (exp(x/100)) + 3*sin(x) + C;
d = @(x,C) (1/100) * exp(x/100) + (3 * cos(x));
N = 5;

overall = [];

for jj = C : 0.1 : 2
    x = -2;
    xArray = [x];
    for ii = 1 : N
        x = x - ((f(x,jj)) / (d(x,jj)));
        xArray(ii+1) = x;
    end
    overall = [overall, xArray(end)];
end
x_axis = C : 0.1 : 2;
y_axis = overall;
plot(x_axis, y_axis)


%EMTH171
%Eulers

clear 
clc
close all

f = @(t, x, C) t^3 - cosh(x) + C;

tf = 3;
h = 0.02;
cArray = [-1 : 0.1 : 2];
result = [];
for jjj = 1 : length(cArray)
    t0 = 0;
    x0 = 2;
    tArray = [t0];
    xArray = [x0];
    C = cArray(jjj);
    for iii = [t0 : h : tf-h]
        x0 = x0 + (h * f(t0, x0, C));
        t0 = t0 + h;
        tArray = [tArray, t0];
        xArray = [xArray, x0];
    end
    result = [result, xArray(end)];
end
plot(cArray, result);
result = xArray(end);


%EMTH 171
%Newtons
clear 
clc
close all

f = @(x,C) 2*x^3 - 8*x^2 +(1/x) - C;
g = @(x) 6*x^2 - 16*x + -(1/x^2);
result = [];
N = 4;
cArray = [-5 : 0.5 : -2];
for jjj = 1 : length(cArray)
    x0 = 3;
    xArray = [x0];
    C = cArray(jjj);
    for iii = 1 : N
        x0 = x0 - (f(x0,C))/(g(x0));
        xArray = [xArray, x0];
    end
    result = [result, xArray(end)];

end
plot(cArray, result)

%EMTH171
%Eulers

clear
clc
close all

f = @(x, t, C) t^3 - cosh(x) + C;


cArray = [-1 : 0.1 : 2];

tf = 3;
h = 0.02;
result = [];
for jjj = 1 : length(cArray)
    C = cArray(jjj);
    t0 = 0;
    x0 = 2 ;
    tArray = [t0];
    xArray = [x0];
    for iii = t0 : h : tf - h
        x0 = x0 + (h * f(x0, t0, C));
        t0 = t0 + h;
        tArray = [tArray, t0];
        xArray = [xArray, x0];
    end
    result = [result, xArray(end)];
end
result;
cArray;
plot(cArray, result)

%EMTH171
%partial Sums

clear 
clc
close all

fact = 1;
N = 5;
x = 0.3;
partialSum = 0;

for n = [1 : N]
    fact = fact * (2*n + 1) * (2*n);
    expression = ( (-1)^(n+1) * 2^(3*n-1) ) / (x^(2*n) * fact);
    partialSum = partialSum + expression
end








