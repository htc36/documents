%EMTH171
%Partial sum of series

clear 
clc


f = @(t,x, C) sin(x + 1) + exp(t) - C;
h = 0.05;
tf = 2;
cArray = [-2 : 0.1 : 2];
result = [];


for jjj = 1 : length(cArray)
    C = cArray(jjj);
    t0 = 0;
    x0 = 4;
    xArray = [x0];
    tArray = [t0];
    for iii = [t0 : h : tf - h]
        iii;
        x0 = x0 + h*f(t0, x0, C);
        t0 = h + t0;
        tArray = [tArray, t0];
        xArray = [xArray, x0];
    end
    result = [result, xArray(end)];
end
plot(cArray, result)
tArray;
result;
xArray(end);































%EMTH171
%Eulers

clear 
clc
close all

f = @(t, x, C) t^3 - cosh(x) + C;

tf = 3;
h = 0.02;
cArray = [-1 : 0.1 : 2];
result = [];
for jjj = 1 : length(cArray)
    t0 = 0;
    x0 = 2;
    tArray = [t0];
    xArray = [x0];
    C = cArray(jjj);
    for iii = [t0 : h : tf-h]
        x0 = x0 + (h * f(t0, x0, C));
        t0 = t0 + h;
        tArray = [tArray, t0];
        xArray = [xArray, x0];
    end
    result = [result, xArray(end)];
end
plot(cArray, result);
result = xArray(end);