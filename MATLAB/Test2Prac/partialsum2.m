%EMTH171
% More for loop arrays

clear
clc

x = 1.5;
nTerms = 5;
term = 1;
column = 0;
n = 1;
fact = 1;
sum = 0;


for n = [1 : nTerms]
    fact = fact * (2*n+1) * 2*n;
    expression = (((-1)^n)*(3*n)*(x^(3*n)))/fact;
    sum = sum + expression
end