%EMTH171
% More for loop arrays

clear
clc

x = 0.8;
nTerms =5;
term = 1;
column = 0;
n = 1;
fact = 1;
sum = 0;

for n = 2 : 2 : (2 * nTerms)
    fact = fact * n * (n - 1)
    expression = (2^(n-1)) / (x^(1.5*n)*fact);
    fact = (-1) * fact 
    sum = sum + expression
end
