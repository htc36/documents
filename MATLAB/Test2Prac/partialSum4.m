%EMTH171
%Partial sum of series

clear 
clc

x= 2.8;
nTerms = 5;
fact = 1;
partialSum = 0;

for n = [1 : nTerms]
    fact = fact * 2*n * (2*n -1);
    expression = ( (-1)^(n+1) * 2^(2*n-1) * (x^(3*n)) ) / fact;
    partialSum = partialSum + expression
    
end

%EMTH171
%partial Sums


clear 
clc


nTerms = 5;
x = 3.5;
fact = 1;
partialSum = 0;

for n = [1 : 1 : nTerms]
    fact = fact * (2*n + 1) * (2*n);
    expression = ( (-1)^n * (3*n) * fact ) / ( x^(3*n));
    partialSum = partialSum + expression
end


























    