function [ d ] = distanceCalculator(x1,y1,x2,y2)
%Calculate distance betwwen 2 points
%   using right angle triangles
d = sqrt((x2 - x1)^2 + (y2 - y1)^2);
end

