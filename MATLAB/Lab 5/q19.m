%EMTH171
% %anonimas functions
% 
clear 
clc
% 
% result = [];
% inVal = [1, 2, 3];
% f = @(x) (x + 1) .* (3.*x -2);
% result = f(inVal)

K = 1.84;
spLen = 120;
wLvl = 310;
maxLvl = 300;
spFR = spillFlowRate(wLvl, maxLvl, K, spLen);
fprintf('Spill rate %.2f m^3 per s\n', spFR);