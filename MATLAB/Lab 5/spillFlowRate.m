function [spillrate] = spillFlowRate(h, hmax, K, L)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%spill flow rate

if h < hmax
    spillrate = 0;
else
    spillrate = (K .* L).*(h - hmax).^1.5;
end
end

