function [out] = finiteDerivativeApprox(f, x)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

h = 1e-6;
p = (f(x + h) - f(x)) / h;

out = p;

end