clear
clc 

fHandle = @sin;
xval = pi/3;
approxDeriv = finiteDerivativeApprox(fHandle, xval);
fprintf('%.4f\n', approxDeriv);