% EMTH171/MATH170
% script to use a function handle

clear
clc
 
f = @log;

x = 5; % input value
% calculate and display result
result = f(x); 
disp(result);