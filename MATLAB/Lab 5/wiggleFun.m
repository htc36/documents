function [ y ] = wiggleFun( x ) 
%WIGGLEFUNCTION y = 2/3 - 2/9*x - sin(x.^2) 
%INPUT: x, scalar value or array 
 
y = 2/3 - 2/9*x - sin(x.^2);  
 
end

