%EMTH171
%plotting stuff

clear 
clc

f = @mean;
maxNData = 1000;

for ii = [1 : maxNData]
    arrayA = rand(1, ii);
    y(ii) = f(arrayA);
end
x = [1 : maxNData];
plot(x, y )
title('Data summary using mean')
xlabel('Number of data values')
ylabel('mean')