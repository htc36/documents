function [ tempFahrenheit ] = convertToFahrenheit( tempCelsius ) 
%CONVERTTOFAHRENHEIT convert a temperature in Celsius to Fahrenheit 
%   temperature in Fahrenheit = 9/5 * tempCelsius + 32 
 
tempFahrenheit = 9/5 * tempCelsius + 32; 
 
end
