function [volume,surfaceArea] = sphereVolumeAndSurfaceArea(radius)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
surfaceArea = 4.*pi.*radius.^2;
volume = (4/3).*pi.*radius.^3;
end

