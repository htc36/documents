%EMTH171
%demonstrate function handles

clear
clc
close all

f = @wiggleFun;

x = pi/6;
minx = -pi;
maxx = pi;

y = f(x)

xArray = minx : pi/64 : maxx;
yArray = f(xArray);

figure(1);
plot(xArray, yArray);


fmin_x = fminbnd(f, minx, maxx)
