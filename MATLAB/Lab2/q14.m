% EMTH171/MATH170
% sum of integers from 13 to 56

clear
clc

startValue = 2; % starting value
stopValue = 8; % stopping value
stepValue = 1; % intervel amount
product = 1;

for ii = startValue : stepValue : stopValue
    product = product * ii;
end

product %display overall total