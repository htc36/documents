% EMTH171
% Script: For loops
% Author: Harry Collard

clear
clc

n = 12; % number of repetitions

for ii = n : 2 : 1 % start : intervel : stop number
    ii %displays the value ii
end

disp('exited for-loop with ii = ')
disp(ii)