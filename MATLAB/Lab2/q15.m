% EMTH171/MATH170
% sum of integers from 13 to 56

clear
clc

nTerms = 6; % stopping value
term = 0;

for n = 0 : 1 : (nTerms - 1)
    term = term + (n^3)
end

