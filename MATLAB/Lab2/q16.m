% EMTH171/MATH170
% sum of integers from 13 to 56

clear
clc

nTerms = 6; % stopping value
n_minus_2 = 2 % n minus 2
n_minus_1 = 1 % n minus 1

for ii = 1 : 1 : (nTerms - 2)
    n = (n_minus_1) + (n_minus_2)
    n_minus_2 = n_minus_1;
    n_minus_1 = n ;
end