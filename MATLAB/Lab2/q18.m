% EMTH171/MATH170
% sum of integers from 13 to 56

clear
clc

nTerms = 5; % Number of values in pattern
term = 1; % inital term value
partialSum = 1 % intial partial sum

for n = 2 : 1 : nTerms
    term = term * ((2 * n) - 1) * ((2 * n) - 2)
    partialSum = partialSum + term;
    partialSum;
end

    