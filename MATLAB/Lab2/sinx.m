% EMTH171/MATH170
% script to print values of sin(x)

clear
clc

startValue = pi; % starting value
stopValue = 5*pi/4; % stopping value
stepValue = 0.2; % intervel amount

for ii = startValue : stepValue : stopValue
    answer = sin(ii);
    answer
end
