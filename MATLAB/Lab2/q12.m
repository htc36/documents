% EMTH171/MATH170
% sum of integers from 13 to 56

clear
clc

startValue = 13; % starting value
stopValue = 56; % stopping value
stepValue = 1; % intervel amount
total = 0;

for ii = startValue : stepValue : stopValue
    total = total + ii;
end

total %display overall total