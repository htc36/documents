% EMTH171/MATH170
% sum of integers from 13 to 56

clear
clc

startValue = 11; % starting value
stopValue = -8; % stopping value
stepValue = -2; % intervel amount
total = 0;

for ii = startValue : stepValue : stopValue
    total = total + ii;
end

total %display overall total