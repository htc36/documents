% EMTH171
% Script: Practise Assignment Statements
% Author: Harry Collard

clear
clc

% Assigning values to variables a, b and c
a = 3;
b = 14;
c = 8;

% Assigning values to variables x, y and z
x = a + b - c
y = a/c
z = (exp(a))