%EMTH171
%Practise problem

clear
clc

f = @(x,c) 10.*x.^2 - 10.*x + 0.1.*sinh(x) + c;
d = @(x, c) 20*x -10 + 0.1.*cosh(x);

x = 1;
cArray = [-400 : 10 : -300];
count = 1;
results = [];
xArray(1) = x;
N = 5;
for pp = (cArray)
    for ii = 1 : N
        x = x - (f(x,pp))/(d(x,pp));
        xArray(ii + 1) = x;
    end
    results(count) = xArray(end);
    count = count + 1;
    xArray = xArray(1);
    x = xArray(1);
   
end
plot(cArray, results);
