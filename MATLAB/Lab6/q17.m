% EMTH171
% Script use NewtonsMethodWithBreak function
% fix required 
 
clear 
clc 
 
% ----------------------- variables -------------------- 
f = @(x) x/5 - cos(6*x); 
% derivative of f 
d = @(x) 1/5 + 6*sin(6*x); 
 
% ------------------- Newtons method ----------------- 
x = -3; % first guess 
N = 20; % number of iterations 
tol = 1e-4;
 
rootsArray = NewtonsMethodWithBreak( x, f, d, N , tol); 

finalRoot = rootsArray(end);

% display final root found
fprintf('Root found %.4f\n', finalRoot);

% Sanity check:  what is f(x) at this approx?
fValue = f(finalRoot);
fprintf('f(final root)= %.4f\n', fValue);