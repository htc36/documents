x = -0.5;
N = 20;
tol = 1e-2;
f = @(x) 2*x.^4 - x.^3 - 4*x.^2 + 3*x - 2/5;
d = @(x) 8*x.^3 - 3*x.^2 - 8*x + 3;

[ rootsArray ] = NewtonsMethodWithBreak( x, f, d, N, tol );
fprintf('Final root %.4f\n', rootsArray(end));
fprintf('Iterations %d\n', length(rootsArray)-1);