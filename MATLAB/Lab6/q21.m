%EMTH171
%NEwtons Method
clear
clc
close all

f = @(x, b) x - (exp(-b.*x.^2) .* cos(x));
d = @(x, b) (exp(-b.*x.^2)) .* (sin(x) + 2.*b.*x.*cos(x) + exp(-b.*x.^2));

x = 0.3;
b = 24
N = 30;
step = [0 : N];
tol =0.0001;
xArray(1) = x;
counter = 0;
reached = 0;
plot(f(x,b), step)
for ii = 1 : N
    counter = counter + 1;
    x = x - ((f(x,b)) / (d(x,b)));
    err(ii) = abs(f(x,b));
    xArray(ii+1) = x;
    count(ii) = abs((xArray(ii+1) - xArray(ii)));
    if ((err(ii)) < tol) && (count(ii) < tol)
        reached = 1;
        break 
    end
end
root = xArray(end)
f(root,b)