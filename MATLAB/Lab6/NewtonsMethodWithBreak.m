function [rootsArray] = NewtonsMethodWithBreak(x, f, d, N, tol)
%Function for guessing roots
%Using newtons Method
rootsArray(1) = x;
counter = 0;
for ii = 1 : N
    counter = counter + 1;
    x = x - ((f(x)) / (d(x)));
    err(ii) = abs(f(x));
    rootsArray(ii+1) = x;
    count(ii) = abs((rootsArray(ii+1) - rootsArray(ii)));
    if ((err(ii)) < tol) && (count(ii) < tol)
        break
    end
end
end

