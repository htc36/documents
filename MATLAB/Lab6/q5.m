%EMTH171
%NEwtons Method
clear
clc
close all

f = @(x) (2*x.^4) - (x.^3) - (4*x.^2) + (3.*x) -(2/5);
d = @(x) (8*x.^3) - (3*x.^2) - (8.*x) + 3;
x = -0.5;
N = 20;
tol = 1e-2;
for ii = 1 : N+1
    xn = x;
    guess(ii) = x;
    error(ii) = abs(f(x)) 
    x = x - ((f(x)) / (d(x)))
    xn1 = x;
    count(ii) = abs(xn1 - xn)
    if abs((f(x)) < tol) && (abs(xn1 - xn) < tol)
        break
    end
    if ii == N+1
        break
    end
    

end
ii
guess
