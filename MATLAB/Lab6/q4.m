%EMTH171
%NEwtons Method
clear
clc
close all

f = @(x) (2*x.^4) - (x.^3) - (4*x.^2) + (3.*x) -(2/5)
d = @(x) (8*x.^3) - (3*x.^2) - (8.*x) + 3
guess = -2
xMin = -2
xMax = 2
xStep = 0.1

xArray = xMin : xStep : xMax;
yArray = f(xArray)
yArrayd = d(xArray)

plot(xArray, yArray)
hold on
plot(xArray, yArrayd)
hold off

calc = guess - ((f(guess)) / (d(guess)))