%EMTH171
%Intersection points

clear
clc

g = @(x) 1/3 -(3/4).*x;
dg = @(x) -(3/4);
h = @(x) 2*sin(x.^2);
dh = @(x) (4.*x)*cos(x.^2);
f = @(x) g(x) - h(x);
x = -1;
xArray(1) = x;
N = 20;
tol = 0.001;

for ii = 1 : N
    
    x = x - ((g(x) - h(x))/(dg(x) - (dh(x))));
    err(ii) = abs(g(x) - h(x));
    xArray(ii+1) = x;
    count(ii) = abs((xArray(ii+1) - xArray(ii)));
    if ((err(ii)) < tol) && (count(ii) < tol)
        reached = 1;
        break
    end
end
root = xArray(end)
g(root)