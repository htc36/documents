%EMTH171
%NEwtons Method
clear
clc
close all

f = @(x) (2*x.^4) - (x.^3) - (4*x.^2) + (3.*x) -(2/5);
d = @(x) (8*x.^3) - (3*x.^2) - (8.*x) + 3;
x = -0.5;
N = 30;
tol = 1e-2;
xArray(1) = x;
counter = 0;
reached = 0;
for ii = 1 : N
    counter = counter + 1;
    x = x - ((f(x)) / (d(x)));
    err(ii) = abs(f(x));
    xArray(ii+1) = x;
    count(ii) = abs((xArray(ii+1) - xArray(ii)))
    if ((err(ii)) < tol) && (count(ii) < tol)
        reached = 1;
        break
    end
end
xArray
counter;
xValues = [0 : counter];
plot(xValues,xArray);
    