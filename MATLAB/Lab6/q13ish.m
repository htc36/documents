%EMTH171
%NEwtons Method
clear
clc
close all

f = @(x) -100*x.^2 + 5;
d = @(x) -200*x;
x = 3;
N = 20;
tol = 0.05;
xArray(1) = x;
counter = 0;
for ii = 1 : N+1
    counter = counter + 1;
    x = x - ((f(x)) / (d(x)));
    err(ii) = abs(f(x));
    xArray(ii+1) = x;
    count(ii) = abs((xArray(ii+1) - xArray(ii)));
    if ((err(ii)) < tol) && (count(ii) < tol)
        break
    end
end
xArray(end)
counter

%EMTH171
%NEwtons Method
clear
clc
close all

f = @(x) (2*x.^4) - (x.^3) - (4*x.^2) + (3.*x) -(2/5);
d = @(x) (8*x.^3) - (3*x.^2) - (8.*x) + 3;
x = -2;
N = 5;
tol = 1e-4;
xArray(1) = x;
counter = 0;
reached = 0;
for ii = 1 : N
    counter = counter + 1;
    x = x - ((f(x)) / (d(x)));
    err(ii) = abs(f(x));
    xArray(ii+1) = x;
    count(ii) = abs((xArray(ii+1) - xArray(ii)));
    if ((err(ii)) < tol) && (count(ii) < tol)
        reached = 1;
        break
    end
end
xArray(end)
counter

if reached == 1
    disp('Convergence has been reached')
else
    disp('Convergence has not been reached')
end