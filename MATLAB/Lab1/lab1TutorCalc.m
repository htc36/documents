% EMTH171/MATH170
% Script example: Calculate tutors required for EMTH171/MATH170
% Author: Harry Collard

clear
clc

% Set up variables for number of students, tutors per lab and lab size
numberOfStudents = 720;
labSize = 20;
tutorsPerLab = 2;

% Calculate number of labs as ratio students to lab size
numberOfLabs = numberOfStudents/labSize;

% Calculation of tutors required as product of tutorsPerLab and numberOfLabs
tutorsRequired = tutorsPerLab * numberOfLabs;

tutorsRequired % display tutors required
numberOfLabs % display number of labs required