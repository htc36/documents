% EMTH171
% making arrays

clear 
clc

arrayB = [0.1; 1; 0.2; 2]; %An array

nArrayB = length(arrayB);
product = 1;

for ii = 1 : nArrayB
    product = arrayB(ii) * product; %old product times new one
end

disp(product)