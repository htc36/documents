%EMTH171

clear
clc

x = 2;
nTerms = 10;
term = x^0;
termsArray(1) = term;

for n = 1 : nTerms - 1
    term = term * x / n;
    termsArray(n+1) = term;
end
disp(termsArray)