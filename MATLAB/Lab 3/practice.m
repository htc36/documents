clc 
clear
% arrayA = [1 2 3 5]
% arrayB = [0.1; 0.2; 0.3]
% arrayC = [10 20; 30 40; 50 60]
% arrayD = [1 2 3; 4 5 6]
% 
% arrayz = [0 : 0.1 : 1]
arrayA = [1 : 0.5 : 200]
arrayB = [-2*pi : pi/12 : 2*pi]

% max(arrayB)
% prod(arrayA)
% % cumsum(arrayB)
% 
% size(arrayC)
% size(arrayD)

% length(arrayA)
% length(arrayB)
% length(arrayC)
% length(arrayD)
% 
% numel(arrayA)
% numel(arrayD)

% min(arrayC)