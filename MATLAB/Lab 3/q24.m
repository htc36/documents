%EMTH171
%Plotting stuff

clear
clc

arrayX = [0 : pi/16 : 2*pi];

arrayY = sin(arrayX);

plot(arrayX, arrayY)
title('Plotting a sine curve') % add a plot title
xlabel('x') %x axis label
ylabel('sin(x)')
