% clear 
% clc
% 
% arrayA = [10 20; 30 40; 50 60]
% 
% sizeA = size(arrayA);
% m = sizeA(1)
% n = sizeA(2)
% 
% for ii = 1:m
%     for jj = 1:n
%         arrayA(ii, jj)
%     end
% end
% disp(arrayA)

% EMTH171/MATH170
% Preallocation efficiency example

clear
clc

N = 100000; % number of elements to go into array

% Use 'Run and Time' first with the following line 
myArray = []; % empty array, no preallocation

% then comment out the line above and uncomment the line below
% and use 'Run and Time' again
%myArray = zeros(1, N); % with preallocation

for ii = 1 : N
    myArray(ii) = ii;
end

disp('Finished');