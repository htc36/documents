%EMTH171

clear
clc

x = 1.6;
nTerms = 5;
term = 1;
partialSum = 0;
column = 0;

for n = 1 : 2 : (nTerms *2)
    column = column + 1;
    term = (term * n);
    expression = ((x^(n+1))/term);
    term = (-1)*(term * (n+1));
    partialSum = partialSum + expression;
    SumArray(1,(column)) = partialSum;
end
SumArray