% EMTH171
% Use nested for loops to transpose an array

clear
clc

% set dimensions of original array
m = 2; % rows in original
n = 3; % columns in original 

arrayA = (rand(m, n));
transposedArray = [];


for row = 1 : n
    for coloum = 1 : m
        transposedArray(row, coloum) = arrayA(coloum, row);
    end 
end

disp(transposedArray)