%EMTH171
%Array exponential thing

clear
clc

arrayX = [pi : pi/2 : 3*pi];
result = exp(arrayX/2).*(sin(-arrayX))