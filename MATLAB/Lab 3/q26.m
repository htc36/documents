%EMTH171
%Importing stuff

clear
clc

dataFilename = 'AIRNZ.csv';
dataArray = load(dataFilename); % current figure
xAxis = dataArray(:, 1)/7; %x axis
yAxis = dataArray(:, 2); %y axis

plot(xAxis, yAxis);
title('Plotting')
xlabel('Weeks')
ylabel('Closing share price')

