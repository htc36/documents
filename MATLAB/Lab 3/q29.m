% EMTH171/MATH170
% Part of a script to calculate and store
% terms in a power series 

clear
clc

nTerms = 5; % Number of values in pattern
x = 0.1;
term = 1; % inital term value
partialSum = 1; % intial partial sum

for n = 2 : 2 : (nTerms * 2)
    term = (term * n)
    termsArray(1,(n/2)) = (term*(x^(n/2)));
    term = term * (n + 1);
end
termsArray;
