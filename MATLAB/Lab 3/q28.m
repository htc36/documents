% %EMTH171
% %Plotting stuff
% 
clear
clc
% 
% n = 3;
% m = 4;
% 
% arrayA = rand(1, n)
% arrayB = rand(n, m)
% arrayC = rand(m, 1)
% 
% n = 10
% onesArray = 3 * ones(1, n)

N = 10;
product = 1;
factorialArray(1) = product;

for n = 2 : 1 : N
    product = product * n;
    factorialArray(n) = product;
end

factorialArray

