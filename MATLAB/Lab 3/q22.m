%EMTH171 stuff
%more arrays

clear
clc

arrayX = [0 : 0.2 : 5];
arrayY = (arrayX).*log(arrayX.^4+1);
